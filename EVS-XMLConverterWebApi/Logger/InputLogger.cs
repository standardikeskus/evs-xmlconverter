﻿using System.IO;
using EVS_XMLConverter.Aids;
using EVS_XMLConverterWebApi.Models;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverterWebApi.Logger {
    /// <summary>Used for API-side logging.</summary>
    public static class InputLogger {

        /// <summary>Logs the input to discover mistakes.</summary>
        public static void LogInputData(EvsDataModel evsDataModel) {
            LogWriter.Directory = new DirectoryInfo(string.Concat(AppSettings["Temporary"]));
            LogWriter.LogWrite("XmlPackage: " + evsDataModel.EvsXmlPackageDestination);
            LogWriter.LogWrite("PDF: " + evsDataModel.PdfDestination);
            LogWriter.LogWrite("Preview: " + evsDataModel.PdfPreviewDestination);
            foreach (var xmlZipFile in evsDataModel.XmlZipFiles) { LogWriter.LogWrite("XmlFile: " + xmlZipFile); }
            LogWriter.LogWrite("EnTitle: " + evsDataModel.EnTitle);
            LogWriter.LogWrite("EtTitle: " + evsDataModel.EtTitle);
            LogWriter.LogWrite("EvsReference: " + evsDataModel.EvsReference);
            LogWriter.LogWrite("StaDISOrganisation: " + evsDataModel.Organisation);
            LogWriter.LogWrite("Amendments: " + evsDataModel.Amendments);
            LogWriter.LogWrite("BulletinIssue: " + evsDataModel.BulletinIssue);
            LogWriter.LogWrite("Scope: " + evsDataModel.Scope);
            LogWriter.LogWrite("Committee: " + evsDataModel.Committee);
            if (!File.Exists(evsDataModel.XmlZipFiles[0])) throw new ConversionException("XML failid asukohast puudu");
        }
    }
}