﻿using System.Web;
using System.Web.Http;

namespace EVS_XMLConverterWebApi {

    /// <summary>Applications start</summary>
    public class WebApiApplication : HttpApplication {

        /// <summary>Application start, Configuration</summary>
        protected void Application_Start() {
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
