﻿using System.Web.Http;

namespace EVS_XMLConverterWebApi {

    /// <summary>API Config. Main configurations are located in Web.config.</summary>
    public class WebApiConfig {

        /// <summary>Register and route the API.</summary>
        public static void Register(HttpConfiguration config) {
            // Web API routes
           config.MapHttpAttributeRoutes();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
