﻿using System;
using System.Collections.Generic;

namespace EVS_XMLConverterWebApi.Models {
    /// <summary>Model describes the parameters coming from StaDIS.
    /// StaDIS is using the API based on EvsDataModel to compile standard files.
    /// Do not edit this unless StaDIS code is edited.</summary>
    public class EvsDataModel {
        /// <summary>Gets or sets the imported XML .zip files (from StaDIS).</summary>
        public List<string> XmlZipFiles { get; set; }

        /// <summary>Gets or sets the destination for the EVS XML Package (from StaDIS).</summary>
        public string EvsXmlPackageDestination { get; set; }

        /// <summary>Gets or sets the destination for the final Pdf (from StaDIS).</summary>
        public string PdfDestination { get; set; }

        /// <summary>Gets or sets the destination for the preview Pdf (from StaDIS).</summary>
        public string PdfPreviewDestination { get; set; }

        /// <summary>Gets or sets the general folder name for the standard (from StaDIS).</summary>
        public string FolderName { get; set; }

        /// <summary>Gets or sets the general file name for the standard (from StaDIS).</summary>
        public string FileName { get; set; }

        /// <summary>Gets or sets the organisation (from StaDIS).</summary>
        public string Organisation { get; set; }

        /// <summary>Gets or sets the Estonian title (from StaDIS).</summary>
        public string EtTitle { get; set; }

        /// <summary>Gets or sets the English title (from StaDIS).</summary>
        public string EnTitle { get; set; }

        /// <summary>Gets or sets the EVS Reference (from StaDIS).</summary>
        public string EvsReference { get; set; }

        /// <summary>Gets or sets the ICS groups (from StaDIS).</summary>
        public List<string> IcsGroups { get; set; }

        /// <summary>Gets or sets the EVS Teataja issue date for EVS ISO (from StaDIS).</summary>
        public DateTime BulletinIssue { get; set; }

        /// <summary>Gets or sets the scope for EVS ISO (from StaDIS).</summary>
        public string Scope { get; set; }

        /// <summary>Gets or sets the committee for EVS ISO (from StaDIS).</summary>
        public string Committee { get; set; }

        /// <summary>Gets or sets the amendment text for EVS ISO (from StaDIS).</summary>
        public string Amendments { get; set; }
    }
}