﻿using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Logic.Pdf;
using EVS_XMLConverter.Logic.Zip;
using EVS_XMLConverter.Objects;
using EVS_XMLConverterWebApi.Models;

namespace EVS_XMLConverterWebApi.Controllers {

    /// <summary>For StaDIS. Converts XML files to HTML and PDF format.</summary>
    public class ConverterController : ApiController {

        /// <summary>StaDIS sends project input data to converter via Post.
        /// Post returns Ok or BadRequest, depending on whether the files were compiled to given locations correctly.</summary>
        [HttpPost]
        public IHttpActionResult Post(EvsDataModel evsDataModel) {
        try {
                Logger.InputLogger.LogInputData(evsDataModel);
                var evsData = new EvsData {
                    XmlZipFiles = evsDataModel.XmlZipFiles.Select(x => new FileInfo(x)).ToList(),
                    EvsXmlPackageDestination = new FileInfo(evsDataModel.EvsXmlPackageDestination),
                    PdfDestination = new FileInfo(evsDataModel.PdfDestination),
                    PdfPreviewDestination = new FileInfo(evsDataModel.PdfPreviewDestination),
                    StaDISOrganisation = evsDataModel.Organisation,
                    EtTitle = evsDataModel.EtTitle,
                    EnTitle = evsDataModel.EnTitle,
                    EvsReference = evsDataModel.EvsReference,
                    IcsGroups = evsDataModel.IcsGroups,
                    EvsIsoData = new EvsIsoData() {
                        BulletinIssue = evsDataModel.BulletinIssue,
                        Scope = evsDataModel.Scope,
                        Amendments = evsDataModel.Amendments,
                        Committee = evsDataModel.Committee
                    }
                };

                var zipPaths = evsData.XmlZipFiles;
                var package = EvsXmlPackageCompiler.CompileEvsXmlPackage(evsData);
                PdfGenerator.XmlToPdf(evsData, package);
                return Ok();
            }
            catch (Exception ex) {
                LogWriter.LogWrite(string.Concat(ex.Message, " in ", ex.Source));
                LogWriter.LogWrite(ex.StackTrace);
                return BadRequest(ex.Message);
            }
        }
    }
}
