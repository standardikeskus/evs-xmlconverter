﻿using EVS_XMLConverter.Logic.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass()]
    public class FootnoteModifierTests {
        private string _isoCenHtmlOriginalBodyString;
        private string _cenIsoHtmlOriginalBodyString;

        [TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
            _cenIsoHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
            CenIsoHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _cenIsoHtmlOriginalBodyString;
        }
        [TestMethod]
        public void FixFootnotes_FootnotesAtTheEndOfHtml_FootnotesClassifiedToBePrinceCompatible() {
            ClassAdder.ClassifySupLinks(IsoCenHtml);
            var footnoteNode = IsoCenHtml.DocumentNode.Descendants().Where(d => d.Attributes["class"]?.Value == StsFootnotes);
            var singleFootnoteNodes = IsoCenHtml.DocumentNode.Descendants().Where(d => d.Attributes["class"]?.Value == StsFootnote);
            Assert.AreEqual(1, footnoteNode.Count());
            Assert.AreEqual(0, singleFootnoteNodes.Count());

            FootnoteModifier.FixFootnotes(IsoCenHtml);
            Assert.AreEqual(0, footnoteNode.Count());
            Assert.AreEqual(1, singleFootnoteNodes.Count());
        }

        [TestMethod()]
        public void CleanFootnoteLinkTest() {
            Assert.Inconclusive();
        }

        [TestMethod()]
        public void CreateValueLinkTest() {
            Assert.Inconclusive();
        }

        [TestMethod()]
        public void CreateFootnoteTest() {
            Assert.Inconclusive();
        }

        [TestMethod()]
        public void CleanFootnoteTest() {
            Assert.Inconclusive();
        }
    }
}