﻿using System.Linq;
using EVS_XMLConverter.Logic.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class XslConverterTests {
        private string _isoCenHtmlOriginalBodyString;
        private string _cenIsoHtmlOriginalBodyString;

        [TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
            _cenIsoHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
            CenIsoHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _cenIsoHtmlOriginalBodyString;
        }

        [TestMethod]
        public void XmlsToHtmlsTest() {
            //suur meetod mis ainult ühendab kõiki meetodeid
            Assert.Inconclusive();
        }

        [TestMethod]
        public void MochHtmlTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        [DataRow(NameInfo.Iso, "© ISO 2004 - All rights reserved")]
        [DataRow(NameInfo.Cen, "")]
        public void AddCopyrightNotice_HtmlDocument_AddedCopyrightStringToHtml(string fileName, string expected) {
            var copyrightNotice = IsoCenHtml.GetElementbyId(CopyrightNotice);
            Assert.IsNull(copyrightNotice);

            XslConverter.AddCopyrightNotice(IsoCenHtml, GetXmlStandard(fileName));
            copyrightNotice = IsoCenHtml.GetElementbyId(CopyrightNotice);
            Assert.AreEqual(expected, copyrightNotice.InnerHtml);
        }

        [TestMethod]
        public void RemoveLinkFromExternalLinksTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void RemoveRepeatedLabelsTest() {
            //needs very specific file with double labels
            Assert.Inconclusive();
        }

        [TestMethod]
        public void MarkStartOfStandard_HtmlDocument_Added() {
            var start = IsoCenHtml.DocumentNode.Descendants("div")
                .FirstOrDefault(d => d.Attributes["class"]?.Value.Contains(StsMain) == true)?
                .ChildNodes.FirstOrDefault(c => c.Name != "#text");
            if (start == null) Assert.Fail();
            Assert.AreEqual(StsSection, start.Attributes["class"]?.Value);

            XslConverter.MarkStartOfStandard(IsoCenHtml);
            Assert.AreEqual(string.Concat(StsSection, " ", StsFirstSection), start.Attributes["class"]?.Value);

        }

        [TestMethod]
        public void MarkBoldStatementsTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void MoveQuotesToNextElementTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void BringLabelToFrontOfListElementTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void RemoveEmptyNodeFromEndTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FixEntailedTermsLinkingTest() {
            Assert.Inconclusive();
        }
    }
}