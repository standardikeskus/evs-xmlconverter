﻿using System.Linq;
using EVS_XMLConverter.Logic.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class ClassAdderTests {
        private string _isoCenHtmlOriginalBodyString;
        private string _cenIsoHtmlOriginalBodyString;

        [TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
            _cenIsoHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
            CenIsoHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _cenIsoHtmlOriginalBodyString;
        }

        [TestMethod]
        public void ClassifyEndorsement_HtmlDocumentWithEndorsementNotices_AddedClassToEndorsementNotices() {
            var endorsementNotices = IsoCenHtml.DocumentNode.SelectNodes("//div")?
                .Where(d => d.InnerHtml == "Endorsement notice").ToList();
            if (endorsementNotices == null) Assert.Fail();
            foreach (var notice in endorsementNotices) {
                Assert.AreEqual(null, notice.Attributes["class"]?.Value);
            }

            ClassAdder.ClassifyEndorsement(IsoCenHtml);
            foreach (var notice in endorsementNotices) {
                Assert.AreEqual(StsEndorsementNotice, notice.Attributes["class"]?.Value);
            }
        }

        [TestMethod]
        public void ClassifyIntros_HtmlDocumentWithIntros_AddedClassToIntros() {
            var intros = IsoCenHtml.DocumentNode.SelectNodes("//div")?.Where(d => d.Id.Contains("intro")).ToList();
            if (intros == null) Assert.Fail();
            foreach (var intro in intros) {
                Assert.AreEqual(null, intro.Attributes["class"]?.Value);
            }

            ClassAdder.ClassifyIntros(IsoCenHtml);
            foreach (var intro in intros) {
                Assert.AreEqual(StsSectionIntro, intro.Attributes["class"]?.Value);
            }
        }

        [TestMethod]
        public void ClassifyBibliograpies_HtmlDocumentWithBibliograpy_AddedClassToBibliographiesAndHeaderMoved() {
            var bibliographies = IsoCenHtml.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Id.Contains("bibl")).ToList();
            if (bibliographies == null) Assert.Fail();
            foreach (var bibl in bibliographies) {
                Assert.AreEqual("sts-ref-list", bibl.Attributes["class"]?.Value);
                Assert.IsTrue(bibl.FirstChild.Name == "ol");
            }

            ClassAdder.ClassifyBibliograpies(IsoCenHtml);
            foreach (var bibl in bibliographies) {
                Assert.AreEqual(string.Concat("sts-ref-list ", StsSectionBibl), bibl.Attributes["class"]?.Value);
                Assert.IsTrue(bibl.FirstChild.Name == "h1" && bibl.FirstChild.InnerHtml == "Bibliography");
            }
        }

        [TestMethod]
        public void GroupRefListsTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void ClassifySupLinks_HtmlDocumentWithLinks_AddedClassesToAllLinksElements() {
            var links = IsoCenHtml.DocumentNode.SelectNodes("//a")?
                .Where(a => a.ChildNodes.Where(c => c.Name == "sup").FirstOrDefault() != null).ToList();
            if (links == null) Assert.Fail();
            foreach (var link in links) {
                Assert.AreEqual(StsXref, link.Attributes["class"]?.Value);
                if (link.ParentNode.Name != "div") continue;
                Assert.AreEqual(StsP, link.ParentNode.Attributes["class"]?.Value);
            }

            ClassAdder.ClassifySupLinks(IsoCenHtml);
            foreach (var link in links) {
                Assert.IsTrue(link.Attributes["class"].Value.Contains(SupLink));
                if (link.ParentNode.Name != "div") continue;
                Assert.AreEqual(StsPSupDiv, link.ParentNode.Attributes["class"]?.Value);
            }
        }

        [TestMethod]
        public void ClassifyForewords_HtmlDocumentWithForeword_AddedClassToForeword() {
            var forewords = IsoCenHtml.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Id.Contains("sec_foreword")).ToList();
            if (forewords == null) Assert.Fail();
            foreach (var foreword in forewords) {
                Assert.AreEqual(null, foreword.Attributes["class"]?.Value);
            }

            ClassAdder.ClassifyForewords(IsoCenHtml);
            foreach (var foreword in forewords) {
                Assert.AreEqual(StsForeword, foreword.Attributes["class"]?.Value);
            }
        }
    }
}