﻿using EVS_XMLConverter.Logic.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class AmendmentModifierTests {
        private string _isoCenHtmlOriginalBodyString;[TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
        }

        [TestMethod]
        public void ChangeAmendmentSectionTitleClasses_AmendmentContainsSectionTitles_SectionTitlesClassesChanged() {
            var sectionTitles = IsoCenHtml.DocumentNode.Descendants().Where(d => d.Attributes["class"]?.Value.Contains(StsSecTitle) == true);
            Assert.AreEqual(170, sectionTitles.Count());

            AmendmentModifier.ChangeAmendmentSectionTitleClasses(IsoCenHtml, true);
            var sectionAmdTitles = IsoCenHtml.DocumentNode.Descendants().Where(d => d.Attributes["class"]?.Value.Contains(StsAmdSecTitle) == true);
            Assert.AreEqual(170, sectionAmdTitles.Count());
        }

        [TestMethod]
        public void GroupAmendmentSectionsTest() {
            Assert.Inconclusive();
        }
    }
}