﻿using EVS_XMLConverter.Logic.Html;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using static EVS_XMLConverterTests.DataDocuments;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using System.IO;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class ImageModifierTests {
        private string _isoCenHtmlOriginalBodyString;

        [TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
        }

        [TestMethod]
        public void AddImageLocationsToHtml_ImagesExist_AddedSrcValue() {
            var images = IsoCenHtml.DocumentNode.SelectNodes("//img");
            foreach (var image in images) {
                if (image.Attributes["alt"]?.Value.Contains(NameInfo.IsoCenRegularImageName) == true) continue;
                image.Remove();
            }

            var xmlStandard = new XmlStandard {
                Originator = Originator.Evs,
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName)),
                ReleaseDate = new System.DateTime(NameInfo.IsoCenYear, 1, 1)
            };
            var htmlDocLocationsAdded = ImageModifier.AddImageLocationsToHtml(IsoCenHtml, xmlStandard);
            images = htmlDocLocationsAdded.DocumentNode.SelectNodes("//img");
            Assert.AreEqual(1, images.Count);
            var expectedSrc = string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName,
                NameInfo.IsoCenRegularImageName, ".png");
            Assert.AreEqual(expectedSrc, images[0].Attributes["src"].Value);
        }

        [TestMethod()]
        public void ConvertTifImagesToPngTests() {
            Assert.Inconclusive();
        }

        [TestMethod]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.Cen, 2009, "width: 668px;")]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.Cen, 2014, "width: 668px;")]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.Clc, 2009, "width: 668px;")]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.Clc, 2014, "width: 668px;")]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.Iso, 2014, "width: 285.12px;")]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.Iso, 2009, "width: 285.12px;")]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.CenClc, 2009, "width: 668px;")]
        [DataRow(NameInfo.IsoCenRegularImageId, ElementNames.CenClc, 2014, "width: 668px;")]
        public void ResizeImage_RegularImage_WidthDecreasedBasedOnOriginatorAndYear(
            string imageId, string originator, int year, string expected) {
            var image = IsoCenHtml.GetElementbyId(imageId);
            Assert.IsNull(image.Attributes["style"]);

            var xmlStandard = new XmlStandard {
                Originator = Originator.GenerateOriginator(originator, false),
                ReleaseDate = new System.DateTime(year, 1, 1)
            };
            image = ImageModifier.ResizeImage(image, xmlStandard, NameInfo.IsoCenRegularImageDirectory, false);
            Assert.AreEqual(expected, image.Attributes["style"]?.Value);
        }

        [TestMethod]
        public void ReplaceMathElements_OneMathImageExists_OneMathNodeReplaced() {
            Assert.AreEqual(22, IsoCenHtml.DocumentNode.SelectNodes("//math").Count());
            ImageModifier.ReplaceMathElements(IsoCenHtml, IsoCenXmlStandard);
            Assert.AreEqual(21, IsoCenHtml.DocumentNode.SelectNodes("//math").Count());
        }

        [TestMethod]
        [DataRow(NameInfo.IsoCenFirstMathImageId, "FirstMathImageDirectory", "img")]
        [DataRow(NameInfo.IsoCenSecondMathImageId, null, "math")]
        public void ReplaceImage_MathImage_ImageNodeTypeAndSrcChanged(string imageId, string imageDirectory,
            string imageNodeName) {
            if (imageDirectory == "FirstMathImageDirectory") imageDirectory = NameInfo.IsoCenFirstMathImageDirectory;
            var imageNode = IsoCenHtml.GetElementbyId(imageId);
            Assert.IsNull(imageNode.Attributes["src"]);
            Assert.AreEqual("math", imageNode.Name);

            var xmlStandard = new XmlStandard {
                Originator = Originator.Evs,
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName)),
                ReleaseDate = new System.DateTime(NameInfo.IsoCenYear, 1, 1)
            };
            var imageFileLocation = string.Concat(xmlStandard.ImageDirectory, imageId, ".png");
            IsoCenHtml = ImageModifier.ReplaceImage(IsoCenHtml, imageNode, xmlStandard, imageFileLocation, true);
            imageNode = IsoCenHtml.GetElementbyId(imageId);
            Assert.AreEqual(imageDirectory, imageNode.Attributes["src"]?.Value);
            Assert.AreEqual(imageNodeName, imageNode.Name);
        }
        [TestMethod]
        public void MoveArrayOutOfImage_ImageNodeHasArray_ArraysMovedOutOfImageNode() {
            var arrayList = IsoCenHtml.DocumentNode.Descendants().Where(d =>
                d.Attributes["class"]?.Value == StsArray && d.ParentNode.Attributes["class"]?.Value == StsFig).ToList();
            Assert.AreEqual(3, arrayList.Count);

            var imageList = IsoCenHtml.DocumentNode.Descendants("img").ToList();
            foreach (var image in imageList) { ImageModifier.MoveArrayOutOfImage(image); }
            arrayList = IsoCenHtml.DocumentNode.Descendants().Where(d =>
                d.Attributes["class"]?.Value == StsArray && d.ParentNode.Attributes["class"]?.Value == StsFig).ToList();
            Assert.AreEqual(0, arrayList.Count);
        }

        [TestMethod]
        public void MoveCaptionToImageBottom_CaptionBeforeArrayImage_CaptionMovedAfterImage() {
            var firstArray = IsoCenHtml.DocumentNode.Descendants()
                .FirstOrDefault(d => d.Attributes["class"]?.Value.Contains(StsArray) == true
                                                   && d.ParentNode.Attributes["class"]?.Value == StsFig);
            var caption = firstArray?.ParentNode?.ChildNodes.FirstOrDefault(c => c.Attributes["class"]?.Value.Contains(StsCaption) == true);
            Assert.AreEqual(1, firstArray?.ParentNode?.ChildNodes.IndexOf(caption));

            ImageModifier.MoveCaptionToImageBottom(IsoCenHtml);
            Assert.AreEqual(5, firstArray?.ParentNode?.ChildNodes.IndexOf(caption));
        }
    }
}