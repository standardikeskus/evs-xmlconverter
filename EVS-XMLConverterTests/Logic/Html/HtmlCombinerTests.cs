﻿using System.Collections.Generic;
using System.IO;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Logic.Html;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class HtmlCombinerTests {
        private HtmlDocument _template;
        private string _cenIsoHtmlOriginalBodyString;
        private string _isoCenHtmlOriginalBodyString;

        [TestInitialize]
        public void Initialize() {
            EvsTitlepageHtml.Load(NameInfo.EvsTitlepageHtml);
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
            _cenIsoHtmlOriginalBodyString = CenIsoHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
            CenIsoHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _cenIsoHtmlOriginalBodyString;
        }
        [TestMethod]
        public void CombineTest() {
            //vale directory
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FillEvsTitlepage_NoData_NoErrors() {
            _template = new HtmlDocument();
            var body = _template.CreateElement("body");
            var content = _template.CreateElement("div");
            body.AppendChild(content);
            _template.DocumentNode.AppendChild(body);
            var unchanged = HtmlCombiner.FillEvsTitlepage(_template, IsoCenXmlStandard);
            Assert.AreEqual(content, unchanged);
        }

        [TestMethod]
        public void FillEvsTitlepage_EmptyTemplate_ConversionException() {
            _template = new HtmlDocument();
            var exception = Assert.ThrowsException<ConversionException>(() => HtmlCombiner.FillEvsTitlepage(_template, IsoCenXmlStandard));
            Assert.AreEqual("Template on vigane", exception.Message);
        }

        [TestMethod]
        public void AddEvsForewordTest() {
            //vale directory
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FillEvsForeword_NoData_NoErrors() {
            _template = new HtmlDocument();
            var body = _template.CreateElement("body");
            var content = _template.CreateElement("div");
            body.AppendChild(content);
            _template.DocumentNode.AppendChild(body);
            var unchanged = HtmlCombiner.FillEvsForeword(_template, new List<XmlStandard> { CenIsoXmlStandard, IsoCenXmlStandard });
            Assert.AreEqual(content, unchanged);
        }

        [TestMethod]
        public void FillEvsForeword_EmptyTemplate_ConversionException() {
            _template = new HtmlDocument();
            var exception = Assert.ThrowsException<ConversionException>(() => HtmlCombiner.FillEvsForeword(_template, new List<XmlStandard> { CenIsoXmlStandard, IsoCenXmlStandard }));
            Assert.AreEqual("Template on vigane", exception.Message);
        }

        [TestMethod]
        public void AddEnTitlepageTest() {
            //vale directory
            Assert.Inconclusive();
        }

        [TestMethod]
        public void LoadEnTitlepageTemplateTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        [DataRow(NameInfo.IsoCen)]
        [DataRow(NameInfo.CenIso)]
        public void AppendContentToBody_TitlepageBody_ContentAppendedToBody(string fileName) {
            //seda testi annab parandada
            _template = GetHtmlDocument(fileName);
            var titlepageBody = EvsTitlepageHtml.DocumentNode.SelectSingleNode("//body");
            Assert.AreEqual(718, titlepageBody.InnerHtml.Length);
            titlepageBody = HtmlCombiner.AppendContentToBody(_template, titlepageBody);
            Assert.AreNotEqual(718, titlepageBody.InnerHtml.Length);
        }

        [TestMethod]
        public void FillEnTitlepage_NoData_NoErrors() {
            _template = new HtmlDocument();
            var body = _template.CreateElement("body");
            var content = _template.CreateElement("div");
            body.AppendChild(content);
            _template.DocumentNode.AppendChild(body);
            var unchanged = HtmlCombiner.FillEnTitlepage(_template, IsoCenXmlStandard);
            Assert.AreEqual(content, unchanged);
        }

        [TestMethod]
        public void FillEnTitlepage_EmptyTemplate_ConversionException() {
            _template = new HtmlDocument();
            var exception = Assert.ThrowsException<ConversionException>(() => HtmlCombiner.FillEnTitlepage(_template, IsoCenXmlStandard));
            Assert.AreEqual("Template on vigane", exception.Message);
        }

        [TestMethod]
        public void AddEvsBackCoverTest() {
            //vale directory
            Assert.Inconclusive();
        }

        [TestMethod]
        public void SaveFullHtmlAndPreview_DirectoryInfo_HtmlSavedToDirectory() {
            var fileName = "testFile";
            var fullFileName = @"\testFile.html";
            var fullPreviewFileName = @"\testFile_preview.html";
            var directoryInfo = new DirectoryInfo(NameInfo.HtmlDirectory);
            HtmlCombiner.SaveFullHtmlAndPreview(CenIsoHtml, directoryInfo, fileName);
            Assert.IsTrue(File.Exists(string.Concat(directoryInfo.Parent?.FullName, fullFileName)));
            Assert.IsTrue(File.Exists(string.Concat(directoryInfo.Parent?.FullName, fullPreviewFileName)));
            File.Delete(string.Concat(NameInfo.TestDataDirectory, fullFileName));
            File.Delete(string.Concat(NameInfo.TestDataDirectory, fullPreviewFileName));
        }
    }
}