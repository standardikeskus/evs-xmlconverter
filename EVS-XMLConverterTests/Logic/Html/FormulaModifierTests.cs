﻿using System.Linq;
using EVS_XMLConverter.Logic.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class FormulaModifierTests {
        private string _isoCenHtmlOriginalBodyString;
        private string _cenIsoHtmlOriginalBodyString;

        [TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
            _cenIsoHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
            CenIsoHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _cenIsoHtmlOriginalBodyString;
        }

        [TestMethod]
        public void FixFormulasTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void CenterFormulaPanelsContents_FormulaPanelContainsTextNodes_TextReplacedWithSpanNodes() {
            var firstFormulaPanel = IsoCenHtml.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value.Contains(StsDispFormulaPanel) == true);
            Assert.AreEqual(2, firstFormulaPanel.ChildNodes.Where(d => d.Name == "#text").Count());

            FormulaModifier.CenterFormulaPanelsContents(firstFormulaPanel);
            Assert.AreEqual(0, firstFormulaPanel.ChildNodes.Where(d => d.Name == "#text").Count());
            Assert.AreEqual(2, firstFormulaPanel.ChildNodes.Where(d => d.Attributes["class"]?.Value.Contains(StsFormulaText) == true).Count());
        }

        [TestMethod]
        public void FixFormulaSpacingsTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FixFormulaMarkersTest() {
            Assert.Inconclusive();
        }
    }
}