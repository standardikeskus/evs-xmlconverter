﻿using System.Collections.Generic;
using System.Linq;
using EVS_XMLConverter.Logic.Html;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class TocGeneratorTests {
        private string _isoCenHtmlOriginalBodyString;
        private HtmlNode _mainDiv;
        private HtmlNode _secondDiv;
        private HtmlNode _thirdDiv;
        private HtmlNode _mainSpan;
        const string _testId = "toc_standard";

        [TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
        }
        public void InitializeNodes() {
            _mainDiv = IsoCenHtml.CreateElement("div");
            _secondDiv = IsoCenHtml.CreateElement("div");
            _thirdDiv = IsoCenHtml.CreateElement("div");
            _mainSpan = IsoCenHtml.CreateElement("span");
        }

        [TestMethod]
        public void AddEvsTableOfContents_NoTableOfContents_TableOfContentsNodeAdded() {
            Assert.IsFalse(IsoCenHtml.DocumentNode.Descendants().Where(d => d.Id.Contains(EvsTableOfContents) == true).Any());
            TocGenerator.AddEvsTableOfContents(IsoCenHtml, new List<XmlStandard> { IsoCenXmlStandard });
            Assert.IsTrue(IsoCenHtml.DocumentNode.Descendants().Where(d => d.Id.Contains(EvsTableOfContents) == true).Any());
        }

        [TestMethod]
        public void FindAllHeadingNodes_IsoCenHtml_AllHeadingsList() {
            var headingNodes = TocGenerator.FindAllHeadingNodes(IsoCenHtml);
            Assert.AreEqual(328, headingNodes.Count);
        }

        [TestMethod]
        public void ChangeDuplicateIds_DuplicateIdsHtml_DuplicateIdsChanged() {
            InitializeNodes();
            _mainDiv.Id = _testId;
            _secondDiv.Id = _testId;
            _thirdDiv.Id = _testId;

            TocGenerator.ChangeDuplicateIds(new List<HtmlNode> { _mainDiv, _secondDiv, _thirdDiv });
            Assert.AreEqual("toc_standard_0", _mainDiv.Id);
            Assert.AreEqual("toc_standard_1", _secondDiv.Id);
            Assert.AreEqual("toc_standard", _thirdDiv.Id);
        }

        [TestMethod]
        public void FilterHeadings_FindAllHeadingNodesResult_FilteredHeadingList() {
            var allHeadingNodes = TocGenerator.FindAllHeadingNodes(IsoCenHtml);
            Assert.AreEqual(328, allHeadingNodes.Count);

            var headingDivs = TocGenerator.FilterHeadings(allHeadingNodes);
            Assert.AreEqual(110, headingDivs.Count);
        }

        [TestMethod]
        public void CreateTocNode_TestHeading_ValidHtmlGenerated() {
            var headingParent = IsoCenHtml.CreateElement("div");
            headingParent.Id = "toc_testId";
            var heading = IsoCenHtml.CreateElement("h1");
            heading.InnerHtml = "testTitle";
            headingParent.AppendChild(heading);
            var headingList = new List<HtmlNode> { heading };
            var tocNode = TocGenerator.CreateTocNode(IsoCenHtml, headingList, new List<XmlStandard> { IsoCenXmlStandard });
            var rawHtml = "<div id=\"toc-title\">Contents</div><div class=\"toc-section\"><a href=\"#toc_testId\"><span class=\"toc-right\" href=\"#toc_testId\">testTitle</span></a></div>";
            Assert.AreEqual(rawHtml, tocNode.InnerHtml);
        }

        [TestMethod]
        public void CreateTocNodeBase_ValidHtml_MainTocNodeCreated() {
            var tocNodeBase = TocGenerator.CreateTocNodeBase(IsoCenHtml);
            Assert.AreEqual(EvsTableOfContents, tocNodeBase.Id);
            Assert.AreEqual(StsStandard, tocNodeBase.Attributes["class"]?.Value);
            Assert.AreEqual("Contents", tocNodeBase.Descendants().FirstOrDefault(d => d.Id == TocTitle)?.InnerHtml);
        }

        [TestMethod]
        public void TitlepageDivToTocData_TitlepageDiv_TitlepageNodeAsTocData() {
            InitializeNodes();
            _mainSpan.Id = EnReference;
            const string title = "testTitle";
            _mainSpan.InnerHtml = title;
            _mainDiv.Id = _testId;
            _mainDiv.AppendChild(_mainSpan);

            var tocData = TocGenerator.TitlepageDivToTocData(_mainDiv);
            Assert.AreEqual(title, tocData.TocTitle);
            Assert.AreEqual(TocTitlepage, tocData.TocClass);
            Assert.AreEqual(_testId, tocData.TocReference);
        }

        [TestMethod]
        [DataRow(true, "EN ISO 11133")]
        [DataRow(false, "1<span class=\"toc-spacing\">Â </span>Scope")]
        public void HeadingToTocData_HeadingNodeAndXDocument_TocDataGenerated(bool isoForewordCheck, string expectedTitle) {
            var heading = IsoCenHtml.DocumentNode.Descendants("h1").FirstOrDefault();

            var tocData = TocGenerator.HeadingToTocData(heading, CenIsoXml, isoForewordCheck);
            Assert.AreEqual(expectedTitle, tocData.TocTitle);
            Assert.AreEqual("toc_iso_std_iso_11133_ed-1_v2_en_sec_1", tocData.TocReference);
            Assert.AreEqual("toc-section", tocData.TocClass);
        }

        [TestMethod]
        public void HeadingToTocData_EmptyNode_NullReturned() {
            var heading = IsoCenHtml.CreateElement("div");
            var tocData = TocGenerator.HeadingToTocData(heading, CenIsoXml, false);
            Assert.IsNull(tocData);
        }

        [TestMethod]
        public void ReplaceLinkElements_LinkNode_LinkNodeReplaced() {
            var heading = IsoCenHtml.DocumentNode.Descendants("h1").FirstOrDefault();
            heading?.ChildNodes[1].Attributes.Add("class", StsXref);
            var links = heading?.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains(StsXref) == true).ToList();
            Assert.AreEqual(1, links?.Count);

            var newHeading = TocGenerator.ReplaceLinkElements(heading);
            links = newHeading.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains(StsXref) == true).ToList();
            Assert.AreEqual(0, links.Count);
        }

        [TestMethod]
        public void GetAnnexHeadingTitleTest() {
            var heading = IsoCenHtml.DocumentNode.Descendants("h1")
                .FirstOrDefault(d => d.ParentNode.Attributes["class"]?.Value == "sts-app");
            Assert.AreEqual("AnnexÂ A", heading?.InnerHtml);

            var annexHeading = TocGenerator.GetAnnexHeadingTitle(heading);
            var expected =
                "AnnexÂ A <span>(informative)</span> " +
                "<span>Designation of the components of culture media in International " +
                "Standards on microbiological analysis of food, animal feed and water</span>";
            Assert.IsTrue(annexHeading.Contains(expected));
        }

        [TestMethod]
        public void AddAsChildToPreviousTocData() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void InsertTocDatasIntoTocNodeTests() {
            Assert.Inconclusive();
        }

        [TestMethod]
        [DataRow("1<span class=\"toc-spacing\">Â </span>Scope", "<span class=\"toc-left\">1</span>")]
        [DataRow("Scope", "")]
        public void TocDataToHtmlElement_NewTocData_GeneratedHtmlElement(string tocTitle, string expectedTocLeft) {
            var tocNode = new TocData {
                TocClass = "toc-section",
                TocReference = "toc_sec_1",
                TocTitle = tocTitle
            };
            var tocElement = TocGenerator.TocDataToHtmlElement(tocNode);
            var expected = "<a href=\"#toc_sec_1\">" + expectedTocLeft +
                           "<span class=\"toc-right\" href=\"#toc_sec_1\">Scope</span>" +
                           "</a>";
            Assert.AreEqual(expected, tocElement.InnerHtml);
        }

        [TestMethod]
        public void ComposeTitleNode_SpacingSpanAndTitleElementTemplate_ModifiedTitleElement() {
            var tocTitle = "1<span class=\"toc-spacing\">Â </span>Scope";
            var titleAsNode = IsoCenHtml.CreateElement("");
            titleAsNode.InnerHtml = tocTitle;
            var spacingSpan = titleAsNode.ChildNodes
                .FirstOrDefault(c => c.Attributes["class"]?.Value == "toc-spacing");

            var titleElement = IsoCenHtml.CreateElement("span");
            titleElement.Attributes.Add("class", "toc-right");

            titleElement = TocGenerator.ComposeTitleNode(spacingSpan, titleElement);
            Assert.AreEqual("<span class=\"toc-right\">Scope</span>", titleElement.OuterHtml);
        }
    }
}