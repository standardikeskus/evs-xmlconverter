﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using EVS_XMLConverter.Logic.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class WhiteSpaceFixerTests {
        [TestMethod]
        public void MinimizeHtmlTest() {
            Assert.Inconclusive();
        }

        [TestMethod()]
        public void RemoveSpacingsFromNodeTest() {
            Assert.Inconclusive();
        }

        [TestMethod()]
        public void RemoveMarginFromEmptyFirstTableColumnTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FixWhitespacesTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void RemoveEmptyDivsTest() {
            Assert.Inconclusive();
            //var emptyDivs = CenIsoHtml.DocumentNode.Descendants("div")
            //    .Where(d => d.Attributes["class"]?.Value == "sts-p").ToList();
            //Assert.AreEqual("â€‹", emptyDivs[0].InnerHtml);
            //Assert.AreEqual(1, emptyDivs.Count);

            //XslConverter.RemoveEmptyDivs(CenIsoHtml);
            //Assert.AreEqual(0, emptyDivs.Count);
        }

        [TestMethod]
        public void RemoveMarginFromBsFontTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FixCaptionSpacingTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FixTableWhiteSpacesTest() {
            Assert.Inconclusive();
        }

        [TestMethod()]
        public void RemoveWhiteSpacesTest() {
            Assert.Inconclusive();
            //var descendants = IsoCenHtml.DocumentNode.Descendants().Where(d => d.InnerText.Contains("/n")).ToList();
            //Assert.AreEqual(27, descendants.Count);
            //IsoCenHtml = XslConverter.FixTableWhiteSpaces(IsoCenHtml);
            //descendants = IsoCenHtml.DocumentNode.Descendants().Where(d => d.InnerText.Contains("/n")).ToList();
            //Assert.AreEqual(0, descendants.Count);
        }
    }
}