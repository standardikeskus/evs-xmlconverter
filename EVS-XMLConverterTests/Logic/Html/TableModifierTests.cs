﻿using System.Collections.Generic;
using EVS_XMLConverter.Logic.Html;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using System.IO;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class TableModifierTests {
        private string _isoCenHtmlOriginalBodyString;

        [TestInitialize]
        public void Initialize() {
            _isoCenHtmlOriginalBodyString = IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml;
        }

        [TestCleanup]
        public void Cleanup() {
            IsoCenHtml.DocumentNode.SelectSingleNode("//body").InnerHtml = _isoCenHtmlOriginalBodyString;
        }
        [TestMethod]
        public void ReplaceTableElements_TableImageExists_TableNodeTypeAndSrcChanged() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableDImageId);
            Assert.IsNull(tableWrap.Descendants("img").FirstOrDefault());
            Assert.IsNotNull(tableWrap.Descendants("table").FirstOrDefault());

            var xmlStandard = new XmlStandard {
                Originator = Originator.Evs,
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            IsoCenHtml = TableModifier.ReplaceTableElements(IsoCenHtml, xmlStandard);
            Assert.IsNull(tableWrap.Descendants("table").FirstOrDefault());
            Assert.IsNotNull(tableWrap.Descendants("img").FirstOrDefault());
            Assert.AreEqual(NameInfo.IsoCenTableDImageDirectory,
                tableWrap.Descendants("img").FirstOrDefault()?.Attributes["src"].Value);
        }

        [TestMethod]
        public void ReplaceTableElements_TableImageNotExist_TableUnchanged() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableEImageId);
            Assert.IsNull(tableWrap.Descendants("img").FirstOrDefault());
            Assert.IsNotNull(tableWrap.Descendants("table").FirstOrDefault());

            var xmlStandard = new XmlStandard {
                Originator = Originator.Evs,
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName)),
                ReleaseDate = new System.DateTime(NameInfo.IsoCenYear, 1, 1)
            };
            IsoCenHtml = TableModifier.ReplaceTableElements(IsoCenHtml, xmlStandard);
            Assert.IsNull(tableWrap.Descendants("img").FirstOrDefault());
            Assert.IsNotNull(tableWrap.Descendants("table").FirstOrDefault());
        }

        [TestMethod]
        public void ResizeTables_BigTable_WidthReduced() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableEImageId);
            Assert.AreEqual("1123", tableWrap.Descendants("table").FirstOrDefault()?.Attributes["width"].Value);

            IsoCenHtml = TableModifier.ResizeTables(IsoCenHtml, NameInfo.IsEuropeanIsoCen, null);
            Assert.AreEqual("819.79", tableWrap.Descendants("table").FirstOrDefault()?.Attributes["width"].Value);
        }

        [TestMethod]
        public void ResizeTables_NormalTable_WidthReducedToSpecific() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableEImageId);
            var table = tableWrap.Descendants("table").FirstOrDefault();
            if (table != null) table.Attributes["width"].Value = "780";

            IsoCenHtml = TableModifier.ResizeTables(IsoCenHtml, NameInfo.IsEuropeanIsoCen, null);
            Assert.AreEqual("569.4", table?.Attributes["width"].Value);
        }

        [TestMethod]
        public void ResizeTableBorders_ThickTableBorders_BorderSizesReduced() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableE2ImageId);
            tableWrap.InnerHtml = tableWrap.InnerHtml.Replace("solid 2px", "solid 3px");
            Assert.IsTrue(tableWrap.InnerHtml.Contains("solid 3px"));

            TableModifier.ResizeTableBorders(tableWrap.ChildNodes.FirstOrDefault(c => c.Name == "table"));
            Assert.IsFalse(tableWrap.InnerHtml.Contains("solid 3px"));
        }

        [TestMethod]
        public void ResizeTableBorderTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void AddBorderToBottomOfTablesIfMissingTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void AddAlignmentToRotatedTableContentTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void ClassifyTables_OldEuropeanTable_TableClassChanged() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableEImageId);
            var table = tableWrap.Descendants("table").FirstOrDefault();
            if (table == null) Assert.Fail();
            Assert.IsNull(table.Attributes["class"]);

            IsoCenHtml = TableModifier.ClassifyTables(IsoCenHtml, NameInfo.IsEuropeanIsoCen, NameInfo.OldYear);
            if (table.Attributes["class"] == null) Assert.Fail();
            Assert.IsTrue(table.Attributes["class"].Value.Contains("sts-table-en-borderless"));
        }


        [TestMethod]
        public void MoveIdFromWrapToTableTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void ClassifySideTurnedTable_SideturnTable_TableClassChangedAndWidthAdded() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableEImageId);
            var table = tableWrap.Descendants("table").FirstOrDefault();
            if (table == null) Assert.Fail();
            Assert.IsFalse(tableWrap.Attributes["class"].Value.Contains("sts-table-wrap-sideturn"));
            Assert.IsTrue(tableWrap.Attributes["width"] == null);

            TableModifier.ClassifySideTurnedTable(1000, table, tableWrap);
            Assert.AreEqual("sideturned-table", table.Attributes["class"].Value);
            Assert.AreEqual("920", table.Attributes["width"].Value);
        }

        [TestMethod]
        public void ClassifyLandscapeTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void AddPortraitOrLandscapeClassToParentsTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void MoveHeadingsToTable_PagebreakTable_HeadingsMovedToTableWrap() {
            var tableWrap = IsoCenHtml.GetElementbyId(NameInfo.IsoCenTableDImageId);
            Assert.AreEqual(5, tableWrap.ChildNodes.Count);
            TableModifier.MoveHeadingsToTable(IsoCenHtml);
            Assert.AreEqual(12, tableWrap.ChildNodes.Count);
        }

        [TestMethod]
        [DataRow(StsSecTitle, StsSecTitle, StsSecTitle, 0)]
        [DataRow(StsAppHeader, StsAppHeader, StsAppHeader, 0)]
        [DataRow(StsSecTitle, StsAppHeader, StsTableWrap, 3)]
        [DataRow(StsSecTitle, StsAppHeader, StsArray, 3)]
        [DataRow(StsSecTitle, StsBlankDiv, StsBlankDiv, 2)]

        public void CountPreviousNodes_NodeWithPreviousSiblings_CorrectCountAsInteger(string first, string second, string third, int expected) {
            var classNames = new List<string> { first, second, third };
            var container = IsoCenHtml.CreateElement("div");
            foreach (var className in classNames) {
                var child = IsoCenHtml.CreateElement("div");
                child.Attributes.Add("class", className);
                container.AppendChild(child);
            }
            var actual = TableModifier.CountPreviousNodes(container.ChildNodes.Last());
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MoveNodesIntoTableContainer_NodesBeforeTableContainer_NodesMovedIntoTableContainer() {
            var container = IsoCenHtml.CreateElement("div");
            for (var i = 0; i < 4; i++) { container.AppendChild(IsoCenHtml.CreateElement("div")); }
            container.ChildNodes.Last().Attributes.Add("class", StsTableWrap);
            container.ChildNodes.Last().ChildNodes.Add(IsoCenHtml.CreateElement("div"));
            var tableWrap = container.ChildNodes.Last();
            Assert.AreEqual(1, tableWrap.ChildNodes.Count);

            TableModifier.MoveNodesIntoTableContainer(tableWrap, tableWrap);
            Assert.AreEqual(4, tableWrap.ChildNodes.Count);
        }

        [TestMethod]
        public void MoveCoParentNodesIntoTableContainer_OnePreviousCoParent_CoParentMovedIntoTableContainer() {
            var containerParent = IsoCenHtml.CreateElement("div");
            var container = IsoCenHtml.CreateElement("div");
            for (var i = 0; i < 2; i++) { container.AppendChild(IsoCenHtml.CreateElement("div")); }
            container.ChildNodes.Last().Attributes.Add("class", StsTableWrap);
            container.ChildNodes.Last().ChildNodes.Add(IsoCenHtml.CreateElement("div"));
            var tableWrap = container.ChildNodes.Last();
            containerParent.AppendChild(IsoCenHtml.CreateElement("div"));
            containerParent.AppendChild(container);
            Assert.AreEqual(1, tableWrap.ChildNodes.Count);

            TableModifier.MoveCoParentNodesIntoTableContainer(tableWrap, tableWrap);
            Assert.AreEqual(2, tableWrap.ChildNodes.Count);
        }
    }
}