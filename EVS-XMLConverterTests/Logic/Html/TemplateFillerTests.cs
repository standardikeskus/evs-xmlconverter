﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Logic.Html;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverterTests.DataDocuments;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace EVS_XMLConverterTests.Logic.Html {
    [TestClass]
    public class TemplateFillerTests {
        private HtmlDocument _template;
        private List<XmlStandard> _xmlStandardList;

        [TestInitialize]
        public void Initialize() {
            EvsTitlepageHtml.Load(NameInfo.EvsTitlepageHtml);
            CenTitlepageHtml.Load(NameInfo.CenTitlepageHtml);
            EvsForewordHtml.Load(NameInfo.EvsForewordHtml);
            EvsIsoForewordHtml.Load(NameInfo.EvsIsoForewordHtml);
            _xmlStandardList = new List<XmlStandard>();
            
            CenIsoXmlStandard.XDoc = XDocument.Load(NameInfo.CenIsoXml, LoadOptions.PreserveWhitespace);
            IsoCenXmlStandard.XDoc = XDocument.Load(NameInfo.IsoCenXml, LoadOptions.PreserveWhitespace);
            AmdCenIsoXmlStandard.XDoc = XDocument.Load(NameInfo.AmdCenIsoXml, LoadOptions.PreserveWhitespace);
            AmdIsoXmlStandard.XDoc = XDocument.Load(NameInfo.AmdIsoXml, LoadOptions.PreserveWhitespace);
            Amd2IsoXmlStandard.XDoc = XDocument.Load(NameInfo.Amd2IsoXml, LoadOptions.PreserveWhitespace);
        }

        [TestMethod]
        [DataRow(ElementNames.English, true, "EvsForeword", NameInfo.EnTitle)]
        [DataRow(ElementNames.English, false, "EvsForeword", NameInfo.EnTitle)]
        [DataRow(ElementNames.Estonian, true, "EvsForeword", NameInfo.EtTitle)]
        [DataRow(ElementNames.Estonian, false, "EvsForeword", NameInfo.EtTitle)]
        [DataRow(ElementNames.English, true, "EvsIsoForeword", NameInfo.EnTitle)]
        [DataRow(ElementNames.English, false, "EvsIsoForeword", NameInfo.EnTitle)]
        [DataRow(ElementNames.Estonian, true, "EvsIsoForeword", NameInfo.EtTitle)]
        [DataRow(ElementNames.Estonian, false, "EvsIsoForeword", NameInfo.EtTitle)]
        public void ReplaceTitles_DummyEvsTitles_TitleStringsReplaced(string language, bool quotes, string templateName,
            string expectedTitle) {
            _template = templateName == EvsForewordScope ? EvsForewordHtml : EvsIsoForewordHtml;

            var htmlDoc = TemplateFiller.ReplaceTitles(_template, DummyEvsData, language, quotes);
            var titles = htmlDoc.DocumentNode
                .Descendants().Where(d => d.Attributes["class"]?.Value.Contains(string.Concat(EtTitle, language)) == true);
            if (titles == null) Assert.Fail();

            if (quotes) expectedTitle = string.Concat("„", expectedTitle, "“");
            foreach (var title in titles) {
                Assert.AreEqual(expectedTitle, title.InnerHtml);
            }
        }

        [TestMethod]
        [DataRow(true, "EvsTitlepage", NameInfo.EvsReference)]
        [DataRow(true, "EvsTitlepage", NameInfo.EvsReference)]
        [DataRow(true, "EvsIsoForeword", NameInfo.EvsReference)]
        [DataRow(false, "EvsIsoForeword", "EN ISO 11133:2014")]
        [DataRow(false, "EvsForeword", "EN ISO 11133:2014")]
        public void ReplaceReferences_DummyEvsReferences_ReferenceStringsReplaced(bool evs, string templateName,
            string expectedReference) {
            switch (templateName) {
                case "EvsTitlepage":
                    _template = EvsTitlepageHtml;
                    break;
                case "EvsForeword":
                    _template = EvsForewordHtml;
                    break;
                case "EvsIsoForeword":
                    _template = EvsIsoForewordHtml;
                    break;
            }

            var htmlDoc = TemplateFiller.ReplaceReferences(_template, EvsCenIsoXmlStandard, evs);
            var referenceNode = evs ? EvsReferenceEt : EvsReferenceEn;
            var evsReferences = htmlDoc.DocumentNode.Descendants("span")
                .Where(d => d.Attributes["class"]?.Value.Contains(referenceNode) == true).ToList();
            if (evsReferences == null || !evsReferences.Any()) Assert.Fail();

            foreach (var reference in evsReferences) {
                Assert.AreEqual(expectedReference, reference.InnerHtml);
            }
        }

        [TestMethod]
        [DataRow("EvsForeword")]
        [DataRow("EvsIsoForeword")]
        public void ReplaceDates_DummyEvsDates_DateStringsReplaced(string templateName) {
            _template = templateName == "EvsForeword" ? EvsForewordHtml : EvsIsoForewordHtml;
            var dates = _template.DocumentNode.Descendants("span")
                .Where(d => d.Attributes["class"]?.Value.Contains(EnDateOfAvailability) == true).ToList();
            foreach (var date in dates) {
                Assert.AreEqual("[Date of Availability]", date.InnerHtml);
            }

            TemplateFiller.ReplaceDates(_template, TestDate);
            foreach (var date in dates) {
                Assert.AreEqual("29.10.1995", date.InnerHtml);
            }
        }

        [TestMethod]
        [DataRow(true, "EvsForeword", true, true, " and its amendments A1:2018, A2:2018")]
        [DataRow(true, "EvsForeword", true, false, " and its amendment A1:2018")]
        [DataRow(true, "EvsIsoForeword", false, true, " and its amendments A1:2018 and A2:2018")]
        [DataRow(true, "EvsIsoForeword", false, false, " and its amendment A1:2018")]
        [DataRow(false, "EvsForeword", true, true, " ja selle muudatuste A1:2018, A2:2018")]
        [DataRow(false, "EvsForeword", true, false, " ja selle muudatuse A1:2018")]
        [DataRow(false, "EvsIsoForeword", false, true, " ja selle muudatuste A1:2018 ja A2:2018")]
        [DataRow(false, "EvsIsoForeword", false, false, " ja selle muudatuse A1:2018")]
        public void AddForewordAmendments_XmlStandardListWithAmendments_GeneratedAndAddedAmendmentTextToHtmlDoc(bool english, string templateName,
            bool european, bool multiple, string expectedText) {
            _template = templateName == "EvsForeword" ? EvsForewordHtml : EvsIsoForewordHtml;
            if (european) {
                _xmlStandardList.AddRange(new List<XmlStandard> { CenIsoXmlStandard, IsoCenXmlStandard, AmdCenIsoXmlStandard, AmdIsoXmlStandard });
                if (multiple) _xmlStandardList.AddRange(new List<XmlStandard> { AmdCenIsoXmlStandard, Amd2IsoXmlStandard });
            }
            else {
                _xmlStandardList.AddRange(new List<XmlStandard> { IsoXmlStandard, AmdIsoXmlStandard });
                if (multiple) _xmlStandardList.Add(Amd2IsoXmlStandard); 
            }

            var htmlDoc = TemplateFiller.AddForewordAmendments(_template, _xmlStandardList);
            var referenceNode = english ? EvsReferenceEn : EvsReferenceEt;
            Assert.IsTrue(htmlDoc.GetElementbyId(referenceNode).ParentNode.InnerText.Contains(expectedText));
        }

        [TestMethod]
        [DataRow("EvsForeword")]
        [DataRow("EvsIsoForeword")]
        public void AddAmendmentStatements_DummyAmendmentText_AmendmentStatementTextAdded(string templateName) {
            _template = templateName == "EvsForeword" ? EvsForewordHtml : EvsIsoForewordHtml;

            var htmlDoc = TemplateFiller.AddAmendmentStatements(_template, "EST123", "EN123");
            Assert.AreEqual("EST123", htmlDoc.GetElementbyId(EvsReferenceEt).NextSibling.InnerHtml);
            Assert.AreEqual("EN123", htmlDoc.GetElementbyId(EvsReferenceEn).NextSibling.InnerHtml);
        }

        [TestMethod]
        public void AddAmendmentDates_DummyAmendmentDateStrings_AmendmentDateAdded() {
            var htmlDoc = TemplateFiller.AddAmendmentDates(EvsForewordHtml, "EST123", "EN123");
            Assert.AreEqual("EST123", htmlDoc.GetElementbyId(EnDateOfAvailabilityEt).NextSibling.InnerHtml);
            Assert.AreEqual("EN123", htmlDoc.GetElementbyId(EnDateOfAvailabilityEn).NextSibling.InnerHtml);
        }

        [TestMethod]
        [DataRow("EvsForeword", "ICS grupp")]
        [DataRow("EvsForeword", "07.100.30")]
        [DataRow("EvsIsoForeword", "ICS grupp")]
        [DataRow("EvsIsoForeword", "07.100.30")]
        public void ReplaceIcsGroups_DummyEvsIcsGroups_IcsGroupStringsAppended(string templateName, string expectedIcs) {
            _template = templateName == "EvsForeword" ? EvsForewordHtml : EvsIsoForewordHtml;
            EvsData evsData = new EvsData { IcsGroups = new List<string> { expectedIcs } };

            var htmlDoc = TemplateFiller.ReplaceIcsGroups(_template, EvsCenIsoXml, evsData);
            var icsElement = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains(EnIcsGroups) == true).FirstOrDefault();
            Assert.AreEqual(string.Concat("ICS ", expectedIcs), icsElement.InnerText);
        }

        [TestMethod]
        public void ReplaceForewordScope_DummyEvsScopeText_ForewordScopeTextReplaced() {
            var forewordScopeNode = EvsIsoForewordHtml.GetElementbyId(EvsForewordScope);
            Assert.AreEqual(string.Empty, forewordScopeNode.InnerHtml);

            TemplateFiller.ReplaceForewordScope(EvsIsoForewordHtml, DummyEvsData.EvsIsoData);
            Assert.AreEqual("Käsitlusala test-tekst", forewordScopeNode.InnerText);

        }

        [TestMethod]
        public void ReplaceCommittees_DummyEvsCommitees_CommiteeStringsReplaced() {
            _template = EvsIsoForewordHtml;
            var evsCommittees = _template.DocumentNode.Descendants("span")
                .Where(d => d.Attributes["class"]?.Value.Contains("evs-committee") == true).ToList();
            foreach (var committee in evsCommittees) {
                Assert.AreEqual("[Committee]", committee.InnerHtml);
            }

            TemplateFiller.ReplaceCommittees(_template, DummyEvsData.EvsIsoData);
            foreach (var committee in evsCommittees) {
                Assert.AreEqual("Test-komitee", committee.InnerHtml);
            }
        }

        [TestMethod]
        public void ReplaceEvsBulletinDate_DummyEvsBulletinDate_BulletinDateStringsReplaced() {
            _template = EvsIsoForewordHtml;
            var bulletinDateEtNode = _template.GetElementbyId(EvsBulletinDateEt);
            var bulletinDateEnNode = _template.GetElementbyId(EvsBulletinDateEn);
            Assert.AreEqual("[9999. aasta jaanuarikuu]", bulletinDateEtNode.InnerHtml);
            Assert.AreEqual("[January 9999]", bulletinDateEnNode.InnerHtml);

            TemplateFiller.ReplaceBulletinDate(_template, DummyEvsData.EvsIsoData);
            Assert.AreEqual("1995. aasta oktoobrikuu", bulletinDateEtNode.InnerHtml);
            Assert.AreEqual("October 1995", bulletinDateEnNode.InnerHtml);

        }

        [TestMethod]
        [DataRow(true, "October 1995")]
        [DataRow(false, "1995. aasta oktoobrikuu")]
        public void BulletinDateToWords_DummyDateTime_DateTimeWordedString(bool isEnglish, string expected) {
            var date = new DateTime(1995, 10, 29, 0, 0, 0);
            var worded = TemplateFiller.BulletinDateToWords(date, isEnglish);
            Assert.AreEqual(expected, worded);
        }

        [TestMethod]
        [DataRow(true, "A1:2018")]
        [DataRow(false, "A1")]
        public void FindAmendmentVersion_IsoAmendment_AmendmentVersionString(bool yearNeeded, string expected) {
                var version = TemplateFiller.FindAmendmentVersion(AmdIsoXml, yearNeeded);
                Assert.AreEqual(expected, version);
        }

        [TestMethod]
        public void ReplaceReferenceWithoutYear_CenReference_ReferenceStringReplaced() {
            _template = CenTitlepageHtml;
            var referenceNode = _template.GetElementbyId(EnTitlepageHeaderReference);
            Assert.AreEqual("[EN tÃ¤his]", referenceNode.InnerHtml);

            TemplateFiller.ReplaceReferenceWithoutYear(_template, EvsCenIsoXml);
            Assert.AreEqual("EN ISO 11133", referenceNode.InnerHtml);
        }

        [TestMethod]
        public void ReplaceDateYearAndMonth_CenDate_DateStringReplaced() {
            _template = CenTitlepageHtml;
            var dateNode = _template.GetElementbyId(EnTitlepageHeaderDate);
            Assert.AreEqual("[DOA kuupÃ¤ev ja aasta]", dateNode.InnerHtml);

            TemplateFiller.ReplaceDateYearAndMonth(_template, EvsCenIsoXml, TestDate);
            Assert.AreEqual("October 1995", dateNode.InnerHtml);
        }

        [TestMethod]
        public void ReplaceSupersedes_CenSupersedes_SupersedeStringsAppended() {
            _template = CenTitlepageHtml;
            var supersedeNode = _template.GetElementbyId(EnTitlepageSupersedes);

            TemplateFiller.ReplaceSupersedes(_template, EvsCenIsoXml);
            Assert.AreEqual("Supersedes ", supersedeNode.ChildNodes[0].InnerHtml);
            Assert.AreEqual("CEN ISO/TS 11133-2:2003, ", supersedeNode.ChildNodes[1].InnerHtml);
            Assert.AreEqual("CEN ISO/TS 11133-1:2009", supersedeNode.ChildNodes[2].InnerHtml);
        }

        [TestMethod]
        [DataRow(EnTitlepageTitleEn, "[Ingliskeelne originaalpealkiri]",
            "Microbiology of food, animal feed and water - Preparation, production, storage and performance testing of culture media (ISO 11133:2014)")]
        [DataRow(EnTitlepageTitleFr, "[Prantsuskeelne pealkiri]",
            "Microbiologie des aliments, des aliments pour animaux et de l'eau - Préparation, production, stockage et essais de performance des milieux de culture (ISO 11133:2014)")]
        [DataRow(EnTitlepageTitleDe, "[Saksakeelne pealkiri]",
            "Mikrobiologie von Lebensmitteln, Futtermitteln und Wasser - Vorbereitung, Herstellung, Lagerung und Leistungsprüfung von Nährmedien (ISO 11133:2014)")]
        public void ReplaceEuropeanTitlepageTitles_CenTitles_TitleStringsReplaced(string titleNodeId,
            string initialValue, string outputValue) {
            _template = CenTitlepageHtml;
            var titleNode =_template.GetElementbyId(titleNodeId);
            Assert.AreEqual(initialValue, titleNode.InnerHtml);

            TemplateFiller.ReplaceEuropeanTitlepageTitles(_template, EvsCenIsoXml);
            Assert.AreEqual(outputValue, titleNode.InnerHtml);
        }

        [TestMethod]
        public void ReplaceSections_CenMainSectionsText_SectionsStringReplaced() {
            _template = CenTitlepageHtml;
            var sectionNode = _template.GetElementbyId(EnTitlepageSections);
            Assert.IsTrue(sectionNode.InnerText.Length < 10);

            TemplateFiller.ReplaceSections(_template, EvsCenIsoXml);
            Assert.IsTrue(sectionNode.InnerText.Length > 1000);
        }

        [TestMethod]
        public void ReplaceCopyright_CenCopyrightHolderAndStatement_CopyrightStringsGeneratedAndReplaced() {
            _template = CenTitlepageHtml;
            var copyrightHolderNode = _template.GetElementbyId(EnCopyright);
            var copyrightStatementNode = _template.GetElementbyId(EnCopyrightStatement);
            Assert.AreEqual("[Copyright]", copyrightHolderNode.InnerHtml);
            Assert.AreEqual("[Copyright statement]", copyrightStatementNode.InnerHtml);
            TemplateFiller.ReplaceCopyright(_template, EvsCenIsoXmlStandard);
            
            Assert.AreEqual("© CEN 2014", copyrightHolderNode.InnerHtml);
            Assert.AreEqual("All rights of exploitation in any form and by any means reserved worldwide for CEN national Members",
                copyrightStatementNode.InnerHtml);
        }

        [TestMethod]
        public void ReplaceReferenceNumber_CenReferenceNumber_ReferenceNumberReplaced() {
            _template = CenTitlepageHtml;
            var enReference = _template.GetElementbyId(EnReference);
            Assert.AreEqual("Ref. No. ", enReference.InnerHtml);

            TemplateFiller.ReplaceReferenceNumber(_template, EvsCenIsoXml);
            Assert.AreEqual("Ref. No. EN ISO 11133:2014 E", enReference.InnerHtml);
        }
    }
}