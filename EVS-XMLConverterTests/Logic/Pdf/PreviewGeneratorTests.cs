﻿using System.IO;
using EVS_XMLConverter.Logic.Pdf;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EVS_XMLConverterTests.Logic.Pdf {
    [TestClass]
    public class PreviewGeneratorTests {

        [TestMethod]
        public void GeneratePreview_DirectoryInfo_PreviewHtmlSavedToDirectory() {
            var directoryInfo = new DirectoryInfo(NameInfo.HtmlDirectory);
            Assert.IsFalse(File.Exists(Path.Combine(NameInfo.HtmlDirectory, "testFile.html")));

            PreviewGenerator.GeneratePreview(DataDocuments.CenIsoHtml, directoryInfo, "testFile");
            Assert.IsTrue(File.Exists(Path.Combine(NameInfo.HtmlDirectory, "testFile_preview.html")));
            File.Delete(string.Concat(directoryInfo.FullName, "testHtmlFile_preview.html"));
        }

        [TestMethod]
        public void Clone_HtmlFileAndNewLocation_HtmlClonedToNewLocation() {
            var directoryFile = string.Concat(NameInfo.HtmlDirectory, "testHtmlFile.html");
            Assert.IsFalse(File.Exists(directoryFile));

            PreviewGenerator.Clone(DataDocuments.IsoCenHtml, directoryFile);
            Assert.IsTrue(File.Exists(directoryFile));
            File.Delete(directoryFile);
        }
    }
}