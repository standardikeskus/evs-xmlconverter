﻿using System.Linq;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EVS_XMLConverterTests.Logic.Xml {
    [TestClass]
    public class EvsMetaAdderTests {
        private XElement _testElement;
        private XElement _testContainer;
        [TestInitialize]
        public void Initialize() {
            _testElement = new XElement("elementName", new XAttribute("attribute", "attributeValue"), "elementValue");
            _testContainer = new XElement("container");
        }

        [TestMethod]
        public void AddMetaToXmlTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void CreateXElementIntoXElement_XElementWithValues_ChildXElementHasGivenValue() {
            var element = EvsMetaAdder.CreateXElementIntoXElement(_testElement.Name.ToString(), _testContainer, _testElement.Value);
            Assert.AreEqual(_testContainer.Name.ToString(), element.Name);
            Assert.IsTrue(_testContainer.Descendants(_testElement.Name.ToString()).Any(e => e.Value == _testElement.Value));
        }

        [TestMethod]
        public void CreateXElementIntoXElement_XElementWithAttributes_ChildXElementHasGivenAttributeValue() {
            var element = EvsMetaAdder.CreateXElementIntoXElement(_testElement.Name.ToString(), _testContainer, 
                _testElement.Value, _testElement.FirstAttribute.Name.ToString(), _testElement.FirstAttribute.Value);
            Assert.AreEqual(_testContainer.Name, element.Name);
            Assert.IsTrue(_testContainer.Descendants(_testElement.Name.ToString()).FirstOrDefault()?
                              .Attributes(_testElement.FirstAttribute.Name.ToString())
                              .FirstOrDefault()?.Value == _testElement.FirstAttribute.Value);
        }
    }
}