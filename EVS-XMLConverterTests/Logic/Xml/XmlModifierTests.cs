﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Logic.Xml;
using EVS_XMLConverter.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Logic.Xml {
    [TestClass]
    public class XmlModifierTests {
        private List<XmlStandard> _xmlStandardList;
        [TestInitialize]
        public void Initialize() {
            CenXmlStandard.XDoc = XDocument.Load(NameInfo.CenXml, LoadOptions.PreserveWhitespace);
            IsoXmlStandard.XDoc = XDocument.Load(NameInfo.IsoXml, LoadOptions.PreserveWhitespace);
            CenIsoXmlStandard.XDoc = XDocument.Load(NameInfo.CenIsoXml, LoadOptions.PreserveWhitespace);
            IsoCenXmlStandard.XDoc = XDocument.Load(NameInfo.IsoCenXml, LoadOptions.PreserveWhitespace);
            InvalidDataXmlStandard.XDoc = XDocument.Load(NameInfo.InvalidXml, LoadOptions.PreserveWhitespace);
            AmdCenIsoXmlStandard.XDoc = XDocument.Load(NameInfo.AmdCenIsoXml, LoadOptions.PreserveWhitespace);
            AmdIsoXmlStandard.XDoc = XDocument.Load(NameInfo.AmdIsoXml, LoadOptions.PreserveWhitespace);
            EvsCenIsoXmlStandard.XDoc = XDocument.Load(NameInfo.EvsCenIsoXml, LoadOptions.PreserveWhitespace);
            _xmlStandardList = CompileXmlStandardList();
        }
        public static int GetFrontSecCount(XDocument xDoc, string secTypeValue) {
            return (int) xDoc.Descendants("front").FirstOrDefault()?.Descendants("sec")?
                .Count(d => d.Attributes("sec-type").FirstOrDefault()?.Value == secTypeValue);
        }
        public List<XmlStandard> CompileXmlStandardList() {
            var firstXmlStandard = CenIsoXmlStandard;
            firstXmlStandard.Xml = new FileInfo(NameInfo.FirstFolderXml);
            firstXmlStandard.XmlDirectory = new DirectoryInfo(NameInfo.FirstFolder);
            firstXmlStandard.XDoc = XDocument.Load(NameInfo.FirstFolderXml, LoadOptions.PreserveWhitespace);

            var secondXmlStandard = IsoCenXmlStandard;
            secondXmlStandard.Xml = new FileInfo(NameInfo.SecondFolderXml);
            secondXmlStandard.XmlDirectory = new DirectoryInfo(NameInfo.SecondFolder);
            secondXmlStandard.XDoc = XDocument.Load(NameInfo.SecondFolderXml, LoadOptions.PreserveWhitespace);

            return new List<XmlStandard> {firstXmlStandard, secondXmlStandard};
        }

        [TestCleanup]
        public void Cleanup() {
            var firstXml = new FileInfo(NameInfo.ModifiedCenIsoXml);
            var secondXml = new FileInfo(NameInfo.ModifiedIsoCenXml);
            firstXml.Delete();
            secondXml.Delete();
        }

        [TestMethod]
        public void CompileXmlStandardList_ValidDirectories_ModifiedXmlStandardListCreated() {
            var xmlStandardList = XmlModifier.CompileXmlStandardList(XmlDirectories, DummyEvsData);

            var expectedFirst = new FileInfo(NameInfo.ModifiedCenIsoXml);
            var expectedSecond = new FileInfo(NameInfo.ModifiedIsoCenXml);
            var actualFirstXml = xmlStandardList.FirstOrDefault(x => x.Xml.FullName == expectedFirst.FullName)?.Xml;
            var actualSecondXml = xmlStandardList.FirstOrDefault(x => x.Xml.FullName == expectedSecond.FullName)?.Xml;
            Assert.AreEqual(expectedFirst.FullName, actualFirstXml?.FullName);
            Assert.AreEqual(expectedSecond.FullName, actualSecondXml?.FullName);
        }

        [TestMethod]
        [DataRow("Xml")]
        [DataRow("XDoc")]
        [DataRow("EvsData")]
        public void CompileXmlStandardList_ValidDirectoriesAndEvsData_NoNullValues(string propertyName) {
            var xmlStandardList = XmlModifier.CompileXmlStandardList(XmlDirectories, DummyEvsData);
            var xmlStandard = xmlStandardList[0];
            Assert.IsNotNull(xmlStandard.GetType().GetProperty(propertyName)?.GetValue(xmlStandard));
        }

        [TestMethod]
        public void CompileXmlStandardList_ValidDirectoriesAndEvsData_SpecificValuesGiven() {
            var xmlStandardList = XmlModifier.CompileXmlStandardList(XmlDirectories, DummyEvsData);
            var firstXmlStandard = xmlStandardList[0];
            Assert.AreEqual(new FileInfo(NameInfo.ModifiedCenIsoXml).FullName, firstXmlStandard.Xml.FullName);
            Assert.AreEqual(DummyEvsData, firstXmlStandard.EvsData);
        }

        [TestMethod]
        [DataRow("XDoc")]
        [DataRow("XmlDirectory")]
        [DataRow("ReleaseDate")]
        [DataRow("MetaStructure")]
        public void PrepareXmlStandardList_ValidXmlDirectories_NoNullValues(string propertyName) {
            var directory = new DirectoryInfo(NameInfo.FirstFolder);
            var xmlStandardList = XmlModifier.PrepareXmlStandardList(new List<DirectoryInfo> { directory });
            foreach (var xmlStandard in xmlStandardList) {
                Assert.IsNotNull(xmlStandard.GetType().GetProperty(propertyName)?.GetValue(xmlStandard));
            }
        }

        [TestMethod]
        public void PrepareXmlStandardList_ValidXmlDirectories_CorrectValuesAndNoNullValues() {
            var directory = new DirectoryInfo(NameInfo.FirstFolder);
            var xmlStandardList = XmlModifier.PrepareXmlStandardList(new List<DirectoryInfo> {directory, directory});
            foreach (var xmlStandard in xmlStandardList) {
                Assert.AreEqual(directory, xmlStandard.XmlDirectory);
                Assert.AreEqual(new DateTime(2014, 5, 21), xmlStandard.ReleaseDate);
                Assert.AreEqual(ElementNames.RegMeta, xmlStandard.MetaStructure);
            }
        }

        [TestMethod]
        [DataRow(NameInfo.IsoCen, NameInfo.CenIso)]
        [DataRow(NameInfo.CenIso, NameInfo.IsoCen)]
        public void SequenceXmlStandardList_SpecificOrderXmlStandardList_OrderCorrected(string firstXmlStandard, string secondXmlStandard) {
            var xmlStandardList = XmlModifier.SequenceXmlStandardList(new List<XmlStandard>
                { GetXmlStandard(firstXmlStandard), GetXmlStandard(secondXmlStandard) });
            Assert.AreEqual(CenIsoXmlStandard, xmlStandardList[0]);
            Assert.AreEqual(IsoCenXmlStandard, xmlStandardList[1]);
        }

        [TestMethod]
        public void ModifyXmlStandardList_XmlStandardList_ModifiedXmlStandardsCreatedAndSaved() {
            var xmlStandardList = XmlModifier.ModifyXmlStandardList(_xmlStandardList);
            foreach (var xmlStandard in xmlStandardList) {
                Assert.IsTrue(xmlStandard.Xml.Name.Contains("modified-"));
                File.Delete(xmlStandard.Xml.FullName);
            }
        }

        [TestMethod]
        [DataRow(NameInfo.CenIso, "Xml")]
        [DataRow(NameInfo.CenIso, "Originator")]
        [DataRow(NameInfo.IsoCen, "Xml")]
        [DataRow(NameInfo.IsoCen, "Originator")]
        [DataRow(NameInfo.Cen, "Xml")]
        [DataRow(NameInfo.Cen, "Originator")]
        [DataRow(NameInfo.Iso, "Xml")]
        [DataRow(NameInfo.Iso, "Originator")]
        public void ModifyXmlStandard_XmlStandard_NoNullValues(string xmlStandardName, string propertyName) {
            var xmlStandard = GetXmlStandard(xmlStandardName);
            xmlStandard.Xml = null; xmlStandard.Originator = null;

            XmlModifier.ModifyXmlStandard(xmlStandard, null);
            Assert.IsNotNull(xmlStandard.GetType().GetProperty(propertyName)?.GetValue(xmlStandard));
        }

        [TestMethod]
        [DataRow(NameInfo.CenIso, "IsEuropean", true)]
        [DataRow(NameInfo.CenIso, "IsAmendment", false)]
        [DataRow(NameInfo.IsoCen, "IsEuropean", false)]
        [DataRow(NameInfo.IsoCen, "IsAmendment", false)]
        [DataRow(NameInfo.Iso, "IsEuropean", false)]
        [DataRow(NameInfo.Iso, "IsAmendment", false)]
        [DataRow(NameInfo.Cen, "IsEuropean", true)]
        [DataRow(NameInfo.Cen, "IsAmendment", false)]
        [DataRow(NameInfo.AmdCenIso, "IsAmendment", true)]
        [DataRow(NameInfo.AmdCenIso, "IsEuropean", true)]
        [DataRow(NameInfo.AmdIso, "IsAmendment", true)]
        [DataRow(NameInfo.AmdIso, "IsEuropean", false)]
        public void ModifyXmlStandard_XmlStandard_SpecificValuesGiven(string xmlStandardName, string propertyName, bool propertyValue) {
            var xmlStandard = GetXmlStandard(xmlStandardName);
            xmlStandard.IsEuropean = false; xmlStandard.IsAmendment = true;

            XmlModifier.ModifyXmlStandard(xmlStandard, null);
            Assert.AreEqual(propertyValue, xmlStandard.GetType().GetProperty(propertyName)?.GetValue(xmlStandard));
        }

        [TestMethod]
        [DataRow(NameInfo.CenIso, 0, "foreword")]
        [DataRow(NameInfo.CenIso, 1, "titlepage")]
        [DataRow(NameInfo.IsoCen, 1, "foreword")]
        [DataRow(NameInfo.IsoCen, 0, "intro")]
        public void MoveSectionsToBody_XmlStandard_SpecificSectionMovedToBody(string fileName,
            int secCount, string secTypeValue) {
            Assert.AreEqual(1, GetFrontSecCount(GetXmlStandard(fileName).XDoc, secTypeValue));

            var xmlStandard = GetXmlStandard(fileName);
            XmlModifier.MoveSectionsToBody(xmlStandard.XDoc, xmlStandard.SpecificOriginator);
            Assert.AreEqual(secCount, GetFrontSecCount(GetXmlStandard(fileName).XDoc, secTypeValue));
        }

        [TestMethod]
        public void MoveSectionsToBody_NoSections_Unchanged() {
            Assert.AreEqual(0, InvalidDataXml.Descendants("front").FirstOrDefault()?
                .Descendants("sec").Count());

            XmlModifier.MoveSectionsToBody(InvalidDataXml, null);
            Assert.AreEqual(0, InvalidDataXml.Descendants("front").FirstOrDefault()?
                .Descendants("sec").Count());
        }
    }
}