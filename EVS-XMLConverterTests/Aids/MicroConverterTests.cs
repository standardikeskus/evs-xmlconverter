﻿using System;
using System.Collections.Generic;
using System.IO;
using EVS_XMLConverter.Aids;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EVS_XMLConverterTests.Aids {
    [TestClass]
    public class MicroConverterTests {
        private DateTime _dateTimeExample;

        [TestInitialize]
        public void Initialize() {
            _dateTimeExample = new DateTime(1995, 1, 29, 0, 0, 0);
        }

        [TestMethod]
        public void TransformXmlToHtml_XmlStandardAndHtmlLocation_HtmlFileCreatedToLocationFromXml() {
            var htmlFile = Path.Combine(NameInfo.HtmlDirectory, "testHtmlFile.html");
            Assert.IsFalse(File.Exists(htmlFile));

            MicroConverter.TransformXmlToHtml(DataDocuments.IsoXmlStandard, new FileInfo(htmlFile));
            Assert.IsTrue(File.Exists(htmlFile));
            File.Delete(htmlFile);
        }

        [TestMethod]
        public void FileInfoToHtmlDoc_ExistingHtml_HtmlDocCreated() {
            var directory = NameInfo.CenIsoHtml;
            var htmlDoc = MicroConverter.FileInfoToHtmlDoc(new FileInfo(directory));
            Assert.AreEqual("html", htmlDoc.DocumentNode.FirstChild.Name);
        }

        [TestMethod]
        [DataRow("1995-01-29")]
        [DataRow("1995/01/29")]
        public void FormatDate_CustomDate_PeriodDate(string date) {
            var outputDate = MicroConverter.FormatDate(date);
            Assert.AreEqual("29.01.1995", outputDate);
        }

        [TestMethod]
        public void FormatDate_InvalidDate_ReturnNull() {
            var outputDate = MicroConverter.FormatDate("1995-1995-1995");
            Assert.AreEqual(null, outputDate);
        }

        [TestMethod]
        public void DateTimeToHyphenString_RegularDateTime_HyphenDate() {
            var dateString = MicroConverter.DateTimeToHyphenString(_dateTimeExample);
            Assert.AreEqual("1995-01-29", dateString);
        }

        [TestMethod]
        [DataRow("1995-01-29")]
        [DataRow("1995/01/29")]
        [DataRow("29.01.1995")]
        public void StringToDateTime_CustomDate_RegularDateTime(string date) {
            var actualDate = MicroConverter.StringToDateTime(date);
            Assert.AreEqual(_dateTimeExample, actualDate);
        }

        [TestMethod]
        public void StringToDateTime_InvalidDate_ConversionException() {
            var exception = Assert.ThrowsException<ConversionException>(() => MicroConverter.StringToDateTime("29\\01\\95"));
            Assert.AreEqual("Kuupäev ei ole õigel kujul", exception.Message);
        }
        
        [TestMethod]
        public void GetMonthAndYearFromDateTime_PeriodDateString_MonthAndYearString() {
            var monthAndYearString = MicroConverter.GetMonthAndYearFromDateTime(DataDocuments.TestDate);
            Assert.AreEqual("October 1995", monthAndYearString);
        }

        [TestMethod]
        public void GetYearFromString_PeriodDateString_YearString() {
            var yearString = MicroConverter.GetYearFromString("29.01.1995");
            Assert.AreEqual("1995", yearString);
        }

        [TestMethod]
        public void GetYearFromString_InvalidDateString_ConversionException() {
            var exception = Assert.ThrowsException<ConversionException>(() => MicroConverter.GetYearFromString("29\\01\\95"));
            Assert.AreEqual("Kuupäev ei ole õigel kujul", exception.Message);
        }

        [TestMethod]
        public void DateTimeToEvsMonth_AllMonthDateTimes_CorrectEstonianMonthStrings() {
            var date = _dateTimeExample;
            Assert.AreEqual("jaanuarikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("veebruarikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("märtsikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("aprillikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("maikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("juunikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("juulikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("augustikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("septembrikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("oktoobrikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("novembrikuu", MicroConverter.DateTimeToEvsMonth(date));
            date = date.AddMonths(1);
            Assert.AreEqual("detsembrikuu", MicroConverter.DateTimeToEvsMonth(date));
        }
        [TestMethod]
        public void Swap_IntegerListSwapFirstWithThird_FirstWithThirdSwapped() {
            var list = new List<int> {1, 2, 3};
            MicroConverter.Swap(list, 0, 2);
            Assert.AreEqual(3, list[0]);
            Assert.AreEqual(2, list[1]);
            Assert.AreEqual(1, list[2]);
        }
        [TestMethod]
        public void ConstructName_SpecificName_CorrectNameConstructed() {
            var expected = @"\\directory\directory\testfile_temporary.xml";
            var actual = MicroConverter.ConstructName(@"\\directory\directory", "testfile", ".xml", "_temporary");
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void CreateOrReferenceDirectoryTest() {
            Assert.Inconclusive();
        }
    }
}