﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.IO.Compression;
using EVS_XMLConverter.Aids;

namespace EVS_XMLConverterTests.Aids {
    [TestClass]
    public class ZipArchiveExtensionsTests {
        [DataRow(false)]
        [DataRow(true)]
        [TestMethod]
        public void ExtractToDirectoryTest(bool overwrite) {
            var zipDirectory = new DirectoryInfo(NameInfo.ZipDirectory).FullName;
            var zipFile = new FileInfo(Path.Combine(zipDirectory, NameInfo.TestZipFile));
            using (var zipStream = new FileStream(zipFile.FullName, FileMode.Open)) {
                new ZipArchive(zipStream, ZipArchiveMode.Update).ExtractToDirectory(zipDirectory, overwrite);
                zipStream.Close();
            }
            var expectedFile = string.Concat(NameInfo.ZipDirectory, NameInfo.TestZipContent);
            Assert.IsTrue(File.Exists(expectedFile));
            File.Delete(expectedFile);
        }
    }
}