﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests.Aids {
    [TestClass]
    public class XmlHelperTests {
        [TestInitialize]
        public void Initialize() {
            CenXml = XDocument.Load(NameInfo.CenXml, LoadOptions.PreserveWhitespace);
            IsoXml = XDocument.Load(NameInfo.IsoXml, LoadOptions.PreserveWhitespace);
            CenIsoXml = XDocument.Load(NameInfo.CenIsoXml, LoadOptions.PreserveWhitespace);
            IsoCenXml = XDocument.Load(NameInfo.IsoCenXml, LoadOptions.PreserveWhitespace);
            InvalidDataXml = XDocument.Load(NameInfo.InvalidXml, LoadOptions.PreserveWhitespace);
            AmdCenIsoXml = XDocument.Load(NameInfo.AmdCenIsoXml, LoadOptions.PreserveWhitespace);
            AmdIsoXml = XDocument.Load(NameInfo.AmdIsoXml, LoadOptions.PreserveWhitespace);
            EvsCenIsoXml = XDocument.Load(NameInfo.EvsCenIsoXml, LoadOptions.PreserveWhitespace);
        }
        public void InitializeCorruptValue(XDocument xDoc, string name) {
            var element = xDoc?.Descendants(name).FirstOrDefault();
            if (element != null) element.Name = string.Concat("corrupt-", name);
        }

        [TestMethod]
        [DataRow(true, NameInfo.Cen, NameInfo.Cen, ElementNames.Cen)]
        [DataRow(true, NameInfo.Iso, NameInfo.Iso, ElementNames.Iso)]
        [DataRow(true, NameInfo.IsoCen, NameInfo.CenIso, ElementNames.IsoCen)]
        [DataRow(true, NameInfo.CenIso, NameInfo.IsoCen, ElementNames.CenIso)]
        [DataRow(false, NameInfo.Cen, NameInfo.Cen, ElementNames.Cen)]
        [DataRow(false, NameInfo.Iso, NameInfo.Iso, ElementNames.Iso)]
        [DataRow(false, NameInfo.IsoCen, NameInfo.CenIso, ElementNames.Iso)]
        [DataRow(false, NameInfo.CenIso, NameInfo.IsoCen, ElementNames.Cen)]
        public void GetOriginator_CurrentAndPreviousXDocument_CurrentOriginator(bool specific, string fileName, string previousFileName, string expectedOriginator) {
            var expected = Originator.GenerateOriginator(expectedOriginator, specific);
            var actual = XmlHelper.GetOriginator(GetXmlStandard(fileName), GetXmlStandard(previousFileName), specific);
            Assert.Inconclusive();
            //Lisada test-fail kus on Cen/Clc
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(true, false, true)]
        [DataRow(false, true, true)]
        [DataRow(true, true, false)]
        [DataRow(false, false, false)]
        public void CheckIfEnIso_TwoMetaElements_CorrectBooleanValueReturned(bool first, bool second, bool expected) {
            var firstXmlStandard = new XmlStandard() { IsEuropean = first };
            var secondXmlStanrad = new XmlStandard() { IsEuropean = second };

            var actual = XmlHelper.CheckIfEnIso(firstXmlStandard, secondXmlStanrad);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(NameInfo.CenIso, ElementNames.Cen)]
        [DataRow(NameInfo.Cen, ElementNames.Cen)]
        [DataRow(NameInfo.Iso, ElementNames.Iso)]
        [DataRow(NameInfo.AmdCenIso, ElementNames.Cen)]
        [DataRow(NameInfo.AmdIso, ElementNames.Iso)]
        public void GetOriginatorValue_MetaElement_MetaElementOriginatorValueReturned(string xmlStandardName, string expected) {
            var xmlStandard = GetXmlStandard(xmlStandardName);
            var metaElement = xmlStandard.XDoc.Descendants(xmlStandard.MetaStructure).FirstOrDefault();
            var actual = XmlHelper.GetOriginatorValue(metaElement);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow(true, ElementNames.Cen, true, ElementNames.CenIso)]
        [DataRow(true, ElementNames.Iso, true, ElementNames.IsoCen)]
        [DataRow(true, ElementNames.CenClc, true, ElementNames.CenClc)]
        [DataRow(false, ElementNames.Cen, true, ElementNames.CenIso)]
        [DataRow(false, ElementNames.Iso, true, ElementNames.IsoCen)]
        [DataRow(false, ElementNames.Cen, false, ElementNames.Cen)]
        [DataRow(false, ElementNames.Clc, false, ElementNames.Clc)]
        [DataRow(false, ElementNames.Iso, false, ElementNames.Iso)]
        [DataRow(false, ElementNames.CenClc, false, ElementNames.CenClc)]
        [DataRow(false, "Unknown", false, ElementNames.Unknown)]
        public void DetermineOriginator_OriginatorValue_CorrectOriginatorObjectReturned(bool specific, string originatorValue, bool checkEnIso, string expected) {
            if (originatorValue == "Unknown") {
                var exception = Assert.ThrowsException<ConversionException>(() => XmlHelper.DetermineOriginator(originatorValue, checkEnIso, specific));
                Assert.AreEqual("Probleem standardimisorganisatsiooni tuvastamisega: Unknown", exception.Message);
            }
            else {
                var actual = XmlHelper.DetermineOriginator(originatorValue, checkEnIso, specific);
                Assert.AreEqual(Originator.GenerateOriginator(expected, specific), actual);
            }
        }

        [TestMethod]
        [DataRow(NameInfo.Cen, ElementNames.RegMeta)]
        [DataRow(NameInfo.Iso, ElementNames.IsoMeta)]
        [DataRow(NameInfo.CenIso, ElementNames.RegMeta)]
        [DataRow(NameInfo.IsoCen, ElementNames.IsoMeta)]
        //needs stdmeta and natmeta test files
        public void GetMetaStructure_XDocument_MetaStructureString(string fileName, string expected) {
            Assert.AreEqual(expected, XmlHelper.GetMetaStructure(GetXDocument(fileName)));
        }

        [TestMethod]
        public void GetMetaStructure_InvalidXDocument_ConversionException() {
            InitializeCorruptValue(InvalidDataXml, ElementNames.IsoMeta);
            var exception = Assert.ThrowsException<ConversionException>(() => XmlHelper.GetMetaStructure(InvalidDataXml));
            Assert.AreEqual("XML faili metastruktuur ei ole äratuntav", exception.Message);
        }

        [TestMethod]
        [DataRow(NameInfo.Cen, false)]
        [DataRow(NameInfo.Iso, false)]
        [DataRow(NameInfo.CenIso, false)]
        [DataRow(NameInfo.IsoCen, false)]
        [DataRow(NameInfo.AmdCenIso, true)]
        [DataRow(NameInfo.AmdIso, true)]
        public void CheckIfAmendment_XDocument_TrueOrFalse(string fileName, bool expected) {
            Assert.AreEqual(expected, XmlHelper.CheckIfAmendment(GetXDocument(fileName)));
        }

        [TestMethod]
        [DataRow(NameInfo.Cen, "")]
        [DataRow(NameInfo.Iso, "© ISO 2004 - All rights reserved")]
        [DataRow(NameInfo.CenIso, "")]
        [DataRow(NameInfo.IsoCen, "© ISO 2014 - All rights reserved")]
        [DataRow(NameInfo.AmdCenIso, "")]
        [DataRow(NameInfo.AmdIso, "© ISO 2018 - All rights reserved")]
        public void GetCopyrightNoticeForIso_XDocument_CopyrightStatementString(string fileName, string expected) {
            Assert.AreEqual(expected, XmlHelper.GetCopyrightNoticeForIso(GetXmlStandard(fileName)));
        }
        
        [TestMethod]
        public void GetCopyrightNoticeForIso_XDocumentWithoutNotice_EmptyString() {
            InvalidDataXml.Descendants("copyright-holder").Remove();
            InvalidDataXml.Descendants("copyright-year").Remove();
            InvalidDataXml.Descendants("copyright-statement").Remove();

            Assert.AreEqual(string.Empty, XmlHelper.GetCopyrightNoticeForIso(InvalidDataXmlStandard));
        }

        [TestMethod]
        [DataRow(NameInfo.Cen, "© 2018 CEN")]
        [DataRow(NameInfo.Iso, "© ISO 2004")]
        [DataRow(NameInfo.CenIso, "© 2014 CEN")]
        [DataRow(NameInfo.IsoCen, "© ISO 2014")]
        [DataRow(NameInfo.AmdCenIso, "© 2018 CEN")]
        [DataRow(NameInfo.AmdIso, "© ISO 2018")]
        public void GetCopyrightHolder_XDocument_CopyrightHolderString(string fileName, string expected) {
            Assert.AreEqual(expected, XmlHelper.GetCopyrightHolder(GetXmlStandard(fileName)));
        }
        
        [TestMethod]
        public void GetCopyrightHolder_XDocumentWithoutHolder_EmptyString() {
            InvalidDataXml.Descendants("copyright-holder").Remove();
            InvalidDataXml.Descendants("copyright-year").Remove();

            Assert.AreEqual(string.Empty, XmlHelper.GetCopyrightHolder(InvalidDataXmlStandard));
        }

        [TestMethod]
        [DataRow(NameInfo.Cen,
            "All rights of exploitation in any form and by any means reserved worldwide for CEN national Members")]
        [DataRow(NameInfo.Iso, "All rights reserved")]
        [DataRow(NameInfo.CenIso,
            "All rights of exploitation in any form and by any means reserved worldwide for CEN national Members")]
        [DataRow(NameInfo.IsoCen, "All rights reserved")]
        [DataRow(NameInfo.AmdCenIso,
            "All rights of exploitation in any form and by any means reserved worldwide for CEN national Members")]
        [DataRow(NameInfo.AmdIso, "All rights reserved")]
        public void GetCopyrightStatement_CenXmls_CenCopyrightStatementStrings(string fileName, string expected) {
            Assert.AreEqual(expected, XmlHelper.GetCopyrightStatement(GetXmlStandard(fileName)));
        }

        [TestMethod]
        public void GetCopyrightStatement_XDocumentWithoutStatement_EmptyString() {
            InvalidDataXml.Descendants("copyright-statement").Remove();

            Assert.AreEqual(string.Empty, XmlHelper.GetCopyrightStatement(InvalidDataXmlStandard));
        }

        [TestMethod]
        public void CheckIfPureIso_IsoXDocuments_True() {
            var isoXmlList = new List<XmlStandard> { IsoXmlStandard, AmdIsoXmlStandard };
            Assert.IsTrue(XmlHelper.CheckIfPureIso(isoXmlList));
        }

        [TestMethod]
        public void CheckIfPureIso_CenAndIsoXDocuments_False() {
            var cenXmlList = new List<XmlStandard> { CenIsoXmlStandard, IsoCenXmlStandard };
            Assert.IsFalse(XmlHelper.CheckIfPureIso(cenXmlList));
        }

        [TestMethod]
        public void FindXDocFromDirectory_CenIsoXmlDirectory_CenIsoXmlDocument() {
            var directory = new DirectoryInfo(NameInfo.FirstFolder);
            var expected = XDocument.Load(NameInfo.FirstFolderXml, LoadOptions.PreserveWhitespace).ToString();
            var actual = XmlHelper.FindXDocFromDirectory(directory).ToString();
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void FindXDocFromDirectory_EmptyDirectory_ConversionException() {
            var directory = new DirectoryInfo(NameInfo.EmptyFolder);
            var exception =
                Assert.ThrowsException<ConversionException>(() => XmlHelper.FindXDocFromDirectory(directory).ToString());
            Assert.AreEqual("Vähemalt ühes kaustas puudub XML fail", exception.Message);
        }

        [TestMethod]
        public void FindEvsMeta_EvsXDocument_EvsMetaElement() {
            var evsMeta = new XElement(ElementNames.NatMeta, new XAttribute("originator", ElementNames.Evs));
            Assert.AreEqual(evsMeta.Name, XmlHelper.FindEvsMeta(EvsCenIsoXml).Name);
            Assert.AreEqual(evsMeta.Attribute("originator")?.Value,
                XmlHelper.FindEvsMeta(EvsCenIsoXml).Attribute("originator")?.Value);
        }

        [TestMethod]
        public void FindEvsMeta_CenXDocument_ConversionException() {
            var exception = Assert.ThrowsException<ConversionException>(() => XmlHelper.FindEvsMeta(CenXml));
            Assert.AreEqual("Peamisel XML failil puuduvad Eesti standardi metaandmed", exception.Message);
        }
        
        [TestMethod]
        public void FindOriginalMeta_CenXDocument_RegMetaElement() {
            var originalCenMeta = new XElement(ElementNames.RegMeta, new XAttribute("originator", ElementNames.Cen));
            Assert.AreEqual(originalCenMeta.Name, XmlHelper.FindOriginalMeta(CenXml).Name);
            Assert.AreEqual(originalCenMeta.Attribute("originator")?.Value,
                XmlHelper.FindOriginalMeta(CenXml).Attribute("originator")?.Value);
        }

        [TestMethod]
        public void FindOriginalMeta_IsoXDocument_IsoMetaElement() {
            var originalIsoMeta = new XElement(ElementNames.IsoMeta);
            Assert.AreEqual(originalIsoMeta.Name, XmlHelper.FindOriginalMeta(IsoXml).Name);
            Assert.AreEqual(originalIsoMeta.Attribute("originator")?.Value,
                XmlHelper.FindOriginalMeta(IsoXml).Attribute("originator")?.Value);
        }

        [TestMethod]
        public void FindOriginalMeta_InvalidXml_ConversionException() {
            InitializeCorruptValue(InvalidDataXml, ElementNames.IsoMeta);
            var exception = Assert.ThrowsException<ConversionException>(() => XmlHelper.FindOriginalMeta(InvalidDataXml));
            Assert.AreEqual("XML faili metastruktuur ei ole äratuntav", exception.Message);
        }

        [TestMethod]
        [DataRow(NameInfo.Cen,
            "Tanks for transport of dangerous goods - Digital interface for product recognition devices for liquid fuels")]
        [DataRow(NameInfo.Iso, 
            "Dentistry — Polymer-based crown and bridge materials")]
        [DataRow(NameInfo.CenIso,
            "Microbiology of food, animal feed and water - Preparation, production, storage and performance testing of culture media (ISO 11133:2014)")]
        [DataRow(NameInfo.IsoCen,
            "Microbiology of food, animal feed and water — Preparation, production, storage and performance testing of culture media")]
        [DataRow(NameInfo.AmdCenIso,
            "Microbiology of food, animal feed and water - Preparation, production, storage and performance testing of culture media - Amendment 1 (ISO 11133:2014/Amd 1:2018)")]
        [DataRow(NameInfo.AmdIso,
            "Microbiology of food, animal feed and water — Preparation, production, storage and performance testing of culture media AMENDMENT 1")]
        public void GetTitleString_XDocument_TitleString(string fileName, string expected) {
            var titleString = XmlHelper.GetTitleString(XmlHelper.FindOriginalMeta(GetXDocument(fileName)), ElementNames.English);
            Assert.AreEqual(expected, titleString);
        }

        [TestMethod]
        [DataRow(NameInfo.Cen, "27.06.2018")]
        [DataRow(NameInfo.Iso, "01.10.2004")]
        [DataRow(NameInfo.CenIso, "21.05.2014")]
        [DataRow(NameInfo.IsoCen, "15.05.2014")]
        [DataRow(NameInfo.AmdCenIso, "07.03.2018")]
        [DataRow(NameInfo.AmdIso, "28.02.2018")]
        public void GetReleaseDate_XDocument_ReleaseDateStrings(string fileName, string expected) {
            Assert.AreEqual(expected, XmlHelper.GetReleaseDate(GetXDocument(fileName)));
        }
        
        [TestMethod]
        public void GetReleaseDate_NotExist_ConversionException() {
            InvalidDataXml.Descendants("pub-date").Remove();
            InvalidDataXml.Descendants("release-date").Remove();
            var exception = Assert.ThrowsException<ConversionException>(() => XmlHelper.GetReleaseDate(InvalidDataXml));
            Assert.AreEqual("XML failis puudub avaldamise kuupäev", exception.Message);
        }

        [TestMethod]
        [DataRow(-1, true)]
        [DataRow(0, false)]
        [DataRow(1, true)]
        [DataRow(2, false)]
        public void IsOdd_OddNumbers_True(int number, bool expected) {
            Assert.AreEqual(expected, XmlHelper.IsOdd(number));
        }

        [TestMethod]
        public void RemoveYearFromEvsReference_EvsReference_EvsReferenceWithoutYear() {
            Assert.AreEqual("EVS-EN ISO 10077-1", XmlHelper.RemoveYearFromEvsReference("EVS-EN ISO 10077-1:2017"));
        }
    }
}