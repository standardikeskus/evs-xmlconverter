﻿using System.Collections.Generic;
using System.IO;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static EVS_XMLConverter.Aids.HtmlHelper;

namespace EVS_XMLConverterTests.Aids {
    [TestClass]
    public class HtmlHelperTests {
        private HtmlDocument _htmlDoc;
        private HtmlNode _mainDiv;
        private HtmlNode _mainTable;
        [TestInitialize]
        public void Initialize() {
            _htmlDoc = new HtmlDocument();
            _mainDiv = _htmlDoc.CreateElement("div");
            _htmlDoc.DocumentNode.AppendChild(_mainDiv);
            _mainTable = _htmlDoc.CreateElement("table");
            _mainTable.Attributes.Add("class", "sts-table");
            _htmlDoc.DocumentNode.AppendChild(_mainTable);
        }

        private HtmlNode _childDiv;
        private HtmlNode _childTh;
        public void InitializeDescendants() {
            _childDiv = _htmlDoc.CreateElement("div");
            _mainDiv.AppendChild(_childDiv);
            _childTh = _htmlDoc.CreateElement("th");
            _mainTable.AppendChild(_childTh);
        }

        [TestMethod]
        public void AddAttributeToNode_NewAttribute_AttributeAdded() {
            AddAttributeToNode(_mainDiv, "style", "TestStyle1");
            Assert.AreEqual("TestStyle1", _mainDiv.Attributes["style"].Value);
        }

        [TestMethod]
        public void AddAttributeToNode_ExistingAttribute_AttributeAppended() {
            _mainDiv.Attributes.Add("class", "TestClass1");
            AddAttributeToNode(_mainDiv, "class", "TestClass2");
            Assert.AreEqual("TestClass1 TestClass2", _mainDiv.Attributes["class"].Value);
        }

        [TestMethod]
        public void AddAttributeToNode_StyleAttributeWithoutClosingTag_AttributeAdded() {
            AddAttributeToNode(_mainDiv, "style", "TestStyle1");
            AddAttributeToNode(_mainDiv, "style", "TestStyle2");
            Assert.AreEqual("TestStyle1; TestStyle2", _mainDiv.Attributes["style"].Value);
        }

        [TestMethod]
        public void FindNodesByClassNameTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FindNodesByIdTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void GetNodesDescendants_NoNodes_CountZero() {
            var descendants = GetNodesDescendants(_htmlDoc, new List<string> { "sup" });
            Assert.AreEqual(0, descendants.Count);
        }

        [TestMethod]
        public void GetNodesDescendants_NoDescendants_CountZero() {
            var descendants = GetNodesDescendants(_htmlDoc, new List<string> { "div" });
            Assert.AreEqual(0, descendants.Count);
        }

        [TestMethod]
        public void GetNodesDescendants_DescendantsExist_ListOfAllDescendants() {
            InitializeDescendants();
            var descendants = GetNodesDescendants(_htmlDoc, new List<string> { "div", "table" });
            Assert.IsTrue(descendants.Contains(_childDiv));
            Assert.IsTrue(descendants.Contains(_childTh));
        }

        [TestMethod]
        public void CheckIfInTable_InTable_True() {
            InitializeDescendants();
            Assert.IsTrue(CheckIfInTable(_childTh));
        }

        [TestMethod]
        public void CheckIfInTable_NotInTable_False() {
            InitializeDescendants();
            Assert.IsFalse(CheckIfInTable(_childDiv));
        }

        [TestMethod]
        public void SaveHtml_ValidLocation_SavedToLocation() {
            var htmlLocation = Path.Combine(NameInfo.HtmlDirectory, "testHtmlFile.html");
            SaveHtml(_htmlDoc, htmlLocation);
            Assert.IsTrue(File.Exists(htmlLocation));
            File.Delete(htmlLocation);
        }

        [TestMethod]
        public void FindImageTests() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void TrimStart_WordsAtStart_WordAtStartRemoved() {
            var target = "Test Test String";
            var trimString = "Test ";
            var actual = TrimStart(target, trimString);
            Assert.AreEqual("String", actual);
        }

        [TestMethod]
        public void FindNextSiblingTest() {
            Assert.Inconclusive();
        }

        [TestMethod]
        public void FindPreviousSiblingTest() {
            Assert.Inconclusive();
        }
    }
}