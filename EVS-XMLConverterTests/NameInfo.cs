﻿using System;
using System.Collections.Generic;
using System.IO;

namespace EVS_XMLConverterTests {
    public class NameInfo {
        //XML
        public const string XmlDirectory = @"..\..\TestData\Xml";
        public static readonly string IsoXml = Path.Combine(XmlDirectory, "ISO-XML.xml");
        public static readonly string CenXml = Path.Combine(XmlDirectory, "CEN-XML.xml");
        public static readonly string CenIsoXml = Path.Combine(XmlDirectory, "CEN-ISO-XML.xml");
        public static readonly string IsoCenXml = Path.Combine(XmlDirectory, "ISO-CEN-XML.xml");
        public static readonly string AmdCenIsoXml = Path.Combine(XmlDirectory, "AMD-CEN-ISO-XML.xml");
        public static readonly string AmdIsoXml = Path.Combine(XmlDirectory, "AMD-ISO-XML.xml");
        public static readonly string Amd2IsoXml = Path.Combine(XmlDirectory, "AMD2-ISO-XML.xml");
        public static readonly string InvalidXml = Path.Combine(XmlDirectory, "INVALID-DATA-XML.xml");
        public static readonly string EvsCenIsoXml = Path.Combine(XmlDirectory, "EVS-CEN-ISO-XML.xml");

        public static readonly string ModifiedCenIsoXml = Path.Combine(XmlDirectory, "XmlFolders", "XML1", "modified-CEN-ISO-XML.xml");
        public static readonly string ModifiedIsoCenXml = Path.Combine(XmlDirectory, "XmlFolders", "XML2", "modified-ISO-CEN-XML.xml");
        
        public static readonly string FirstFolder = Path.Combine(XmlDirectory, "XmlFolders", "XML1");
        public static readonly string FirstFolderXml = Path.Combine(XmlDirectory, "XmlFolders", "XML1", "CEN-ISO-XML.xml");
        public static readonly string SecondFolder = Path.Combine(XmlDirectory, "XmlFolders", "XML2");
        public static readonly string SecondFolderXml = Path.Combine(XmlDirectory, "XmlFolders", "XML2", "ISO-CEN-XML.xml");
        public static readonly string EmptyFolder = Path.Combine(XmlDirectory, "XmlFolders", "XML3-Empty");
        public const string GraphicsFolderName = @"\graphics\";

        public const bool IsEuropeanIsoCen = true;
        public const int IsoCenYear = 2014;
        public const int OldYear = 2009;

        //HTML
        public const string IsoCenRegularImageName = "fig_B.1";
        public const string IsoCenRegularImageId = "toc_iso_std_iso_11133_ed-1_v2_en_fig_B.1";
        public static readonly string IsoCenRegularImageDirectory = Path.Combine(XmlDirectory, GraphicsFolderName, "fig_B.1.png");
        public static readonly string IsoCenFirstMathImageDirectory = Path.Combine(XmlDirectory, GraphicsFolderName, "mml_m1.png");
        public const string IsoCenFirstMathImageId = "mml_m1";
        public const string IsoCenSecondMathImageId = "mml_m2";
        
        public const string IsoCenTableDImageId = "toc_iso_std_iso_11133_ed-1_v2_en_tab_D.1";
        public const string IsoCenTableEImageId = "toc_iso_std_iso_11133_ed-1_v2_en_tab_E.1";
        public const string IsoCenTableE2ImageId = "toc_iso_std_iso_11133_ed-1_v2_en_tab_E.2";
        public static readonly string IsoCenTableDImageDirectory = Path.Combine(XmlDirectory, GraphicsFolderName, "tab_D.1.png");

        public const string TestDataDirectory = @"..\..\TestData";
        public const string HtmlDirectory = @"..\..\TestData\Html";
        public static readonly string IsoCenHtml = Path.Combine(HtmlDirectory, "ISO-CEN-HTML.html");
        public static readonly string CenIsoHtml = Path.Combine(HtmlDirectory, "CEN-ISO-HTML.html");

        //ZIP
        public const string ZipDirectory = @"..\..\TestData\Zip\";
        public const string TestZipFile = @"ZipTest.zip";
        public const string TestZipContent = @"test_text.txt";

        //templates
        public const string Templates = @"\\evs.ee\Data\Kataloog\XMLFiles\Assets\templates\";
        public static readonly string EvsTitlepageHtml = Path.Combine(Templates, "EVS_titlepage.html");
        public static readonly string CenTitlepageHtml = Path.Combine(Templates, "CEN_titlepage.html");
        public static readonly string EvsForewordHtml = Path.Combine(Templates, "EVS_foreword.html");
        public static readonly string EvsIsoForewordHtml = Path.Combine(Templates, "EVS_ISO_foreword.html");

        //test files
        public const string Iso = "Iso";
        public const string Cen = "Cen";
        public const string CenIso = "CenIso";
        public const string IsoCen = "IsoCen";
        public const string Invalid = "Invalid";
        public const string AmdCenIso = "AmdCenIso";
        public const string AmdIso = "AmdIso";
        public const string Amd2Iso = "Amd2Iso";
        public const string EvsCenIso = "EvsCenIso";

        public const string EvsTitlepage = "EvsTitlepage";
        public const string CenTitlepage = "CenTitlepage";
        public const string EvsForeword = "EvsForeword";
        public const string EvsIsoForeword = "EvsIsoForeword";

        //test values
        public const string TestFileName = "faili_nimetus_test";
        public const string EtTitle = "Eestikeelne test-pealkiri";
        public const string EnTitle = "Ingliskeelne test-pealkiri";
        public const string EvsReference = "EVS test-tähis";
        public static List<string> IcsGroups = new List<string> {"ICS test-grupp1", "ICS grupp test-grupp2", "ICS grupp test-grupp3"};
        public static DateTime BulletinIssue = new DateTime(1995, 10, 29);
        public const string Scope = "Käsitlusala test-tekst";
        public const string Amendments = "Muudatuste test-tekst";
        public const string Committee = "Test-komitee";
        
        public const string Evs = "EVS";
    }
}
