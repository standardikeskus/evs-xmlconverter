﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;

namespace EVS_XMLConverterTests {
    public class DataDocuments {
        //XDocuments
        public static XDocument CenXml { get; set; }
        public static XDocument IsoXml { get; set; }
        public static XDocument CenIsoXml { get; set; }
        public static XDocument IsoCenXml { get; set; }
        public static XDocument InvalidDataXml { get; set; }
        public static XDocument AmdCenIsoXml { get; set; }
        public static XDocument AmdIsoXml { get; set; }
        public static XDocument Amd2IsoXml { get; set; }
        public static XDocument EvsCenIsoXml { get; set; }

        public static XDocument GetXDocument(string originator) {
            switch (originator) {
                case NameInfo.Cen:
                    return CenXml;
                case NameInfo.Iso:
                    return IsoXml;
                case NameInfo.CenIso:
                    return CenIsoXml;
                case NameInfo.IsoCen:
                    return IsoCenXml;
                case NameInfo.Invalid:
                    return InvalidDataXml;
                case NameInfo.AmdCenIso:
                    return AmdCenIsoXml;
                case NameInfo.AmdIso:
                    return AmdIsoXml;
                case NameInfo.Amd2Iso:
                    return Amd2IsoXml;
                case NameInfo.EvsCenIso:
                    return EvsCenIsoXml;
                default:
                    return null;
            }
        }
        
        //XmlStandards
        public static XmlStandard CenXmlStandard { get; set; }
        public static XmlStandard IsoXmlStandard { get; set; }
        public static XmlStandard CenIsoXmlStandard { get; set; }
        public static XmlStandard IsoCenXmlStandard { get; set; }
        public static XmlStandard InvalidDataXmlStandard { get; set; }
        public static XmlStandard AmdCenIsoXmlStandard { get; set; }
        public static XmlStandard AmdIsoXmlStandard { get; set; }
        public static XmlStandard Amd2IsoXmlStandard { get; set; }
        public static XmlStandard EvsCenIsoXmlStandard { get; set; }

        public static XmlStandard GetXmlStandard(string xmlStandard) {
            switch (xmlStandard) {
                case NameInfo.Cen:
                    return CenXmlStandard;
                case NameInfo.Iso:
                    return IsoXmlStandard;
                case NameInfo.CenIso:
                    return CenIsoXmlStandard;
                case NameInfo.IsoCen:
                    return IsoCenXmlStandard;
                case NameInfo.Invalid:
                    return InvalidDataXmlStandard;
                case NameInfo.AmdCenIso:
                    return AmdCenIsoXmlStandard;
                case NameInfo.AmdIso:
                    return AmdIsoXmlStandard;
                case NameInfo.Amd2Iso:
                    return Amd2IsoXmlStandard;
                case NameInfo.EvsCenIso:
                    return EvsCenIsoXmlStandard;
                default:
                    return null;
            }
        }

        //HtmlDocuments
        public static HtmlDocument IsoCenHtml { get; set; }
        public static HtmlDocument CenIsoHtml { get; set; }
        public static HtmlDocument EvsTitlepageHtml { get; set; }
        public static HtmlDocument CenTitlepageHtml { get; set; }
        public static HtmlDocument EvsForewordHtml { get; set; }
        public static HtmlDocument EvsIsoForewordHtml { get; set; }
        public static HtmlDocument GetHtmlDocument(string originator) {
            switch (originator) {
                case NameInfo.CenIso:
                    return CenIsoHtml;
                case NameInfo.IsoCen:
                    return IsoCenHtml;
                case NameInfo.EvsTitlepage:
                    return EvsTitlepageHtml;
                case NameInfo.CenTitlepage:
                    return CenTitlepageHtml;
                case NameInfo.EvsForeword:
                    return EvsForewordHtml;
                case NameInfo.EvsIsoForeword:
                    return EvsIsoForewordHtml;
                default:
                    return null;
            }
        }

        //Folders and Other Data
        public static DirectoryInfo FirstFolder { get; set; }
        public static DirectoryInfo SecondFolder { get; set; }

        public static List<DirectoryInfo> XmlDirectories { get; set; }
        public static EvsData DummyEvsData { get; set; }
        public static DateTime TestDate { get; set; }
    }
}
