﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using TestContext = Microsoft.VisualStudio.TestTools.UnitTesting.TestContext;
using static EVS_XMLConverter.Aids.MicroConverter;
using static EVS_XMLConverterTests.DataDocuments;

namespace EVS_XMLConverterTests {
    [TestClass]
    [SetUpFixture]
    public class AssemblyInitialize {
        [AssemblyInitialize]
        public static void InitAssembly(TestContext testContext) {
            InitAssembly(testContext.DeploymentDirectory);
        }
        private static void InitAssembly(string deploymentDirectory) {
            CenXml = XDocument.Load(NameInfo.CenXml, LoadOptions.PreserveWhitespace);
            IsoXml = XDocument.Load(NameInfo.IsoXml, LoadOptions.PreserveWhitespace);
            CenIsoXml = XDocument.Load(NameInfo.CenIsoXml, LoadOptions.PreserveWhitespace);
            IsoCenXml = XDocument.Load(NameInfo.IsoCenXml, LoadOptions.PreserveWhitespace);
            InvalidDataXml = XDocument.Load(NameInfo.InvalidXml, LoadOptions.PreserveWhitespace);
            AmdCenIsoXml = XDocument.Load(NameInfo.AmdCenIsoXml, LoadOptions.PreserveWhitespace);
            AmdIsoXml = XDocument.Load(NameInfo.AmdIsoXml, LoadOptions.PreserveWhitespace);
            Amd2IsoXml = XDocument.Load(NameInfo.Amd2IsoXml, LoadOptions.PreserveWhitespace);
            EvsCenIsoXml = XDocument.Load(NameInfo.EvsCenIsoXml, LoadOptions.PreserveWhitespace);

            FirstFolder = new DirectoryInfo(NameInfo.FirstFolder);
            SecondFolder = new DirectoryInfo(NameInfo.SecondFolder);

            XmlDirectories = new List<DirectoryInfo> { FirstFolder, SecondFolder };
            DummyEvsData = new EvsData {
                FileName = NameInfo.TestFileName,
                EtTitle = NameInfo.EtTitle,
                EnTitle = NameInfo.EnTitle,
                EvsReference = NameInfo.EvsReference,
                IcsGroups = NameInfo.IcsGroups,
                StaDISOrganisation = NameInfo.Evs,
                EvsIsoData = new EvsIsoData {
                    BulletinIssue = NameInfo.BulletinIssue,
                    Scope = NameInfo.Scope,
                    Amendments = NameInfo.Amendments,
                    Committee = NameInfo.Committee
                }
            };

            TestDate = new DateTime(1995, 10, 29);
            
            CenXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = false,
                IsEuropean = true,
                IsPureIso = false,
                MetaStructure = ElementNames.RegMeta,
                Originator = Originator.Cen,
                SpecificOriginator = Originator.Cen,
                ReleaseDate = TestDate,
                XDoc = CenXml,
                Xml = new FileInfo(NameInfo.CenXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            IsoXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = false,
                IsEuropean = false,
                IsPureIso = true,
                MetaStructure = ElementNames.IsoMeta,
                Originator = Originator.Iso,
                SpecificOriginator = Originator.Iso,
                ReleaseDate = TestDate,
                XDoc = IsoXml,
                Xml = new FileInfo(NameInfo.IsoXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            CenIsoXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = false,
                IsEuropean = true,
                IsPureIso = false,
                MetaStructure = ElementNames.RegMeta,
                Originator = Originator.Cen,
                SpecificOriginator = Originator.CenIso,
                ReleaseDate = TestDate,
                XDoc = CenIsoXml,
                Xml = new FileInfo(NameInfo.CenIsoXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            IsoCenXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = false,
                IsEuropean = false,
                IsPureIso = false,
                MetaStructure = ElementNames.IsoMeta,
                Originator = Originator.Iso,
                SpecificOriginator = Originator.IsoCen,
                ReleaseDate = TestDate,
                XDoc = IsoCenXml,
                Xml = new FileInfo(NameInfo.IsoCenXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            InvalidDataXmlStandard = new XmlStandard {
                XDoc = InvalidDataXml
            };
            AmdCenIsoXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = true,
                IsEuropean = true,
                IsPureIso = false,
                MetaStructure = ElementNames.RegMeta,
                Originator = Originator.Cen,
                SpecificOriginator = Originator.CenIso,
                ReleaseDate = TestDate,
                XDoc = AmdCenIsoXml,
                Xml = new FileInfo(NameInfo.AmdCenIsoXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            AmdIsoXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = true,
                IsEuropean = false,
                IsPureIso = true,
                MetaStructure = ElementNames.IsoMeta,
                Originator = Originator.Iso,
                SpecificOriginator = Originator.Iso,
                ReleaseDate = TestDate,
                XDoc = AmdIsoXml,
                Xml = new FileInfo(NameInfo.AmdIsoXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            Amd2IsoXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = true,
                IsEuropean = false,
                IsPureIso = true,
                MetaStructure = ElementNames.IsoMeta,
                Originator = Originator.Iso,
                SpecificOriginator = Originator.Iso,
                ReleaseDate = TestDate,
                XDoc = Amd2IsoXml,
                Xml = new FileInfo(NameInfo.Amd2IsoXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };
            EvsCenIsoXmlStandard = new XmlStandard {
                EvsData = DummyEvsData,
                IsAmendment = true,
                IsEuropean = false,
                IsPureIso = true,
                MetaStructure = ElementNames.RegMeta,
                Originator = Originator.Evs,
                SpecificOriginator = Originator.Evs,
                ReleaseDate = TestDate,
                XDoc = EvsCenIsoXml,
                Xml = new FileInfo(NameInfo.EvsCenIsoXml),
                XmlDirectory = new DirectoryInfo(NameInfo.XmlDirectory),
                ImageDirectory = new DirectoryInfo(string.Concat(NameInfo.XmlDirectory, NameInfo.GraphicsFolderName))
            };

            IsoCenHtml = Transform(IsoCenXmlStandard, NameInfo.IsoCenHtml);
            CenIsoHtml = Transform(IsoCenXmlStandard, NameInfo.CenIsoHtml);

            EvsTitlepageHtml = new HtmlDocument();
            EvsTitlepageHtml.Load(NameInfo.EvsTitlepageHtml);

            CenTitlepageHtml = new HtmlDocument();
            CenTitlepageHtml.Load(NameInfo.CenTitlepageHtml);

            EvsForewordHtml = new HtmlDocument();
            EvsForewordHtml.Load(NameInfo.EvsForewordHtml);

            EvsIsoForewordHtml = new HtmlDocument();
            EvsIsoForewordHtml.Load(NameInfo.EvsIsoForewordHtml);
        }
        public static HtmlDocument Transform(XmlStandard xmlStandard, string htmlLocation) {
            return FileInfoToHtmlDoc(TransformXmlToHtml(xmlStandard,
                new FileInfo(htmlLocation)));
        }
    }
}
