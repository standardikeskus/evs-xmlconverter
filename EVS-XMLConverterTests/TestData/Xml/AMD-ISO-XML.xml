<?xml version="1.0" encoding="UTF-8"?><standard xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:tbx="urn:iso:std:iso:30042:ed-1" xmlns:xlink="http://www.w3.org/1999/xlink">
<?foreward metadata-error?>
<front>
<iso-meta>
<title-wrap xml:lang="en">
<intro>Microbiology of food, animal feed and water</intro>
<main>Preparation, production, storage and performance testing of culture media</main>
<compl/>
<full>Microbiology of food, animal feed and water — Preparation, production, storage and performance testing of culture media AMENDMENT 1</full>
</title-wrap>
<title-wrap xml:lang="fr">
<intro>Microbiologie des aliments, des aliments pour animaux et de l'eau</intro>
<main>Préparation, production, stockage et essais de performance des milieux de culture</main>
<compl/>
<full>Microbiologie des aliments, des aliments pour animaux et de l'eau — Préparation, production, stockage et essais de performance des milieux de culture AMENDEMENT 1</full>
</title-wrap>
<doc-ident>
<sdo>ISO</sdo>
<proj-id>72068</proj-id>
<language>en</language>
<release-version>IS</release-version>
<urn>iso:std:iso:11133:ed-1:v2:amd:1:v1:en</urn>
</doc-ident>
<std-ident>
<originator>ISO</originator>
<doc-type>IS</doc-type>
<doc-number>11133</doc-number>
<part-number/>
<edition>1</edition>
<version>2</version>
<suppl-type>amd</suppl-type>
<suppl-number>1</suppl-number>
<suppl-version>1</suppl-version>
</std-ident>
<content-language>en</content-language>
<std-ref type="dated">ISO 11133:2014/Amd.1:2018</std-ref>
<std-ref type="undated">ISO 11133:2014/Amd.1</std-ref>
<doc-ref>ISO 11133:2014/Amd.1:2018(en)</doc-ref>
<pub-date>2018-02-28</pub-date>
<release-date>2018-01-10</release-date>
<comm-ref>ISO/TC 34/SC 9</comm-ref>
<secretariat>AFNOR</secretariat>
<ics>07.100.20</ics>
<ics>07.100.30</ics>
<page-count count="7"/>

<permissions>
<copyright-statement>All rights reserved</copyright-statement>
<copyright-year>2018</copyright-year>
<copyright-holder>ISO</copyright-holder>
</permissions>
<custom-meta-group>

<custom-meta>
<meta-name>price-ref-pages</meta-name>
<meta-value>7</meta-value>
</custom-meta>
<custom-meta>
<meta-name>edition-pub-date</meta-name>
<meta-value>2014-05-15</meta-value>
</custom-meta>

<custom-meta>
<meta-name>generation-date</meta-name>
<meta-value>2018-01-23 14:47:26</meta-value>
</custom-meta>
</custom-meta-group>
</iso-meta>
<sec id="sec_foreword" sec-type="foreword">
<title>Foreword</title>
<p>ISO (the International Organization for Standardization) is a worldwide federation of national standards bodies (ISO member bodies). The work of preparing International Standards is normally carried out through ISO technical committees. Each member body interested in a subject for which a technical committee has been established has the right to be represented on that committee. International organizations, governmental and non-governmental, in liaison with ISO, also take part in the work. ISO collaborates closely with the International Electrotechnical Commission (IEC) on all matters of electrotechnical standardization. </p>
<p>The procedures used to develop this document and those intended for its further maintenance are described in the ISO/IEC Directives, Part 1. In particular the different approval criteria needed for the different types of ISO documents should be noted. This document was drafted in accordance with the editorial rules of the ISO/IEC Directives, Part 2 (see <ext-link ext-link-type="uri" xlink:href="http://www.iso.org/directives">www.iso.org/directives</ext-link>).</p>
<p>Attention is drawn to the possibility that some of the elements of this document may be the subject of patent rights. ISO shall not be held responsible for identifying any or all such patent rights. Details of any patent rights identified during the development of the document will be in the Introduction and/or on the ISO list of patent declarations received (see <ext-link ext-link-type="uri" xlink:href="http://www.iso.org/patents">www.iso.org/patents</ext-link>).</p>
<p>Any trade name used in this document is information given for the convenience of users and does not constitute an endorsement. </p>
<p>For an explanation on the voluntary nature of standards, the meaning of ISO specific terms and expressions related to conformity assessment, as well as information about ISO’s adherence to the World Trade Organization (WTO) principles in the Technical Barriers to Trade (TBT) see the following URL: <ext-link ext-link-type="uri" xlink:href="http://www.iso.org/iso/foreword.html">www.iso.org/iso/foreword.html</ext-link>.</p>
<p>This document was prepared by Technical Committee ISO/TC 34, <italic>Food products</italic>, Subcommittee SC 9, <italic>Microbiology</italic>, in collaboration with Technical Committee ISO/TC 147 <italic>Water quality</italic>, Subcommittee SC 4, <italic>Microbiological methods</italic>.</p>
</sec>
</front>
<body>
<sec id="section_1"><label/>
<p> </p>
<p><italic>Introduction </italic></p>
<p>Add the following text as the last paragraph:</p>
<p>When specific standards are revised and new standards developed, they will include a paragraph for performance testing of the culture media used in the standard.</p>
<p> </p>
<p><italic>Scope </italic></p>
<p>Replace the<italic> </italic>last paragraph with the following:</p>
<p>This document also sets criteria and describes methods for the performance testing of culture media. This document is applicable to end-users of ready-to-use media and to producers such as</p>
<list list-type="bullet">
<list-item><label>—</label><p>commercial bodies producing and/or distributing ready-to-use or semi-finished reconstituted or dehydrated media,</p></list-item>
<list-item><label>—</label><p>non-commercial bodies supplying media to third parties, and</p></list-item>
<list-item><label>—</label><p>microbiological laboratories preparing culture media for their own use.</p></list-item></list>
<p> </p>
<p><italic>3.2.6, electivity of culture medium</italic></p>
<p>Replace the definition with the following:</p>
<p>demonstration, under defined conditions, that non-target organisms, if able to grow on the medium, do not show the same visual characteristics as target microorganisms</p>
<p> </p>
<p><italic>4.3.1 General</italic></p>
<p>Add the following text as the third paragraph:</p>
<p>When a formula indicates an ingredient in hydrated form (e.g. Na<sub>2</sub>HPO<sub>4</sub>·12H<sub>2</sub>O for BPW in <std std-id="iso:std:iso:6887:-1:en" type="undated"><std-ref>ISO 6887-1</std-ref></std>) it can be replaced by an anhydrous or hydrated ingredient with a different number of water molecules, as long as the final quantity of the ingredient takes account of this difference by calculation of the molar mass.</p><?Para Page_Break?>
<p><italic>4.3.8.1 General</italic></p>
<p>Replace the last sentence with the following:</p>
<p>In all cases, make reference to the appropriate International Standard or the manufacturer’s<break/>instructions.</p>
<p> </p>
<sec><italic>5.4.2.5.1.1 Quantitative testing</italic></sec>
<p>Replace the first two paragraphs with the following:</p>
<p>For the quantitative enumeration test, a level of approximately 100 cfu is necessary to achieve sufficient precision (see Table 1). This can necessitate the use of more than one plate.</p>
<p>A practicable range of 80 cfu to 120 cfu per plate with a minimum number of 50 cfu per plate should be used. The use of more than one plate will increase the precision. For filters, the same number of cfu is needed using one or more filters. Table 1 shows the 95 % confidence intervals associated with colony counts.</p>
<p> </p>
<p><italic>5.4.2.5.1.2 Qualitative testing</italic></p>
<p>Replace the part of the sentence introducing the list with the following:</p>
<p>The volume of suspension used for testing should contain</p>
<p> </p>
<p><italic>5.4.2.5.2 Inoculum level for selectivity testing</italic></p>
<p>Replace the sentence with the following:</p>
<p>For selectivity testing of culture media, a suspension of the non-target microorganism containing at least 10<sup>4</sup> cfu is inoculated on to the plate or into the tube of medium.</p>
<p> </p>
<p><italic>5.4.2.5.3 Inoculum level for specificity testing</italic></p>
<p>Replace the sentence with the following:</p>
<p>For qualitative tests of plate media, for specificity an inoculum level of at least 10<sup>3</sup> cfu is needed.</p>
<p> </p>
<p><italic>7.3 Testing of culture media used for membrane filtration</italic></p>
<p>Add the following text as the last paragraph:</p>
<p>When testing with membrane filters, if the criteria in Table F1 are not achieved, the laboratory should assess the discrepancies between the results.</p>
<p> </p>
<p><italic>8.3.2 Procedure</italic></p>
<p>Replace the fourth list item with the following:</p>
<list list-type="bullet">
<list-item><label>—</label><p><bold>Inoculation of non-target microorganisms:</bold> Inoculate one tube of test broth per microorganism with an inoculum containing a higher number (at least 10<sup>4</sup> cfu) and mix.</p></list-item></list>
<p>Replace the seventh, eighth and ninth list items with the following:</p>
<list list-type="bullet">
<list-item><label>—</label><p>Remove one loopful (10 μl) from the tube containing the target organism and streak on a plate of the relevant selective medium (e.g. XLD).</p></list-item>
<list-item><label>—</label><p>Remove one loop (10 µl) from the culture of non-target microorganism and streak on a plate of a non-selective medium (e.g. TSA).</p></list-item>
<list-item><label>—</label><p>If a mixed culture of target and non-target organisms has been used, remove one loop (10 µl) and streak on a plate of the specific medium for the target microorganism (e.g. XLD).</p></list-item></list>
<p>Replace the last paragraph with the following:</p>
<p>If a larger volume of medium is used (e.g. 225 ml), the user may choose to adjust the volume of inoculum proportionately, but without changing the total number of organisms inoculated.</p>
<p> </p>
<p><italic>8.3.3 Calculation and interpretation of results</italic></p>
<p>Replace the last paragraph with the following:</p>
<p>Selectivity of the liquid test broth is satisfactory if either no growth, or less than 10 cfu or less than 100 cfu of non-target microorganisms occurs on the non-selective agar plate, as specified in Annexes E and F or the specific International Standard.</p>
<p> </p>
<p><italic>8.4.2.2 Confirmation media</italic></p>
<p>Replace the first list item with the following:</p>
<list list-type="bullet">
<list-item><label>—</label><p>For performance testing of liquid confirmation media, inoculate the medium under test with ≥ 10<sup>4</sup> cfu of the target organism (for example, by using a working culture suspension containing more than 10<sup>6</sup> cfu/ml and a 10 μl loop).</p></list-item></list>
<p> </p>
<p><italic>After 8.4.3</italic></p>
<p>Add the following text as a new subclause:</p>
<p><bold>8.5 Multipurpose liquid media</bold></p>
<p>For multipurpose liquid media such as BPW, perform a qualitative pre-enrichment test as a minimum, using a pathogenic organism appropriate to the laboratory’s range of test methods, e.g. <italic>Salmonella.</italic></p>
<p>Commercial and non-commercial suppliers of multipurpose liquid media are also expected to test these multipurpose liquid media as diluents for enumerations of microorganisms to further ensure the quality of the culture media they supply.</p>
<p>If a new or revised standard uses a multipurpose medium as part of the isolation procedure for a target organism not currently included in this document (<std std-id="iso:std:iso:11133:ed-1:en" type="dated"><std-ref>ISO 11133:2014</std-ref></std>) or specific standards published subsequently, the performance testing described in the new or revised standard for the multipurpose medium should at least include the use of the target organism.</p>
<p/>
<p><italic>C.4 Qualitative single tube method for selective liquid enrichment media (with target, non-target, or a mixture of target and non-target microorganisms in the same tube) (see 8.3 and Figure C.3)</italic></p>
<p>In the middle and right-hand columns, replace “≥ 10<sup>3</sup>”<sup> </sup>with “≥ 10<sup>4</sup>”.</p>
<p>In the middle and right-hand columns, delete the text in brackets in the bottom boxes.</p><?Para Page_Break?>
<p><italic>Annex E (page 45) </italic></p>
<p>Replace footnote m with the following:</p>
<p>If BPW is used for more than one application, perform at least one enrichment test as a minimum using a pathogenic organism appropriate to the laboratory’s range of test methods, e.g. <italic>Salmonella.</italic> See 8.5.</p>
<p><italic> </italic></p>
<p><italic>Table E.1, Selective media for enumeration of microorganisms</italic></p>
<p>On page 47, delete the row “IS (“TS”)”.</p>
<p>Modify the row “TBX – Productivity” as follows:</p>
<table-wrap id="tab_a" position="float"><?Table Table_MMinus?>
<table width="617">
<col width="7.32%"/>
<col width="5.79%"/>
<col width="11.59%"/>
<col width="8.69%"/>
<col width="7.23%"/>
<col width="8.68%"/>
<col width="7.4%"/>
<col width="7.09%"/>
<col width="10.14%"/>
<col width="7.24%"/>
<col width="7.24%"/>
<col width="11.59%"/>
<tbody>
<tr>
<td align="left" rowspan="2" scope="row" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">TBX</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">S</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">β-D-<break/>Glucuronidase positive <break/><italic>Escherichia coli</italic></td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><std><std-ref>ISO 16649-1</std-ref></std> and <std std-id="iso:std:iso:16649:-2:en" type="undated"><std-ref>ISO 16649-2</std-ref></std></td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Productivity</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">(21 ± 3) h / (44 ± 1) °C</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>Escherichia </italic><break/><italic>coli</italic><sup>d,h</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00012<break/>00013</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">TSA</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Quantitative</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>P</italic><sub>R</sub> ≥ 0,5</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 2px" valign="middle">Blue colonies</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>Escherichia </italic><break/><italic>coli</italic><sup>h</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">00202<sup>b</sup></td>
</tr>
</tbody></table></table-wrap>
<p>For the row “TSC (SC)”, “Productivity”, column “Reference media”, replace “TSA or other nonselective medium for anaerobes” with “A suitable nonselective medium for anaerobes (<italic>P</italic><sub>R</sub> ≥ 0,5) or media batch TSC (SC) already validated (<italic>P</italic><sub>R</sub> ≥ 0,7)”.</p>
<p> </p>
<p><italic>Table E.1, Non-selective media for enumeration of microorganisms</italic></p>
<p>Add, on page 50, the row “IS (“TS”)” that was deleted from <italic>Table E.1, Selective media for enumeration of microorganisms</italic>, with modifications for columns “Reference media” and “Criteria” as follows:</p>
<table-wrap id="tab_b" position="float"><?Table Table_MMinus?>
<table width="586">
<col width="7.33%"/>
<col width="4.58%"/>
<col width="9.14%"/>
<col width="7.62%"/>
<col width="7.64%"/>
<col width="10.74%"/>
<col width="10.3%"/>
<col width="7.44%"/>
<col width="9.62%"/>
<col width="9.14%"/>
<col width="7.31%"/>
<col width="9.14%"/>
<tbody>
<tr>
<td align="left" rowspan="2" scope="row" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">IS (“TS”)</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">S</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Sulfite-<break/>reducing bacteria</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><std std-id="iso:std:iso:15213:en" type="undated"><std-ref>ISO 15213</std-ref></std></td>
<td align="left" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Productivity</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">(24 ± 3) h <break/>to (48 ± 2) h/ <break/>(37 ± 1) °C anaerobic atmosphere</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>Clostridium perfringens</italic></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00007<sup>b</sup><break/>00080</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">A suitable non-<break/>selective medium for anaerobes</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Quantitative</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>P</italic><sub>R</sub> ≥ 0,7</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 1px" valign="middle">Black <break/>colonies</td>
</tr>
<tr>
<td align="left" colspan="1" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Specificity</td>
<td align="center" colspan="1" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>Escherichia </italic><break/><italic>coli</italic><sup>d</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">00012<break/>00013</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">—</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Qualitative</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">—</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 2px; border-bottom: solid 2px" valign="middle">No <break/>blackening</td>
</tr>
</tbody></table></table-wrap>
<p> </p>
<p><italic>Table E.1, Selective enrichment media</italic></p>
<p>For the row “ITC”, “Selectivity”, column “WDCM numbers”, add a footnote to “00023<sup>b</sup>”.</p>
<p> </p>
<p><italic>Table E.1, Selective isolation media</italic></p>
<p>For the row “CIN/SSDC”, column “Incubation” replace “(21 ± 3) h/(30 ± 1) °C” with “ (24 ± 2) h/(30 ± 1) °C”.</p>
<p>Modify the row “TBXj – Productivity” as follows:</p>
<table-wrap id="tab_c" position="float"><?Table Table_MMinus?>
<table width="617">
<col width="7.33%"/>
<col width="5.79%"/>
<col width="11.59%"/>
<col width="8.69%"/>
<col width="5.78%"/>
<col width="10.14%"/>
<col width="8.69%"/>
<col width="8.68%"/>
<col width="7.24%"/>
<col width="7.24%"/>
<col width="7.24%"/>
<col width="11.59%"/>
<tbody>
<tr>
<td align="left" rowspan="2" scope="row" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">TBXj</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">S</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">β-D-<break/>Glucuronidase positive<break/><italic>Escherichia coli</italic></td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><std std-id="iso:std:iso:16649:-3:en" type="undated"><std-ref>ISO 16649-3</std-ref></std></td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Productivity</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">(21 ± 3) h/<break/>(44 ± 1) °C</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>Escherichia coli</italic><sup>d,h</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00012<break/>00013</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">—</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Qualitative</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Good growth (2)</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 2px" valign="middle">Blue colonies</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>Escherichia coli</italic><sup>h</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">00202<sup>b</sup></td>
</tr>
</tbody></table></table-wrap>
<p> </p><?Para Page_Break?>
<p><italic>Table E.1, footnotes</italic> </p>
<p>Replace footnote m with the following:</p>
<p>If BPW is used for more than one application, perform at least one enrichment test as a minimum using a pathogenic organism appropriate to the laboratory’s range of test methods, e.g. <italic>Salmonella.</italic> See 8.5.</p>
<p/>
<p><italic>Annex F (page 67)</italic></p>
<p>Replace footnote k with the following:</p>
<p>If BPW is used for more than one application, perform at least one enrichment test as a minimum using a pathogenic organism appropriate to the laboratory’s range of test methods, e.g. <italic>Salmonella.</italic> See 8.5.</p>
<p> </p>
<p><italic>Table F.1, Selective media for enumeration of microorganisms by comparing with a non-selective reference medium</italic></p>
<p>For the row “Colilert-18”, column “Control strains”, replace “<italic>Klebsiella pneumoniae</italic>” with “<italic>Klebsiella variicola” </italic>[WDCM 00206].</p>
<p>For the row “GVPC”, “Selectivity”, column “Control strains”, delete “<italic>Pseudomonas aeruginosa</italic><sup>d</sup>”, and column “WDCM numbers”, delete “00026 or 00025”.</p>
<p>Delete the row for “Sulfite Iron/Tryptose Sulfite (TS)”.</p>
<p>For the row “TSC”, column “Reference medium”, replace “TSA or Blood agar or other nonselective medium for anaerobes” with “A suitable non-selective medium for anaerobes”.</p>
<p> </p>
<p><italic>Table F.1, Selective media for enumeration of microorganisms by comparing with a previously accepted batch (for use in special cases)</italic></p>
<p>For the row “Colilert-18”, column “Control strains”, replace “<italic>Klebsiella pneumoniae</italic>” with “<italic>Klebsiella variicola</italic>” [WDCM 00206].</p>
<p>Delete the row for “Sulfite Iron/Tryptose Sulfite (TS)”.</p>
<p/>
<p><italic>Table F.1, Non-selective media for enumeration of microorganisms</italic></p>
<p>Add, on page 72, a row for “Sulfite Iron/Tryptose Sulfite (TS)” corresponding to the two rows that were deleted from Table F.1, <italic>Selective media for enumeration of microorganisms by comparing with a non-selective reference medium</italic> and from Table F.1, <italic>Selective media for enumeration of microorganisms with a previously accepted batch</italic> with modifications for columns “Incubation”, “Reference media” and “Criteria” as follows:</p>
<table-wrap id="tab_d" position="float"><?Table Table_MMinus?>
<table width="586">
<col width="7.33%"/>
<col width="4.58%"/>
<col width="9.14%"/>
<col width="7.62%"/>
<col width="7.64%"/>
<col width="10.74%"/>
<col width="10.3%"/>
<col width="7.44%"/>
<col width="9.62%"/>
<col width="9.14%"/>
<col width="7.31%"/>
<col width="9.14%"/>
<tbody>
<tr>
<td align="left" rowspan="2" scope="row" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"> Sulfite Iron<p>Tryptose Sulfite (TS)</p></td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">S</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Sulfite-<break/>reducing anaerobes (clostridia)</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><std std-id="iso:std:iso:6461:-2:en" type="undated"><std-ref>ISO 6461-2</std-ref></std></td>
<td align="left" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Productivity</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">(20 ± 4) h to (44 ± 4) h / (37 ± 1) °C anaerobic atmosphere</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>Clostridium perfringens</italic></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00007<sup>b</sup><break/>00080</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">A suitable non-<break/>selective medium for anaerobes or Media batch Sulfite iron already accepted</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Quantitative</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>P</italic><sub>R</sub> ≥ 0,7</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 1px" valign="middle">Black <break/>colonies</td>
</tr>
<tr>
<td align="left" colspan="1" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Specificity</td>
<td align="center" colspan="1" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>Escherichia coli</italic><sup>d</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">00012<break/>00013</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">—</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Qualitative</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">—</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 2px; border-bottom: solid 2px" valign="middle">No <break/>blackening</td>
</tr>
</tbody></table></table-wrap>
<?Para Page_Break?>
<p><italic>Table F.1, Selective enrichment media</italic></p>
<p>Add the row “MKTTn” as follows:</p>
<table-wrap id="tab_e" position="float"><?Table Table_MMinus?>
<table width="610">
<col width="6.6%"/>
<col width="5.64%"/>
<col width="7.97%"/>
<col width="8.79%"/>
<col width="5.86%"/>
<col width="12.4%"/>
<col width="11.04%"/>
<col width="8.78%"/>
<col width="4.39%"/>
<col width="7.33%"/>
<col width="9.48%"/>
<col width="11.72%"/>
<tbody>
<tr>
<td align="left" rowspan="6" scope="row" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">MKTTn</td>
<td align="left" rowspan="6" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">L</td>
<td align="left" rowspan="6" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>Salmonella</italic></td>
<td align="left" rowspan="6" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><std std-id="iso:std:iso:19250:en" type="undated"><std-ref>ISO 19250</std-ref></std></td>
<td align="left" rowspan="4" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Productivity</td>
<td align="center" rowspan="6" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">(24 ± 3) h/<break/>(36 ± 2) °C</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px" valign="middle"><italic>Salmonella</italic> Typhimurium<sup>d,i</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px" valign="middle">00031</td>
<td align="center" rowspan="4" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">—</td>
<td align="center" rowspan="4" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Qualitative</td>
<td align="center" rowspan="4" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">&gt; 10 characteristic colonies on XLD or other medium of choice</td>
<td align="center" rowspan="4" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 1px" valign="middle">Characteristic colonies according to each medium (see <std std-id="iso:std:iso:19250:en" type="undated"><std-ref>ISO 19250</std-ref></std>)</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-right: solid 1px" valign="middle"><italic>Salmonella</italic> Enteritidis<sup>d,i</sup></td>
<td align="center" style="border-left: solid 1px; border-right: solid 1px" valign="middle">00030</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-right: solid 1px" valign="middle">+ <italic>Escherichia coli</italic><sup>d</sup></td>
<td align="center" style="border-left: solid 1px; border-right: solid 1px" valign="middle">00012 or 00013</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">+ <italic>Pseudomonas aeruginosa</italic></td>
<td align="center" style="border-left: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00025</td>
</tr>
<tr>
<td align="left" colspan="1" rowspan="2" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Selectivity</td>
<td align="center" colspan="1" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>Escherichia coli</italic><sup>d</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00012 or 00013</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">—</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Qualitative</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Partial inhibition ≤ 100 colonies on TSA</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 2px; border-bottom: solid 1px" valign="middle">—</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>Enterococcus faecalis</italic><sup>d</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">00009 or 00087</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">—</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Qualitative</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">&lt; 10 <break/>colonies on TSA</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 2px; border-bottom: solid 2px" valign="middle">—</td>
</tr>
</tbody></table></table-wrap>
<p> </p>
<p><italic>Table F.1, footnotes</italic> </p>
<p>Replace footnote k with the following:</p>
<p>If BPW is used for more than one application, perform at least one enrichment test as a minimum using a pathogenic organism appropriate to the laboratory’s range of test methods, e.g. <italic>Salmonella.</italic> See 8.5.</p>
<p/>
<p><italic>Table F.2, Abbreviated terms for media used in Table F.1</italic></p>
<p>Add the following line in the table, in alphabetical order<bold>:</bold></p>
<array id="tab_f">
<table width="606">
<col width="27.71%"/>
<col width="39.83%"/>
<col width="32.46%"/>
<tbody>
<tr>
<td align="center" scope="row" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">MKTTn</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">Muller-Kauffmann tetrathionate-<break/>novobiocin broth</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 2px" valign="middle"><std std-id="iso:std:iso:19250:en" type="undated"><std-ref>ISO 19250</std-ref></std></td>
</tr>
</tbody></table></array>
<p> </p>
<p><italic>G.2.4 Performance evaluation and interpretation of results</italic></p>
<p>Replace the text of the subclause with the following:</p>
<p>A batch of culture medium shall be accepted if both general (see 6.2) and microbiological quality criteria are met. “Out of control” situations for quantitative tests performed as described above include the following:</p>
<list list-type="bullet">
<list-item><label>—</label><p>a single violation of the ±3s (99 %) action limit;</p></list-item>
<list-item><label>—</label><p>two out of three observations in a row exceed the ±2s (95 %) warning limit;</p></list-item>
<list-item><label>—</label><p>six observations in a row that are steadily increasing or decreasing;</p></list-item>
<list-item><label>—</label><p>nine observations in a row on the same side of the mean.</p></list-item></list>
<p>If a single batch of culture medium violates the 99 % action limit, it shall be rejected and a process investigation shall be undertaken to establish the cause.</p>
<p>In the event of other “out of control” situations from quantitative tests performed as described above, any sequence of batches of culture medium that fail the identified criteria shall be quarantined pending the outcome of a process investigation. If repeat analyses fail to demonstrate that a quarantined batch is satisfactory it shall be rejected.</p>
<non-normative-note><label>NOTE</label><p>A criterion of four observations in a row exceeding a ±1s level can also be useful as an indication of a developing problem.</p></non-normative-note>
<p> </p>
<p><italic>J.4.4 Number of strains per criterion</italic></p>
<p>Replace the last two lines of the subclause, for footnote i, with the following:</p>
<p>Table footnote i  Qualitative specificity (agar medium): 7.4 </p>
<p>Two non-target strains: strain A and strain B</p>
</sec>
</body>
</standard>