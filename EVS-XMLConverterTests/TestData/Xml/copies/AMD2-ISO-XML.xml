<?xml version="1.0" encoding="UTF-8"?><standard xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:tbx="urn:iso:std:iso:30042:ed-1" xmlns:xlink="http://www.w3.org/1999/xlink">
<?foreward metadata-error?>
<front>
<iso-meta>
<title-wrap xml:lang="en">
<intro>Microbiology of food and animal feeding stuffs</intro>
<main>Horizontal method for the enumeration of coagulase-positive staphylococci (Staphylococcus aureus and other species)</main>
<compl>Technique using Baird-Parker agar medium</compl>
<full>Microbiology of food and animal feeding stuffs — Horizontal method for the enumeration of coagulase-positive staphylococci (Staphylococcus aureus and other species) — Part 1: Technique using Baird-Parker agar medium AMENDMENT 2: Inclusion of an alternative confirmation test using RPFA stab method</full>
</title-wrap>
<title-wrap xml:lang="fr">
<intro>Microbiologie des aliments</intro>
<main>Méthode horizontale pour le dénombrement des staphylocoques à coagulase positive (Staphylococcus aureus et autres espèces)</main>
<compl>Technique utilisant le milieu gélosé de Baird-Parker</compl>
<full>Microbiologie des aliments — Méthode horizontale pour le dénombrement des staphylocoques à coagulase positive (Staphylococcus aureus et autres espèces) — Partie 1: Technique utilisant le milieu gélosé de Baird-Parker AMENDEMENT 2: Ajout d'un essai alternatif de confirmation utilisant la méthode de piqûre sur RPFA</full>
</title-wrap>
<doc-ident>
<sdo>ISO</sdo>
<proj-id>64947</proj-id>
<language>en</language>
<release-version>IS</release-version>
<urn>iso:std:iso:6888:-1:ed-1:v1:amd:2:v1:en</urn>
</doc-ident>
<std-ident>
<originator>ISO</originator>
<doc-type>IS</doc-type>
<doc-number>6888</doc-number>
<part-number>1</part-number>
<edition>1</edition>
<version>1</version>
<suppl-type>amd</suppl-type>
<suppl-number>2</suppl-number>
<suppl-version>1</suppl-version>
</std-ident>
<content-language>en</content-language>
<std-ref type="dated">ISO 6888-1:1999/Amd.2:2018</std-ref>
<std-ref type="undated">ISO 6888-1:1999/Amd.2</std-ref>
<doc-ref>ISO 6888-1:1999/Amd.2:2018(en)</doc-ref>
<pub-date>2018-07-15</pub-date>
<release-date>2018-06-11</release-date>
<comm-ref>ISO/TC 34/SC 9</comm-ref>
<secretariat>AFNOR</secretariat>
<ics>07.100.30</ics>
<page-count count="5"/>
<std-xref type="supplements">
<std-ref>ISO 6888-1:1999</std-ref>
</std-xref>
<permissions>
<copyright-statement>All rights reserved</copyright-statement>
<copyright-year>2018</copyright-year>
<copyright-holder>ISO</copyright-holder>
</permissions>
<custom-meta-group>

<custom-meta>
<meta-name>price-ref-pages</meta-name>
<meta-value>5</meta-value>
</custom-meta>
<custom-meta>
<meta-name>edition-pub-date</meta-name>
<meta-value>1999-02-18</meta-value>
</custom-meta>

<custom-meta>
<meta-name>generation-date</meta-name>
<meta-value>2018-06-13 17:11:12</meta-value>
</custom-meta>
</custom-meta-group>
</iso-meta>
<sec id="sec_foreword" sec-type="foreword">
<title>Foreword</title>
<p>ISO (the International Organization for Standardization) is a worldwide federation of national standards bodies (ISO member bodies). The work of preparing International Standards is normally carried out through ISO technical committees. Each member body interested in a subject for which a technical committee has been established has the right to be represented on that committee. International organizations, governmental and non-governmental, in liaison with ISO, also take part in the work. ISO collaborates closely with the International Electrotechnical Commission (IEC) on all matters of electrotechnical standardization. </p>
<p>The procedures used to develop this document and those intended for its further maintenance are described in the ISO/IEC Directives, Part 1. In particular, the different approval criteria needed for the different types of ISO documents should be noted. This document was drafted in accordance with the editorial rules of the ISO/IEC Directives, Part 2 (see <ext-link ext-link-type="uri" xlink:href="https://www.iso.org/directives-and-policies.html">www.iso.org/directives</ext-link>).</p>
<p>Attention is drawn to the possibility that some of the elements of this document may be the subject of patent rights. ISO shall not be held responsible for identifying any or all such patent rights. Details of any patent rights identified during the development of the document will be in the Introduction and/or on the ISO list of patent declarations received (see <ext-link ext-link-type="uri" xlink:href="https://www.iso.org/iso-standards-and-patents.html">www.iso.org/patents</ext-link>).</p>
<p>Any trade name used in this document is information given for the convenience of users and does not constitute an endorsement. </p>
<p>For an explanation of the voluntary nature of standards, the meaning of ISO specific terms and expressions related to conformity assessment, as well as information about ISO's adherence to the World Trade Organization (WTO) principles in the Technical Barriers to Trade (TBT) see <ext-link ext-link-type="uri" xlink:href="https://www.iso.org/foreword-supplementary-information.html">www.iso.org/iso/foreword.html</ext-link>.</p>
<p>This document was prepared by the European Committee for Standardization (CEN) Technical Committee CEN/TC 275, <italic>Food analysis — Horizontal methods</italic>, in collaboration with ISO Technical Committee TC 34, <italic>Food products</italic>, Subcommittee SC 9, <italic>Microbiology</italic>, in accordance with the agreement on technical cooperation between ISO and CEN (Vienna Agreement).</p>
<p>Any feedback or questions on this document should be directed to the user’s national standards body. A complete listing of these bodies can be found at <ext-link ext-link-type="uri" xlink:href="https://www.iso.org/members.html">www.iso.org/members.html</ext-link>.</p>
<p>A list of all the parts in the ISO 6888 series can be found on the ISO website.</p>
</sec>
</front>
<body>
<sec id="section_1"><label/>
</sec>
<sec id="section_2"><label/>
<p><italic> </italic></p>
<p><italic>Clause 2</italic></p>
<p>Add the following normative reference.</p>
<p>ISO 11133, <italic>Microbiology of food, animal feed and water — Preparation, production, storage and performance testing of culture media</italic></p>
<p><italic> </italic></p>
<p><italic>Clause 5</italic></p>
<p>Add the following text.</p>
<p><bold>5.6	Rabbit plasma fibrinogen agar (RPFA) medium</bold></p>
<p><bold>5.6.1	General</bold></p>
<p>Commercially available media, in accordance with this document, can be used. Nevertheless, considering the established variability of manufactured lots of the supplement, it is recommended that each batch of bovine fibrinogen/rabbit plasma solution be tested before use, by running positive and negative controls.</p>
<p><bold>5.6.2	Base medium</bold></p>
<p>Prepare the base medium as stated in 5.3.1, with the exception of the distribution of the base medium, in quantities of 90 ml per flask or bottle.</p>
<p><bold>5.6.3	Solutions</bold></p>
<p><bold>5.6.3.1	Potassium tellurite solution</bold></p>
<p>Prepare the potassium tellurite solution as indicated in 5.3.2.1.</p><?Para Page_Break?>
<p><bold>5.6.3.2	Bovine fibrinogen solution</bold></p>
<p><bold>5.6.3.2.1	Composition</bold></p>
<array id="tab_a">
<table width="623">
<col width="32.94%"/>
<col width="67.06%"/>
<tbody>
<tr>
<td align="left" scope="row" valign="top">Bovine fibrinogen</td>
<td align="left" valign="top">5 g to 7 g<sup>a</sup></td>
</tr>
<tr>
<td align="left" scope="row" valign="top">Sterile water</td>
<td align="left" valign="top">100 ml</td>
</tr>
<tr>
<td align="left" colspan="2" scope="col" valign="top"><sup>a</sup>   Depending on the purity of the bovine fibrinogen.</td>
</tr>
</tbody></table></array>
<p><bold>5.6.3.2.2	Preparation</bold></p>
<p>Under aseptic conditions, dissolve the bovine fibrinogen in the water just prior to use.</p>
<p><bold>5.6.3.3	Rabbit plasma and trypsin inhibitor solution</bold></p>
<sec id="sec_5.6.3.3.1">
<label>5.6.3.3.1</label><title>Composition</title>
<array id="tab_b">
<table width="409">
<col width="73.78%"/>
<col width="26.22%"/>
<tbody>
<tr>
<td align="left" scope="row" valign="top">Rabbit plasma with EDTA for coagulase <break/>(EDTA coagulase plasma)</td>
<td align="left" valign="top">30 ml</td>
</tr>
<tr>
<td align="left" scope="row" valign="top">Trypsin inhibitor</td>
<td align="left" valign="top">30 mg</td>
</tr>
</tbody></table></array>
</sec>
<sec id="sec_5.6.3.3.2">
<label>5.6.3.3.2</label><title>Preparation</title>
<p>Operating under aseptic conditions, dissolve the components in the water, just prior to use.</p>
<p><bold>5.6.4	Complete medium</bold></p>
<p><bold>5.6.4.1	Composition</bold></p>
<array id="tab_c">
<table width="616">
<col width="50%"/>
<col width="50%"/>
<tbody>
<tr>
<td align="left" scope="row" valign="top">Base medium (5.6.2)</td>
<td align="left" valign="top">90 ml</td>
</tr>
<tr>
<td align="left" scope="row" valign="top">Potassium tellurite solution (5.6.3.1)</td>
<td align="left" valign="top">0,25 ml</td>
</tr>
<tr>
<td align="left" scope="row" valign="top">Bovine fibrinogen solution (5.6.3.2)</td>
<td align="left" valign="top">7,5 ml</td>
</tr>
<tr>
<td align="left" scope="row" valign="top">Rabbit plasma and trypsin inhibitor solution (5.6.3.3)</td>
<td align="left" valign="top">2,5 ml</td>
</tr>
</tbody></table></array>
</sec>
<sec id="sec_5.6.4.2"><label>5.6.4.2</label>
<p><bold>Preparation</bold></p>
<p>Melt the base medium, then let it cool down to 47 °C to 50 °C in a water bath (6.4) because at a lower temperature, this medium solidifies.</p>
<p>Under aseptic conditions, add the three solutions previously warmed to 48 °C ± 1 °C in a water bath. Mix thoroughly after each addition by rotation to minimize foaming.</p>
<p>Use the complete medium <bold>immediately after its preparation</bold>, in order to avoid any precipitation of the plasma.</p>
<non-normative-note content-type="warning"><label>WARNING</label><p>If a commercially available solution of bovine fibrinogen/rabbit plasma is used, follow with great care the manufacturer's instructions for the preparation of this solution and of the complete medium (in particular the temperature of the base medium). Otherwise, the medium can completely lose its activity.</p></non-normative-note>
</sec>
<sec id="sec_5.6.5"><label>5.6.5</label>
<p><bold>Preparation of RPFA plates</bold></p>
<p>Place the appropriate quantity of the complete medium into sterile Petri dishes in order to obtain an agar thickness of about 4 mm, and allow to solidify.</p>
<p>The prepared RPFA plates may be stored, prior to drying, at 5 °C ± 3 °C for a given period of time before use if the stability of the medium is proven for this period.</p>
<p>For plates prepared commercially, the instructions of the manufacturers should be followed.</p>
<p>Before use, dry the plates, according to the conditions of <std std-id="iso:std:iso:7218:en" type="undated"><std-ref>ISO 7218</std-ref></std>.</p>
<p><bold>5.6.6 	Performance testing for the quality assurance of the culture medium</bold></p>
<p>Table 1 shows the performance testing for the quality assurance of RPFA medium used as confirmation medium.</p>
<p>For the definition of productivity, selectivity and specificity, refer to <std std-id="iso:std:iso:11133:en" type="undated"><std-ref>ISO 11133</std-ref></std>. In general, follow the procedures for performance testing described in <std std-id="iso:std:iso:11133:en" type="undated"><std-ref>ISO 11133</std-ref></std>.</p>
<table-wrap id="tab_1" position="float">
<label>Table 1</label><caption><title>Performance testing for the quality assurance of the RPFA medium</title>
</caption>
<table width="686">
<col width="10.16%"/>
<col width="6.5%"/>
<col width="13.03%"/>
<col width="11.72%"/>
<col width="14.32%"/>
<col width="10.42%"/>
<col width="11.72%"/>
<col width="10.42%"/>
<col width="11.71%"/>
<thead>
<tr>
<th align="center" scope="col" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>Medium</bold></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>Type</bold></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>Function</bold></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>Incubation</bold></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>Control strain</bold></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>WDCM number</bold><sup>a</sup></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>Method of control</bold></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 2px" valign="top"><bold>Criteria</bold><sup>c</sup></th>
<th align="center" scope="col" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 2px" valign="top"><bold>Characteristic reaction</bold></th>
</tr>
</thead>
<tbody>
<tr>
<td align="center" rowspan="4" scope="row" style="border-left: solid 2px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">RPFA</td>
<td align="center" rowspan="4" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">S<sup>e</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Productivity</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">(24 ± 2) h to (48 ± 2) h / (37 ± 1) °C</td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>Staphylococcus aureus</italic></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00034<sup>b</sup><break/><sup/>00032</td>
<td align="left" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Qualitative<sup>f</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Good growth (2)</td>
<td align="left" style="border-left: solid 1px; border-top: solid 2px; border-right: solid 2px; border-bottom: solid 1px" valign="middle">Black or grey <break/>colonies with <break/>opacity halo</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Selectivity</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">(48 ± 2) h / (37 ± 1) °C</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle"><italic>Escherichia coli</italic><sup>d</sup></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">00012 or 00013</td>
<td align="left" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Qualitative</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Total inhibition</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 2px; border-bottom: solid 1px" valign="middle">—</td>
</tr>
<tr>
<td align="center" colspan="1" rowspan="2" scope="row" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Specificity</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">(24 ± 2) h to (48 ± 2) h / (37 ± 1) °C</td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px" valign="middle"><italic>Staphylococcus saprophyticus</italic></td>
<td align="center" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px" valign="middle">00159<sup>b</sup></td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">Qualitative</td>
<td align="center" rowspan="2" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 1px; border-bottom: solid 1px" valign="middle">—</td>
<td align="left" rowspan="2" style="border-left: solid 1px; border-top: solid 1px; border-right: solid 2px; border-bottom: solid 1px" valign="middle">Black or grey <break/>colonies without opacity halo</td>
</tr>
<tr>
<td align="center" colspan="1" scope="row" style="border-left: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle"><italic>Staphylococcus epidermidis</italic></td>
<td align="center" style="border-left: solid 1px; border-right: solid 1px; border-bottom: solid 2px" valign="middle">00036</td>
</tr>
</tbody>
</table><table-wrap-foot>
<fn id="table-fn_1.1"><label><sup>a</sup></label><p>Refer to the reference strain catalogue on at <uri>http://www.wfcc.info</uri> for information on culture collection strain numbers and contact details; WDCM: World Data Centre for Microorganisms.</p></fn>
<fn id="table-fn_1.2"><label><sup>b</sup></label><p>Strain to be used by the user laboratory (minimum).</p></fn>
<fn id="table-fn_1.3"><label><sup>c</sup></label><p>Growth is categorized as: 0: — no growth; 1: — weak growth; 2: — good growth (see <std std-id="iso:std:iso:11133:en" type="undated"><std-ref>ISO 11133</std-ref></std>).</p></fn>
<fn id="table-fn_1.4"><label><sup>d</sup></label><p>Strain free of choice, one of the strains has to be used as a minimum.</p></fn>
<fn id="table-fn_1.5"><label><sup>e</sup></label><p>S: solid medium.</p></fn>
<fn id="table-fn_1.6"><label><sup>f</sup></label><p>In case of both quantitative and qualitative use for the RPFA medium, only results of the quantitative tests are required.</p></fn>
</table-wrap-foot></table-wrap>
<p>NOTE	<italic>Proteus</italic> or <italic>Bacillus</italic> may also grow but appear as brown colonies on Baird-Parker Agar.</p>
<p> </p>
<p><italic>6.4</italic></p>
<p>Replace paragraph 6.4 with the following text.</p>
<p><bold>6.4	Water baths,</bold> or similar apparatus, capable of being maintained between 47 °C to 50 °C.</p>
<p> </p>
<p><italic>6.7</italic></p>
<p>Replace paragraph 6.7 with the following text.</p>
<p><bold>6.7	Straight wire,</bold> (see <std std-id="iso:std:iso:7218:en" type="undated"><std-ref>ISO 7218</std-ref></std>) or Pasteur pipette.</p>
<p> </p><?Para Page_Break?>
<p><italic>9.5</italic></p>
<p>Replace subclause 9.5 with the following text.</p>
<non-normative-note><label>NOTE</label><p>The text for the replacement 9.5.2 is identical to the previous 9.5 “Confirmation (coagulase test)”.</p></non-normative-note>
<non-normative-note/>
<p><bold>9.5	Confirmation</bold></p>
<p><bold>9.5.1	General</bold></p>
<p>The confirmation of coagulase positive staphylococci is undertaken by a tube test (9.5.2). Alternatively, it can be undertaken by a plate test using RPFA (9.5.3) (see References [9], [10] and [11]).</p>
<p><bold>9.5.2	Coagulase tube test</bold></p>
<p>From the surface of each selected colony (9.4), remove an inoculum with a sterile wire (6.7) and transfer it to a tube or bottle of brain-heart infusion broth (5.4).</p>
<p>Incubate at 35 °C or 37 °C<sup>[5]</sup> for 24 h ± 2 h.</p>
<p>Aseptically add 0,1 ml of each culture to 0,3 ml of the rabbit plasma (5.5) (unless other amounts are specified by the manufacturer) in sterile haemolysis tubes or bottles (specified in 6.5), and incubate at 35 °C or 37 °C<sup>[5]</sup>.</p>
<p>By tilting the tube, examine for clotting of the plasma after 4 h to 6 h of incubation and, if the test is negative, re-examine at 24 h of incubation, or examine at the incubation times specified by the manufacturer.</p>
<p>Consider the coagulase test to be positive if the volume of clot occupies more than half of the original volume of the liquid.</p>
<p>As an uninoculated control, for each batch of plasma, add 0,1 ml of sterile brain-heart infusion broth (5.4) to the recommended quantity of rabbit plasma (5.5) and incubate without inoculation. For the test to be valid, the control plasma shall show no signs of clotting.</p>
<p><bold>9.5.3	Plate test using RPFA</bold></p>
<p>From the surface of each selected colony (9.4), take an inoculum with a sterile wire (6.7) and stab inoculate on RPFA plates (5.6.4).</p>
<p>Several colonies in addition to one positive control strain and one negative control strain, can be inoculated onto one plate such that the individual colonies have well separated precipitation zones.</p>
<non-normative-note><label>NOTE</label><p> As a guidance, up to ten colonies can be inoculated onto one plate.</p></non-normative-note>
<p>Incubate the plates at 35 °C or 37 °C for 24 h to 48 h.</p>
<p>After incubation, the staphylococci form black or grey or even white, small colonies surrounded by a halo of precipitation, indicating coagulase activity.</p>
<p>Before reading, the plates can be stored in the refrigerator at 5 °C ± 3 °C for 24 h to 48 h.</p>
<p><italic> </italic></p><?Para Page_Break?>
<p><italic>Bibliography</italic></p>
<p>Add the following references.</p>
<p>[9]	<sc>Baudouin</sc> N., <sc>Cauquil</sc> A., <sc>Soudrie</sc> N., <sc>Bouchez</sc> P., <sc>Deperrois</sc> V., <sc>Asséré</sc> A. and <sc>Lombard</sc> B. Report of EURL study of an alternative to the confirmation step of the Standard <std><std-ref>EN ISO 6888-1</std-ref></std> for enumeration of coagulase-positive staphylococci (BP agar). April 2009. Available at: <break/><uri>https://sites.anses.fr/en/minisite/staphylococci/study-alternative-confirmation-step-standard-en-iso-6888-1-enumeration-1</uri></p>
<p>[10]	Food and Consumer Product Safety Authority. Data of the study of the shelf-life of RPFA. Available at: <uri>http://standards.iso.org/iso/6888/-1/ed-1/en/amd/2/</uri> </p>
<p>[11]	French study to compare confirmation of coagulase positive staphylococci by coagulase test tube and RPF agar plate. Available at: <uri>http://standards.iso.org/iso/6888/-1/ed-1/en/amd/2/</uri></p>
</sec>
</sec>
</body>
</standard>