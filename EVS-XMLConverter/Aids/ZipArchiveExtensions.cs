﻿using System;
using System.IO;
using System.IO.Compression;

namespace EVS_XMLConverter.Aids {
    public static class ZipArchiveExtensions {
        public static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryName, bool overwrite) {
            if (!overwrite) {
                archive.ExtractToDirectory(destinationDirectoryName);
                return;
            }
            var destinationDirectoryFullPath = Directory.CreateDirectory(destinationDirectoryName).FullName;
            foreach (var file in archive.Entries) {
                var completeFileName = Path.GetFullPath(Path.Combine(destinationDirectoryFullPath, file.FullName));
                if (!completeFileName.StartsWith(destinationDirectoryFullPath, StringComparison.OrdinalIgnoreCase)) {
                    throw new ConversionException("Trying to extract file outside of destination directory");
                }
                if (string.IsNullOrEmpty(file.Name) == true) {
                    // assuming the directory is empty
                    Directory.CreateDirectory(Path.GetDirectoryName(completeFileName) ?? throw new ConversionException("Extracting error"));
                    continue;
                }
                file.ExtractToFile(completeFileName, true);
            }
        }
    }
}