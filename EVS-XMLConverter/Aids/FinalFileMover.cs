﻿using System;
using System.IO;
using System.Linq;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using static EVS_XMLConverter.Aids.MicroConverter;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Aids {
    public class FinalFileMover {
        public static FinalFiles MoveAndStructureFilesToDestination(EvsData evsData, FinalFiles finalFiles, DirectoryInfo temporaryFolder) {
            try {
                MoveHtmlPackageToDestination(finalFiles.HtmlFull, AppSettings["HtmlLocation"], temporaryFolder, evsData);
                MoveHtmlPackageToDestination(finalFiles.HtmlPreview, AppSettings["HtmlPreviewLocation"], temporaryFolder, evsData);

                MoveAndReplaceIfExists(finalFiles.PdfFull, newFileLocation: new FileInfo(evsData.PdfDestination?.FullName));
                MoveAndReplaceIfExists(finalFiles.PdfPreview, newFileLocation: new FileInfo(evsData.PdfPreviewDestination?.FullName));
                //Conversion Log not moved. Uurida
                if (evsData.EvsIsoData.BulletinIssue == DateTime.MinValue) MoveAndReplaceIfExists(finalFiles.EvsXmlPackage, newFileLocation: new FileInfo(evsData.EvsXmlPackageDestination?.FullName));
                return finalFiles;
            }
            catch (Exception ex) {
                throw new ConversionException(string.Concat("Probleem failide salvestamisel: ", ex.Message));
            }
        }
        public static DirectoryInfo MoveHtmlPackageToDestination(FileInfo htmlFile, string saveLocation, DirectoryInfo temporaryFolder, EvsData evsData) {
            DirectoryInfo htmlDirectory = null;
            try { htmlDirectory = Directory.CreateDirectory(Path.Combine(saveLocation, evsData.FolderName, "Html")); }
            catch { htmlDirectory = new DirectoryInfo(Path.Combine(saveLocation, evsData.FolderName, "Html")); }

            ChangeImageLocations(htmlFile, evsData);
            MoveAndReplaceIfExists(htmlFile, newDirectoryLocation: htmlDirectory);
            if (htmlFile.Name.Contains(AppSettings["PdfPreview"]) == false) MoveHtmlImagesToDestination(temporaryFolder, htmlDirectory);
            return htmlDirectory;
        }
        public static FileInfo ChangeImageLocations(FileInfo htmlFileLocation, EvsData evsData) {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(htmlFileLocation.FullName);
            foreach (var img in htmlDoc.DocumentNode.Descendants("img")) {
                var src = img.Attributes["src"]?.Value;
                //miks saab vahele jätta? Uurida
                if (src == null) continue;
                var srcIndex = src.IndexOf(string.Concat(evsData.FolderName, @"\"), StringComparison.Ordinal);
                if (srcIndex > 0) img.Attributes["src"].Value = src.Substring(srcIndex + string.Concat(evsData.FolderName, @"\").Length);
            }
            htmlDoc.Save(htmlFileLocation.FullName);
            return htmlFileLocation;
        }
        public static void MoveAndReplaceIfExists(FileInfo file, DirectoryInfo newDirectoryLocation = null, FileInfo newFileLocation = null) {
            var destination = string.Empty;
            if (newDirectoryLocation != null) destination = Path.Combine(newDirectoryLocation.FullName, file.Name);
            if (newFileLocation != null) destination = newFileLocation.FullName;
            try {
                if (File.Exists(destination)) File.Delete(destination);
                file.MoveTo(destination);
            }
            catch {
                //try to move twice
                try { file.MoveTo(destination); }
                catch { throw new ConversionException(string.Concat("", destination, " is in use or missing. Conversion failed")); }
            }
        }
        public static DirectoryInfo MoveHtmlImagesToDestination(DirectoryInfo temporaryFolder, DirectoryInfo htmlDirectory) {
            foreach (var mainDirectory in temporaryFolder.GetDirectories()) {
                var filesDirectory = CreateOrReferenceDirectory(Path.Combine(htmlDirectory.FullName, mainDirectory.Name));
                var graphicsDirectory = mainDirectory.GetDirectories().FirstOrDefault();
                if (graphicsDirectory == null) {
                    Directory.Delete(filesDirectory.FullName, true);
                    continue;
                }
                var existingGraphicsFolder = Path.Combine(htmlDirectory.FullName, mainDirectory.Name, graphicsDirectory.Name);
                try {
                    if (Directory.Exists(existingGraphicsFolder)) Directory.Delete(existingGraphicsFolder, true);
                    graphicsDirectory.MoveTo(existingGraphicsFolder);
                }
                catch { /*if failed, expect that correct image already exists*/ }
            }
            return htmlDirectory;
        }
    }
}
