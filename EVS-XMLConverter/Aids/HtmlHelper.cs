﻿using EVS_XMLConverter.Logic.Html;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EVS_XMLConverter.Aids {
    public class HtmlHelper {
        public static HtmlNode AddAttributeToNode(HtmlNode node, string attributeName, string attributeValue) {
            if (node.Name == "#text") return node;
            var attribute = node.Attributes[attributeName];
            if (attribute == null) {
                node.Attributes.Add(attributeName, attributeValue);
                return node;
            }
            if (attributeName == "style" && !node.Attributes[attributeName].Value.TrimEnd().EndsWith(";"))
                node.Attributes[attributeName].Value += ";";
            attribute.Value += string.Concat(" ", attributeValue);
            return node;
        }
        public static HtmlNode RemoveClassFromNode(HtmlNode node, string className) {
            if (node.Name == "#text") return node;
            var attribute = node.Attributes["class"];
            if (attribute == null) return node;
            attribute.Value = attribute.Value.Replace(className, "");
            return node;
        }
        public static List<HtmlNode> FindNodesByClassName(HtmlNode htmlNode, string className) {
            return htmlNode.Descendants().Where(d => d.Attributes["class"]?.Value.Contains(className) == true).ToList();
        }
        public static List<HtmlNode> FindNodesById(HtmlNode htmlNode, string id) {
            return htmlNode.Descendants().Where(d => d.Id?.Contains(id) == true).ToList();
        }
        public static List<HtmlNode> GetNodesDescendants(HtmlDocument htmlDoc, List<string> nodeNames) {
            var descendantsList = new List<HtmlNode>();
            foreach (var name in nodeNames) {
                var descendants = htmlDoc.DocumentNode.SelectNodes(string.Concat("//", name))?.Descendants();
                if (descendants != null) descendantsList.AddRange(descendants);
            }
            return descendantsList;
        }
        public static bool CheckIfInTable(HtmlNode node) {
            //checks if parents' class values contain table rather than if node name is table
            //because for example 'sts-table-wrap' is the class name of a div element
            var parent = node;
            while (parent != null) {
                if (parent.Attributes["class"]?.Value.Contains("table") == true) return true;
                parent = parent.ParentNode;
            }
            return false;
        }
        public static string SaveHtml(HtmlDocument htmlDoc, string fullName) {
            using (var fileStream = new FileStream(fullName, FileMode.Create)) {
                htmlDoc.Save(fileStream);
                fileStream.Close();
            }
            return fullName;
        }
        public static string FindImage(DirectoryInfo directory, string imageName) {
            string imageFileLocation = null;
            var i = 0;
            while (i < 2) {
                imageFileLocation = Path.Combine(directory.FullName, imageName);
                if (File.Exists(imageFileLocation)) return imageFileLocation;
                else if (File.Exists(imageFileLocation.Replace(".png", ".tif"))) {
                    ImageModifier.ConvertTifImageToPng(imageFileLocation);
                    if (File.Exists(imageFileLocation)) return imageFileLocation;
                }
                else {
                    directory = directory?.GetDirectories().FirstOrDefault();
                    if (directory == null) return null;
                    i++;
                }
            }
            if (File.Exists(imageFileLocation)) return imageFileLocation;
            else return null;
        }
        public static string TrimStart(string target, string trimString) {
            if (string.IsNullOrEmpty(trimString)) return target;
            string result = target;
            while (result.StartsWith(trimString)) result = result.Substring(trimString.Length);
            return result;
        }
        public static HtmlNode FindNextSibling(HtmlNode node) {
            var nextSibling = node?.NextSibling;
            if (nextSibling?.Name == "#text") nextSibling = nextSibling?.NextSibling;
            if (nextSibling?.ParentNode == node?.ParentNode) return nextSibling;
            else return null;
        }
        public static HtmlNode FindPreviousSibling(HtmlNode node) {
            var previousSibling = node?.PreviousSibling;
            if (previousSibling?.Name == "#text") previousSibling = previousSibling?.PreviousSibling;
            if (previousSibling?.ParentNode == node?.ParentNode) return previousSibling;
            else return null;
        }
    }
}
