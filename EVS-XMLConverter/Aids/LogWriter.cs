﻿using System;
using System.IO;
using System.Linq;
using EVS_XMLConverter.Objects;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Aids {
    public class LogWriter {
        public static bool ScaledError { get; set; }
        public static DirectoryInfo Directory { get; set; }
        public static void LogWrite(string logMessage) {
            if (Directory == null) return;
            var mExePath = Directory.FullName;
            try { using (var w = File.AppendText(Path.Combine(mExePath, AppSettings["ConversionLog"]))) Log(logMessage, w); }
            catch (Exception) { /* ignored */ }
        }
        public static void Log(string logMessage, TextWriter txtWriter) {
            try {
                txtWriter.Write("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :{0}", logMessage);
            }
            catch (Exception) { /* ignored */ }
        }
        public static void DataWrite(EvsData evsData) {
            LogWrite(string.Concat("PdfDestination: ", evsData.PdfDestination.FullName));
            LogWrite(string.Concat("PdfPreviewDestination: ", evsData.PdfPreviewDestination.FullName));
            LogWrite(string.Concat("EvsXmlPackageDestionation: ", evsData.EvsXmlPackageDestination.FullName));
            LogWrite(string.Concat("EtTitle: ", evsData.EtTitle));
            LogWrite(string.Concat("EnTitle: ", evsData.EnTitle));
            LogWrite(string.Concat("EvsReference: ", evsData.EvsReference));
            LogWrite(string.Concat("Organisation: ", evsData.StaDISOrganisation));
            LogWrite(string.Concat("IcsGroups: ", string.Join(", ", evsData.IcsGroups)));
            LogWrite(string.Concat("FileName: ", evsData.FileName));
            LogWrite(string.Concat("FolderName: ", evsData.FolderName));
            LogWrite(string.Concat("Amendments: ", evsData.EvsIsoData.Amendments));
            LogWrite(string.Concat("Scope: ", evsData.EvsIsoData.Scope));
            LogWrite(string.Concat("Committee: ", evsData.EvsIsoData.Committee));
            LogWrite(string.Concat("BulletinIssue: ", evsData.EvsIsoData.BulletinIssue.ToString()));
            LogWrite(string.Concat("XmlZipFiles: ", string.Join(", ", evsData.XmlZipFiles)));
        }
    }
}
