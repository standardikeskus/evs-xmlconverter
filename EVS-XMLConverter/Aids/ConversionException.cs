﻿using System;
using System.Runtime.Serialization;

namespace EVS_XMLConverter.Aids {
    [Serializable]
    public class ConversionException : Exception, ISerializable {
        protected ConversionException(SerializationInfo info, StreamingContext ctxt) : base(info, ctxt) { }
        public ConversionException(string message) : base(message) { }
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context) {
            base.GetObjectData(info, context);
        }
    }
}
