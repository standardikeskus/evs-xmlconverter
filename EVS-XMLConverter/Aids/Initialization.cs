﻿using System.IO;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Aids {
    public class Initialization {
        public static void DeleteTempFolders() {
            foreach (var directory in new DirectoryInfo(AppSettings["Temporary"]).GetDirectories()) {
                try { Directory.Delete(directory.FullName, true); }
                catch { /* ignored */ }
            }
        }
    }
}
