﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using static EVS_XMLConverter.Aids.MicroConverter;

namespace EVS_XMLConverter.Aids {
    public class XmlHelper {
        public static Originator GetOriginator(XmlStandard xmlStandard, XmlStandard previousXmlStandard, bool specific) {
            try {
                var metaElement = xmlStandard.XDoc.Descendants(xmlStandard.MetaStructure).FirstOrDefault();
                if (metaElement == null) return null;
                XElement previousMetaElement = null;
                if (previousXmlStandard != null) {
                    previousMetaElement = previousXmlStandard.XDoc.Descendants(previousXmlStandard.MetaStructure).FirstOrDefault();
                }
                var originatorValue = GetOriginatorValue(metaElement);
                //IsEuropean might be needed as well
                if (xmlStandard.Originator != null) {
                    xmlStandard.IsEnIso = CheckIfEnIso(xmlStandard, previousXmlStandard);
                    if (xmlStandard.IsEnIso == true) previousXmlStandard.IsEnIso = true;
                }
                return DetermineOriginator(originatorValue, xmlStandard.IsEnIso, specific);
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("Probleem 'Originator' leidmisega XML failis: ", ex.Message)); }
        }
        public static bool CheckIfEuropean(Originator originator) {
            if (originator.Equals(Originator.Cen) ||
                originator.Equals(Originator.Clc) ||
                originator.Equals(Originator.CenClc)) return true;
            else return false;
        }
        public static bool CheckIfEnIso(XmlStandard xmlStandard, XmlStandard previousXmlStandard) {
            //for both EnIso and EnIec
            try {
                if (previousXmlStandard == null) return false;
                if (xmlStandard.IsEuropean == true && previousXmlStandard.IsEuropean == false) return true;
                if (xmlStandard.IsEuropean == false && previousXmlStandard.IsEuropean == true) return true;
                return false;
            }
            catch (Exception) { throw new ConversionException(string.Concat("Probleem EN ISO struktuuri tuvastamisega")); }
        }
        public static string GetOriginatorValue(XElement metaElement) {
            try {
                var originatorValue = metaElement.Attribute("originator") != null
                    ? metaElement.Attributes("originator").FirstOrDefault()?.Value
                    : metaElement.Descendants("originator").FirstOrDefault()?.Value;
                return originatorValue;
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("XML failis puudub 'Originator': ", ex.Message)); }
        }
        public static Originator DetermineOriginator(string originatorValue, bool checkEnIso, bool specific) {
            try {
                switch (specific) {
                    case true when originatorValue == ElementNames.Cen && checkEnIso:
                        return Originator.CenIso;
                    case true when originatorValue == ElementNames.Iso && checkEnIso:
                        return Originator.IsoCen;
                    case true when originatorValue == ElementNames.Cenelec && checkEnIso:
                        return Originator.ClcIec;
                    case true when originatorValue == ElementNames.Clc && checkEnIso:
                        return Originator.ClcIec;
                    case true when originatorValue == ElementNames.Iec && checkEnIso:
                        return Originator.IecClc;
                    default:
                        switch (originatorValue) {
                            case ElementNames.Cen:
                                return Originator.Cen;
                            case ElementNames.Clc:
                                return Originator.Clc;
                            case ElementNames.Cenelec:
                                return Originator.Clc;
                            case ElementNames.Iso:
                                return Originator.Iso;
                            case ElementNames.IsoIec:
                                return Originator.Iso;
                            case ElementNames.CenClc:
                                return Originator.CenClc;
                            case ElementNames.Iec:
                                return Originator.Iec;
                            default:
                                throw new ConversionException(string.Concat("Probleem standardimisorganisatsiooni tuvastamisega: ", originatorValue));
                        }
                }
            }
            catch (Exception) { throw new ConversionException(string.Concat("Probleem standardimisorganisatsiooni tuvastamisega: ", originatorValue)); }
        }
        public static string GetMetaStructure(XDocument xDoc) {
            try {
                if (xDoc.Descendants(ElementNames.RegMeta).FirstOrDefault() != null) return ElementNames.RegMeta;
                if (xDoc.Descendants(ElementNames.IsoMeta).FirstOrDefault() != null) return ElementNames.IsoMeta;
                if (xDoc.Descendants(ElementNames.StdMeta).FirstOrDefault() != null) return ElementNames.StdMeta;
                if (xDoc.Descendants(ElementNames.NatMeta).FirstOrDefault() != null) return ElementNames.NatMeta;
                throw new ConversionException("XML faili metastruktuur ei ole äratuntav");
            }
            catch (Exception) { throw new ConversionException("XML faili metastruktuur ei ole äratuntav"); }
        }
        public static bool CheckIfAmendment(XDocument xDoc) {
            try {
                var amendmentElementValue = xDoc.Descendants(GetMetaStructure(xDoc))
                    .FirstOrDefault()?.Descendants("suppl-type").FirstOrDefault()?.Value;
                return ElementNames.Amendments.Contains(amendmentElementValue);
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("Probleem muudatuste tuvastamisega XML failis: ", ex.Message)); }
        }
        public static string GetCopyrightNoticeForIso(XmlStandard xmlStandard) {
            try {
                var copyrightNotice = string.Empty;
                var holder = GetCopyrightHolder(xmlStandard);
                var statement = GetCopyrightStatement(xmlStandard);
                if (xmlStandard.IsEuropean || xmlStandard.Originator == Originator.Iec || holder == string.Empty || statement == string.Empty)
                    return copyrightNotice;
                copyrightNotice = string.Concat(GetCopyrightHolder(xmlStandard), " - ", GetCopyrightStatement(xmlStandard));
                return copyrightNotice;
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("Probleem 'Copyright' andmete leidmisega ISO XML failist: ", ex.Message)); }
        }
        public static string GetCopyrightHolder(XmlStandard xmlStandard) {
            try {
                var org = xmlStandard.XDoc.Descendants().Elements(xmlStandard.MetaStructure).FirstOrDefault()?
                    .Descendants("copyright-holder").FirstOrDefault()?.Value;
                var year = xmlStandard.XDoc.Descendants().Elements(xmlStandard.MetaStructure).FirstOrDefault()?
                    .Descendants("copyright-year").FirstOrDefault()?.Value;
                if (org == null || year == null) return string.Empty;
                if (xmlStandard.IsEuropean) return string.Concat("© ", year, " ", org);
                return string.Concat("© ", org, " ", year);
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("Probleem 'Copyright' andmete leidmisega XML failist: ", ex.Message)); }
        }
        public static string GetCopyrightStatement(XmlStandard xmlStandard) {
            try {
                var statement = xmlStandard.XDoc.Descendants().Elements(xmlStandard.MetaStructure).FirstOrDefault()?
                    .Descendants("copyright-statement").FirstOrDefault()?.Value;
                if (statement == null) return string.Empty;
                var cenIdentifier = string.Concat(ElementNames.Cen, " ");
                if (statement.StartsWith(cenIdentifier)) {
                    statement = statement.Substring(cenIdentifier.Length);
                }
                return statement;
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("Probleem 'Copyright' tekstide leidmisega XML failist: ", ex.Message)); }
        }
        public static bool CheckIfPureIso(List<XmlStandard> xmlStandardList) {
            try {
                foreach (var xmlStandard in xmlStandardList) {
                    if (xmlStandard.Originator == Originator.Iso) continue;
                    return false;
                }
                return true;
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("Probleem 'Originator' väljaga: ", ex.Message)); }
        }
        public static XDocument FindXDocFromDirectory(DirectoryInfo xmlDirectory) {
            try {
                var xmlPath = xmlDirectory.GetFiles("*.xml").FirstOrDefault(x => !x.Name.Contains("meta-data"));
                if (xmlPath == null) throw new ConversionException("Vähemalt ühes kaustas puudub XML fail");
                var xDoc = XDocument.Load(xmlPath.FullName, LoadOptions.PreserveWhitespace);
                return xDoc;
            }
            catch (Exception) { throw new ConversionException(string.Concat("Vähemalt ühes kaustas puudub XML fail")); }
        }
        public static XElement FindEvsMeta(XDocument xDoc) {
            try {
                var evsMeta = xDoc.Descendants(ElementNames.NatMeta)
                    .FirstOrDefault(d => d.Attribute("originator")?.Value == ElementNames.Evs);
                if (evsMeta == null)
                    throw new ConversionException("Peamisel XML failil puuduvad Eesti standardi metaandmed");
                return evsMeta;
            }
            catch (Exception) { throw new ConversionException(string.Concat("Peamisel XML failil puuduvad Eesti standardi metaandmed")); }
        }
        public static XElement FindOriginalMeta(XDocument xDoc) {
            try {
                var metaStructure = GetMetaStructure(xDoc);
                var originalMeta = xDoc.Descendants(metaStructure).FirstOrDefault();
                if (originalMeta == null)
                    throw new ConversionException("XML faili metastruktuur ei ole äratuntav");
                return originalMeta;
            }
            catch (Exception) { throw new ConversionException(string.Concat("XML faili metastruktuur ei ole äratuntav")); }
        }
        public static string GetTitleString(XElement metaElement, string language) {
            try {
                XNamespace xmlNamespace = "http://www.w3.org/XML/1998/namespace";
                var titleString = metaElement.Elements("title-wrap")
                    .FirstOrDefault(e => e.Attribute(xmlNamespace + "lang")?.Value == language)?
                    .Element("full")?.Value;
                return titleString;
            }
            catch (Exception ex) { throw new ConversionException(string.Concat("XML failis puudub toote pealkiri: ", ex.Message)); }
        }
        public static string GetReleaseDate(XDocument xDoc) {
            try {
                var metaStructure = GetMetaStructure(xDoc);
                var releaseDate = string.Empty;
                var structure = xDoc.Descendants(metaStructure).FirstOrDefault();
                if (structure == null) return releaseDate;
                var xmlDateElement = structure.Element("pub-date") ?? structure.Element("release-date");
                if (xmlDateElement != null) releaseDate = FormatDate(xmlDateElement.Value);
                if (releaseDate == null) {
                    xmlDateElement = structure.Element("release-date");
                    releaseDate = FormatDate(xmlDateElement.Value);
                }
                if (releaseDate == string.Empty || releaseDate == null)
                    throw new ConversionException(string.Concat("XML failis puudub avaldamise kuupäev"));
                return releaseDate;
            }
            catch (Exception) { throw new ConversionException(string.Concat("XML failis puudub avaldamise kuupäev")); }
        }
        public static bool IsOdd(int value) {
            return value % 2 != 0;
        }
        public static string RemoveYearFromEvsReference(string evsReference) {
            if (evsReference.Substring(evsReference.Length - 5, 2) == ":2") evsReference = evsReference.Remove(evsReference.Length - 5);
            return evsReference;
        }
    }
}
