﻿using Saxon.Api;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Aids {
    public class MicroConverter {
        public static FileInfo TransformXmlToHtml(XmlStandard xmlStandard, FileInfo html) {
            var processor = new Processor();
            var compiler = processor.NewXsltCompiler();
            var nisoStsXsl = new FileInfo(Path.Combine(AppSettings["StsTools"], AppSettings["NisoStsXsl"]));
            var executable = compiler.Compile(new Uri(nisoStsXsl.FullName));
            var destination = new DomDestination();
            xmlStandard.XDoc.Save(xmlStandard.Xml.FullName);
            //TODO: if error with nisosts xsl
            using (var inputStream = xmlStandard.Xml.OpenRead()) {
                var transformer = executable.Load();
                transformer.SetInputStream(inputStream,
                    new Uri(xmlStandard.Xml.DirectoryName ?? throw new ConversionException("XML kaust on vigane")));
                transformer.Run(destination);
                inputStream.Close();
            }
            destination.XmlDocument.Save(html.FullName);
            if (xmlStandard.Xml.FullName.Contains("modified")) File.Delete(xmlStandard.Xml.FullName);
            return html;
        }
        public static HtmlDocument FileInfoToHtmlDoc(FileInfo html) {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(html.FullName);
            return htmlDoc;
        }
        public static string FormatDate(string dateString) {
            string formattedString;
            try {
                formattedString = dateString.Contains("-") 
                    ? DateTime.ParseExact(dateString, "yyyy-MM-dd", CultureInfo.InvariantCulture).ToString("dd'.'MM'.'yyyy") 
                    : DateTime.Parse(dateString, CultureInfo.InvariantCulture).ToString("dd'.'MM'.'yyyy");
            }
            catch { return null; }
            return formattedString;
        }
        public static string DateTimeToHyphenString(DateTime dateTime) {
            string dateString;
            try {
                dateString = dateTime.ToString("yyyy-MM-dd");
            }
            catch { throw new ConversionException("Kuupäev ei ole õigel kujul"); }
            return dateString;
        }
        public static DateTime StringToDateTime(string dateString) {
            DateTime date;
            try {
                if (dateString.Contains("-"))
                    date = DateTime.ParseExact(dateString, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                else if (dateString.Contains("."))
                    date = DateTime.ParseExact(dateString, "dd'.'MM'.'yyyy", CultureInfo.InvariantCulture);
                else date = DateTime.Parse(dateString);
            }
            catch { throw new ConversionException("Kuupäev ei ole õigel kujul"); }
            return date;
        }
        public static string GetMonthAndYearFromDateTime(DateTime dateTime) {
            string monthYearString;
            try {
                var format = new CultureInfo("en-US").DateTimeFormat;
                var month = format.GetMonthName(dateTime.Month);
                var year = dateTime.Year;
                monthYearString = string.Concat(month, " ", year);
            }
            catch { throw new ConversionException( "Kuupäev ei ole õigel kujul"); }
            return monthYearString;
        }
        public static string GetYearFromString(string dateString) {
            //use FormatDate method before using this
            DateTime date;
            try {
                date = DateTime.ParseExact(dateString, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            }
            catch { throw new ConversionException("Kuupäev ei ole õigel kujul"); }
            return date.Year.ToString();
        }
        public static string DateTimeToEvsMonth(DateTime date) {
            var monthNumber = int.Parse(date.ToString("MM"));
            switch (monthNumber) {
                case 1:
                    return "jaanuarikuu";
                case 2:
                    return "veebruarikuu";
                case 3:
                    return "märtsikuu";
                case 4:
                    return "aprillikuu";
                case 5:
                    return "maikuu";
                case 6:
                    return "juunikuu";
                case 7:
                    return "juulikuu";
                case 8:
                    return "augustikuu";
                case 9:
                    return "septembrikuu";
                case 10:
                    return "oktoobrikuu";
                case 11:
                    return "novembrikuu";
                case 12:
                    return "detsembrikuu";
            }
            throw new ConversionException("Kuupäev ei ole õigel kujul");
        }
        public static List<T> Swap<T>(List<T> list, int indexA, int indexB) {
            var tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }
        public static string ConstructName(string directoryName, string fileName, string extension, string addition = "") {
            var name = Path.Combine(directoryName, fileName + addition + extension);
            return name;
        }
        public static DirectoryInfo CreateOrReferenceDirectory(string directoryFullName) {
            DirectoryInfo imageDirectory;
            try { imageDirectory = Directory.CreateDirectory(directoryFullName); }
            catch { imageDirectory = new DirectoryInfo(directoryFullName); }
            return imageDirectory;
        }
    }
}
