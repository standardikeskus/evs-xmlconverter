//1. Doing rotated table cells correctly
window.addEventListener('load', function () {
    var rotates = document.getElementsByClassName('table-rotated-container');
    for (var i = 0; i < rotates.length; i++) {
		rotates[i].parentElement.style.transform = 'none';
        rotates[i].parentElement.style.height = rotates[i].offsetWidth + 'px';
        rotates[i].parentElement.style.width = rotates[i].offsetHeight + 'px';
    }
});

//2. Add width to tables
window.addEventListener('load', function () {
    var wraps = document.getElementsByClassName('sts-table-wrap');
    for (var j = 0; j < wraps.length; j++) {
		var tables = wraps[j].getElementsByTagName("table");
		for (var i = 0; i < tables.length; i++) {
			if (tables[i].width == "" || tables[i].width === undefined) {
				var att = document.createAttribute("width");
				if (tables[i].offsetWidth > 870) att.value = 668;
				else if (tables[i].offsetWidth > 667) att.value = 668;
				else att.value = tables[i].offsetWidth;
				tables[i].setAttributeNode(att);
			}
		}
    }
});

//3. Reducting font-size for tables to fit the page
window.addEventListener('load', function ()  {
	var tables = document.getElementsByTagName("table");
	for(var i = 0; i < tables.length; i++) {
	    if (tables[i].width == "") continue;
	    var escape = 0;
	    var off = tables[i].offsetWidth;
	    var width = tables[i].width;
	    while (Math.round(tables[i].width) != Math.round(tables[i].offsetWidth) 
	    && Math.round(tables[i].width) + 1 != Math.round(tables[i].offsetWidth) 
	    && Math.round(tables[i].width) - 1 != Math.round(tables[i].offsetWidth)) {
			var fontSize = parseFloat(window.getComputedStyle(tables[i], null).getPropertyValue('font-size'));
			var allElements = tables[i].querySelectorAll('td, th, sup');
			for (let i = 0; i < allElements.length; i++) {
				reduceFontSize(allElements[i]);
			}
	    escape+=1;
	    if (escape == 10) break;
	    continue;
	    }
	}
});

function reduceFontSize(item) {
	var fontSize = parseFloat(window.getComputedStyle(item, null).getPropertyValue('font-size'));
	var reduced = fontSize - 1;
	var reducedFontSize = reduced + 'px';
	item.style.setProperty('font-size', reducedFontSize);
};