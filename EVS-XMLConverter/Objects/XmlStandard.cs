﻿using System;
using System.IO;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Classificators;
using HtmlAgilityPack;

namespace EVS_XMLConverter.Objects {
    public class XmlStandard {
        //xml
        public FileInfo Xml { get; set; }
        public XDocument XDoc { get; set; }
        public DirectoryInfo XmlDirectory { get; set; }
        public DirectoryInfo ImageDirectory { get; set; }

        //html
        public HtmlDocument HtmlDoc { get; set; }

        //iso
        public bool IsPureIso { get; set; }
        public bool IsEuropean { get; set; }
        public bool IsEnIso { get; set; }

        //parameters
        public EvsData EvsData { get; set; }
        public Originator StaDISOrganisation { get; set; }
        public Originator Originator { get; set; }
        public Originator SpecificOriginator { get; set; }
        public string MetaStructure { get; set; }
        public bool IsAmendment { get; set; }
        public string EnTitle { get; set; }
        public DateTime ReleaseDate { get; set; }

    }
}
