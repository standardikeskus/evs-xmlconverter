﻿using System.IO;

namespace EVS_XMLConverter.Objects {
    public class FinalFiles {
        public FileInfo EvsXmlPackage { get; set; }
        public FileInfo HtmlFull { get; set; }
        public FileInfo HtmlPreview { get; set; }
        public FileInfo PdfFull { get; set; }
        public FileInfo PdfPreview { get; set; }
        public FileInfo ConversionLog { get; set; }
    }
}
