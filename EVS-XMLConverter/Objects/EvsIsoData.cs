﻿using System;

namespace EVS_XMLConverter.Objects {
    public class EvsIsoData {
        public DateTime BulletinIssue { get; set; } = DateTime.MinValue;
        public string Scope { get; set; }
        public string Committee { get; set; }
        public string Amendments { get; set; }
    }
}
