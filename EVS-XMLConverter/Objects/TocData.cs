﻿using HtmlAgilityPack;
using System.Collections.Generic;

namespace EVS_XMLConverter.Objects {
    public class TocData {
        public int TocPriority { get; set; }
        public string TocTitle { get; set; }
        public string TocClass { get; set; }
        public string TocReference { get; set; }
        public List<TocData> ChildTocDatas { get; set; } = new List<TocData>();
        public HtmlNode TocElement{ get; set; }
    }
}
