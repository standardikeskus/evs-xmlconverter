﻿using System.Collections.Generic;
using System.IO;

namespace EVS_XMLConverter.Objects {
    public class EvsData {
        public List<FileInfo> XmlZipFiles { get; set; }
        public FileInfo EvsXmlPackageDestination { get; set; }
        public FileInfo PdfDestination { get; set; }
        public FileInfo PdfPreviewDestination { get; set; }
        public string FolderName { get; set; }
        public string FileName { get; set; }
        public string StaDISOrganisation { get; set; }
        public string EtTitle { get; set; }
        public string EnTitle { get; set; }
        public string EvsReference { get; set; }
        public List<string> IcsGroups { get; set; }
        public EvsIsoData EvsIsoData { get; set; }
    }
}
