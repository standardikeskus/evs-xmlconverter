﻿namespace EVS_XMLConverter.Logic.Classificators {
    public class TemplateElementNames {
        //EVS
        public const string EtTitle = "evs-title-";
        public const string EnTitleAlone = "evs-title-en-alone";
        public const string EvsReferenceEt = "evs-reference-et";
        public const string EvsReferenceEn = "evs-reference-en";
        public const string EvsForewordScope = "evs-foreword-scope";
        public const string EvsBulletinDateEt = "evs-bulletin-date-et";
        public const string EvsBulletinDateEn = "evs-bulletin-date-en";
        public const string EvsTableOfContents = "evs-table-of-contents";
        public const string StsIsoHeader = "sts-iso-header";
        public const string StsIecHeader = "sts-iec-header";
        public const string StsIsoHeaderRight = "sts-iso-header-right";
        public const string StsIsoMainTitle = "sts-iso-main-title";
        public const string StsIsoComplTitle = "sts-iso-compl-title";
        public const string StsIsoComplTitlePart = "sts-iso-compl-title-part";
        public const string StsIsoComplTitleAmd = "sts-iso-compl-title-amd";

        //Cen/Cenelec
        public const string EnTitlepageHeaderReference = "en-titlepage-header-reference";
        public const string EnTitlepageHeaderDate = "en-titlepage-header-date";
        public const string EnTitlepageSupersedes = "en-titlepage-supersedes";
        public const string EnTitlepageTitleEn = "en-titlepage-title-en";
        public const string EnTitlepageTitleFr = "en-titlepage-title-fr";
        public const string EnTitlepageTitleDe = "en-titlepage-title-de";
        public const string EnTitlepageSections = "en-titlepage-sections";
        public const string EnCopyright = "en-copyright";
        public const string EnCopyrightStatement = "en-copyright-statement";
        public const string EnReference = "en-reference";
        public const string EnDateOfAvailability = "en-doa";
        public const string EnDateOfAvailabilityEt = "en-doa-et";
        public const string EnDateOfAvailabilityEn = "en-doa-en";
        public const string EnIcsGroups = "en-ics-groups";

        //Iec
        public const string IecTitlepageHeaderReference = "iec-titlepage-header-reference";
        public const string IecTitlepageFullReference = "iec-titlepage-full-reference";
        public const string IecTitlepageHeaderDate = "iec-titlepage-header-date";
        public const string IecTitlepageHeaderEdition = "iec-titlepage-header-edition";
        public const string IecTitlepageTitle = "iec-titlepage-title-";
        public const string IecTitlepagePreTitle = "iec-titlepage-pre-title-";
        public const string IecCopyrightYear = "iec-copyright-year";
        public const string IecTitlepageIcs = "iec-titlepage-ics";
        public const string IecTitlepageIsbn = "iec-titlepage-isbn";

        //MAIN
        public const string StsEmptypage = "sts-empty-page";
        public const string StsBackCoverEvs = "sts-back-cover-evs";
        public const string StsForewordEvs = "sts-foreword-evs";
        public const string StsTitlepageEn = "sts-titlepage-en";
        public const string StsTitlepageIec = "sts-titlepage-iec";
        public const string StsCopyrightIec = "sts-copyright-iec";
        public const string StsMain = "sts-main";
        public const string StsStandard = "sts-standard";
        public const string TocIdentifier = "toc_";
        public const string TocTitle = "toc-title";
        public const string TocTitlepage = "toc-titlepage";
        public const string CopyrightNotice = "copyright-notice";
        public const string StsEndorsementNotice = "sts-endorsement-notice";
        public const string StsEndorsement = "sts-endorsement";
        public const string StsFirstSection = "sts-first-section";
        public const string FirstIsoSection = "first-iso-section";
        public const string StsSectionIntro = "sts-section-intro";
        public const string StsSectionBibl = "sts-section-bibl";
        public const string StsRefListHeader = "sts-ref-list-header";
        public const string StsRefList = "sts-ref-list";
        public const string SupDiv = "sup-div";
        public const string SupLink = "sup-link";
        public const string SupLinkTable = "sup-link-table";
        public const string StsForeword = "sts-foreword";
        public const string BsFontChar = "bsFont-char";
        public const string BsFontRight = "bsFont-right";
        public const string StsXref = "sts-xref";
        public const string StsXrefSupLink = "sts-xref sup-link";
        public const string StsFootnotes = "sts-footnotes";
        public const string StsFn = "sts-fn";
        public const string StsFootnote = "sts-footnote";
        public const string StsFig = "sts-fig";
        public const string StsFigGroup = "sts-fig-group";
        public const string FigIndex = "fig-index";
        public const string StsArray = "sts-array";
        public const string StsArrayContent = "sts-array-content";
        public const string StsCaption = "sts-caption";
        public const string StsCaptionLabel = "sts-caption-label";
        public const string StsLabel = "sts-label";
        public const string StsP = "sts-p";
        public const string StsPSupDiv = "sts-p sup-div";
        public const string StsSection = "sts-section";
        public const string StsSecTitle = "sts-sec-title";
        public const string StsAmdSecTitle = "sts-amd-sec-title";
        public const string StsBlankDiv = "sts-blank-div";
        public const string StsBlank = "sts-blank";
        public const string StsDispFormulaPanel = "sts-disp-formula-panel";
        public const string StsDispFormulaLabel = "sts-disp-formula-label";
        public const string InlineFormula = "inline-formula";
        public const string StsFormulaText = "sts-formula-text";
        public const string StsTbxEntailedTerm = "sts-tbx-entailedTerm";
        public const string StsTbxSec = "sts-tbx-sec";
        public const string StsTbxSource = "sts-tbx-source";
        public const string StsEditingInstruction = "sts-editing-instruction";

        public const string StsRotatedTd = "sts-rotated-td";
        public const string TableRotatedContainer = "table-rotated-container";
        public const string StsTableEnBorderless = "sts-table-en-borderless";
        public const string StsBoxedTable = "sts-boxed-table";
        public const string StsTableNormal = "sts-table-normal";
        public const string StsTableWrap = "sts-table-wrap";
        public const string StsTableWrapFoot = "sts-table-wrap-foot";
        public const string SideturnedTable = "sideturned-table";
        public const string PortraitPage = "portrait-page";
        public const string LandscapePage = "landscape-page";
        public const string StsApp = "sts-app";
        public const string StsAppHeader = "sts-app-header";
        public const string StsAppHeaderMain = "sts-app-header-main";
        public const string StsIsoTop = "sts-iso-top";
        public const string CenteredContent = "centered-content";
        public const string ImgTd = "img-td";
        public const string StsImgContainer = "sts-img-container";
        public const string StsNonNormativeNote = "sts-non-normative-note";
        public const string StsNonNormativeNoteTd = "sts-non-normative-note-td";
    }
}
