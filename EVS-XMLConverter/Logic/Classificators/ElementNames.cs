﻿using System.Collections.Generic;

namespace EVS_XMLConverter.Logic.Classificators {
    public class ElementNames {
        public const string Cen = "CEN";
        public const string Clc = "CLC";
        public const string Cenelec = "CENELEC";
        public const string Iso = "ISO";
        public const string Iec = "IEC";
        public const string CenIso = "CEN/ISO";
        public const string IsoCen = "ISO/CEN";
        public const string ClcIec = "CLC/IEC";
        public const string IecClc = "IEC/CLC";
        public const string CenClc = "CEN/CLC";
        public const string IsoIec = "ISO/IEC";
        public const string Evs = "EVS";
        public const string Unknown = "Unknown";

        public const string EnIso = "EN ISO";

        public const string RegMeta = "reg-meta";
        public const string IsoMeta = "iso-meta";
        public const string StdMeta = "std-meta";
        public const string NatMeta = "nat-meta";

        public static readonly List<string> Amendments = new List<string> { "amd", "AM", "AMD" };

        public const string Estonian = "et";
        public const string English = "en";
        public const string German = "de";
        public const string French = "fr";
    }
}
