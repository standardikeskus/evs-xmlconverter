﻿using System;
using EVS_XMLConverter.Aids;

namespace EVS_XMLConverter.Logic.Classificators {
    public class Originator {
        public readonly string _originatorName;
        public Originator(string originatorName) {
            _originatorName = originatorName;
        }

        public static Originator Iso = new Originator(ElementNames.Iso);
        public static Originator Iec = new Originator(ElementNames.Iec);
        public static Originator Cen = new Originator(ElementNames.Cen);
        public static Originator Clc = new Originator(ElementNames.Clc);
        public static Originator CenIso = new Originator(ElementNames.CenIso);
        public static Originator IsoCen = new Originator(ElementNames.IsoCen);
        public static Originator IecClc = new Originator(ElementNames.IecClc);
        public static Originator ClcIec = new Originator(ElementNames.ClcIec);
        public static Originator CenClc = new Originator(ElementNames.CenClc);
        public static Originator Evs = new Originator(ElementNames.Evs);
        public static Originator Unknown = new Originator(ElementNames.Unknown);

        public static Originator GenerateOriginator(string name, bool specific) {
            try {
                switch (name) {
                    case ElementNames.CenIso:
                        switch (specific) {
                            case true:
                                return CenIso;
                            default:
                                return Cen;
                        }
                    case ElementNames.ClcIec:
                        switch (specific) {
                            case true:
                                return ClcIec;
                            default:
                                return Clc;
                        }
                    case ElementNames.IsoCen:
                        switch (specific) {
                            case true:
                                return IsoCen;
                            default:
                                return Iso;
                        }
                    case ElementNames.CenClc:
                        return CenClc;
                    case ElementNames.Iso:
                        return Iso;
                    case ElementNames.Iec:
                        return Iec;
                    case ElementNames.Cen:
                        return Cen;
                    case ElementNames.Clc:
                        return Clc;
                    case ElementNames.Cenelec:
                        return Clc;
                    case ElementNames.Evs:
                        return Evs;
                    case ElementNames.Unknown:
                        return Unknown;
                    default:
                        throw new ConversionException(string.Concat("Probleem standardimisorganisatsiooni tuvastamisega: ", name));
                }
            }
            catch (Exception) {
                throw new ConversionException(string.Concat("Probleem standardimisorganisatsiooni tuvastamisega: ", name));
            }
        } 
    }
}
