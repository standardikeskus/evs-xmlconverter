﻿using System.IO;
using System.Linq;
using EVS_XMLConverter.Aids;
using HtmlAgilityPack;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Logic.Pdf {
    public class PreviewGenerator {
        public static HtmlDocument GeneratePreview(HtmlDocument htmlDoc, DirectoryInfo directory, string fileName) {
            var fullName = MicroConverter.ConstructName(directory.FullName, fileName, ".html", AppSettings["HtmlPreview"]);
            var previewDoc = Clone(htmlDoc, fullName);
            var normativeReference = previewDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Id.Contains("_sec_2"));
            while (normativeReference?.NextSibling != null) normativeReference.NextSibling.Remove();
            var mainDiv = normativeReference?.ParentNode;
            while (mainDiv?.NextSibling != null) mainDiv.NextSibling.Remove();
            previewDoc.Save(fullName);
            return previewDoc;
        }
        public static HtmlDocument Clone(HtmlDocument htmlDoc, string directory) {
            HtmlHelper.SaveHtml(htmlDoc, directory);
            var cloneDoc = new HtmlDocument();
            cloneDoc.Load(directory);
            return cloneDoc;
        }
    }
}
