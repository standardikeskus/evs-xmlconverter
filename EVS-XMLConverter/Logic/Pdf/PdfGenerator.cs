﻿using System;
using System.IO;
using System.Linq;
using PdfSharp.Pdf.IO;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Aids.MicroConverter;
using static EVS_XMLConverter.Aids.FinalFileMover;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverter.Logic.Xml.XmlModifier;
using static EVS_XMLConverter.Logic.Html.HtmlCombiner;
using static EVS_XMLConverter.Logic.Html.XslConverter;
using static EVS_XMLConverter.Logic.Zip.EvsXmlPackageUnpacker;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Logic.Pdf {
    public class PdfGenerator {
        public static FinalFiles XmlToPdf(EvsData evsData, FileInfo evsXmlPackage) {
            var temporaryFolder = new DirectoryInfo(string.Concat(AppSettings["Temporary"], evsData.FolderName));
            ExtractPackageToDestination(evsXmlPackage, temporaryFolder);
            var xmlDirectories = FindXmlZipDirectories(temporaryFolder);
            
            var xmlStandardList = CompileXmlStandardList(xmlDirectories, evsData);
            xmlStandardList = XmlsToHtmls(xmlStandardList);
            var htmlPackage = Combine(xmlStandardList, evsData);
            var finalFiles = new FinalFiles {
                HtmlFull = htmlPackage.Item1,
                HtmlPreview = htmlPackage.Item2,
                PdfFull = HtmlToPdf(htmlPackage.Item1, false, xmlStandardList[0].EvsData.FileName),
                PdfPreview = HtmlToPdf(htmlPackage.Item2, true, xmlStandardList[0].EvsData.FileName),
                ConversionLog = new FileInfo(ConstructName(htmlPackage.Item1.DirectoryName, AppSettings["ConversionLog"], string.Empty))
            };
            //if Committee is not null = EVS ISO standard => No EvsXmlPackage
            if (evsData.EvsIsoData.Committee == null) finalFiles.EvsXmlPackage = evsXmlPackage;
            MoveAndStructureFilesToDestination(evsData, finalFiles, temporaryFolder);
            return finalFiles;
        }
        public static FileInfo HtmlToPdf(FileInfo html, bool preview, string fileName) {
            var newFileName = ConstructName(html.DirectoryName, fileName, ".pdf",
                preview ? AppSettings["PdfPreview"] : AppSettings["PreEvsPdf"]);
            var pdf = ConvertHtmlToPdf(html, fileName, preview);
            var evsPdf = preview ? new FileInfo(newFileName) : AddEmptyPagesToPdf(pdf, html, false, fileName);
            var pdfDoc = PdfReader.Open(evsPdf.FullName);
            return evsPdf;
        }
        public static FileInfo ConvertHtmlToPdf(FileInfo html, string fileName, bool preview) {
            var prn = new Prince(AppSettings["Prince"]);
            if (preview) fileName = string.Concat(fileName, AppSettings["PdfPreview"]);
            var filePath = ConstructName(html.DirectoryName, fileName, ".pdf");
            prn.SetLog(ConstructName(html.DirectoryName, "prince_log", ".txt"));
            prn.AddScript(string.Concat(AppSettings["Templates"], AppSettings["PrinceScript"]));
            if (preview) prn.AddStyleSheet(string.Concat(AppSettings["Templates"], AppSettings["PreviewText"]));
            prn.Convert(html.FullName, filePath);
            return new FileInfo(filePath);
        }
        public static FileInfo AddEmptyPagesToPdf(FileInfo pdf, FileInfo html, bool preview, string fileName) {
            try {
                var htmlDoc = new HtmlDocument();
                htmlDoc.Load(html.FullName);
                var backpage = htmlDoc.DocumentNode.Descendants("div").FirstOrDefault(d => d.Id == StsBackCoverEvs);

                var pdfDoc = PdfReader.Open(pdf.FullName, PdfDocumentOpenMode.Import);
                File.Delete(pdf.FullName);
                var lacking = CalculateEmptyPages(pdfDoc.PageCount);
                for (var i = 0; i < lacking; i++) { AddEmptypageBeforeBackpage(htmlDoc, backpage); }
                htmlDoc.Save(html.FullName);
                
                ConvertHtmlToPdf(html, fileName, preview);
                var path = new FileInfo(ConstructName(html.Directory?.FullName, fileName, ".pdf"));
                return path;
            }
            catch (Exception) { throw new ConversionException("Pdf fail on avatud"); }
        }
        public static int CalculateEmptyPages(int numberOfPages) {
            var lacking = 0;
            if (numberOfPages <= 52) {
                var surplus = numberOfPages % 4;
                if (surplus != 0) lacking = 4 - surplus;
            }
            else {
                var surplus = numberOfPages % 2;
                if (surplus != 0) lacking = 2 - surplus;
            }
            return lacking;
        }
        public static HtmlDocument AddEmptypageBeforeBackpage(HtmlDocument htmlDoc, HtmlNode backpage) {
            var emptyPage = htmlDoc.CreateElement("div");
            AddAttributeToNode(emptyPage, "class", StsStandard);
            AddAttributeToNode(emptyPage, "class", StsEmptypage);
            var blankDiv = htmlDoc.CreateElement("div");
            AddAttributeToNode(blankDiv, "class", StsBlankDiv);
            var blank = htmlDoc.CreateElement("div");
            AddAttributeToNode(blank, "class", StsBlank);
            blank.InnerHtml = "[Blank page]";
            blankDiv.AppendChild(blank);
            emptyPage.AppendChild(blankDiv);
            backpage.ParentNode.InsertBefore(emptyPage, backpage);
            return htmlDoc;
        }
    }
}
