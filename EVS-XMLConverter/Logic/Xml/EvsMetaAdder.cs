﻿using System;
using System.IO;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using static System.String;
using static EVS_XMLConverter.Aids.MicroConverter;
using static EVS_XMLConverter.Aids.XmlHelper;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Logic.Xml {
    public class EvsMetaAdder {
        public static XDocument CreateEvsXDocument(EvsData evsData) {
            var natMeta = new XElement("nat-meta", new XAttribute("originator", ElementNames.Evs));
            XNamespace xmlNamespace = "http://www.w3.org/XML/1998/namespace";

            var titleWrapEt = new XElement("title-wrap", new XAttribute(xmlNamespace + "lang", ElementNames.Estonian));
            CreateXElementIntoXElement("main", titleWrapEt);
            CreateXElementIntoXElement("full", titleWrapEt, evsData.EtTitle);
            natMeta.Add(titleWrapEt);

            var titleWrapEn = new XElement("title-wrap", new XAttribute(xmlNamespace + "lang", ElementNames.English));
            CreateXElementIntoXElement("main", titleWrapEn);
            CreateXElementIntoXElement("full", titleWrapEn, evsData.EnTitle);
            natMeta.Add(titleWrapEn);

            var docIdent = new XElement("doc-ident");
            CreateXElementIntoXElement("sdo", docIdent, ElementNames.Evs);
            CreateXElementIntoXElement("proj-id", docIdent);
            CreateXElementIntoXElement("language", docIdent);
            CreateXElementIntoXElement("release-version", docIdent);
            natMeta.Add(docIdent);

            var stdIdent = new XElement("std-ident");
            CreateXElementIntoXElement("originator", stdIdent, ElementNames.Evs);
            CreateXElementIntoXElement("doc-type", stdIdent);
            CreateXElementIntoXElement("doc-number", stdIdent);
            CreateXElementIntoXElement("edition", stdIdent);
            CreateXElementIntoXElement("version", stdIdent);
            natMeta.Add(stdIdent);

            CreateXElementIntoXElement("content-language", natMeta);
            CreateXElementIntoXElement("std-ref", natMeta, evsData.EvsReference, "type", "dated");
            CreateXElementIntoXElement("std-ref", natMeta, RemoveYearFromEvsReference(evsData.EvsReference), "type", "undated");
            CreateXElementIntoXElement("doc-ref", natMeta);
            CreateXElementIntoXElement("pub-date", natMeta);
            CreateXElementIntoXElement("release-date", natMeta);
            CreateXElementIntoXElement("comm-ref", natMeta, evsData.EvsIsoData.Committee);
            CreateXElementIntoXElement("secretariat", natMeta);
            foreach (var icsGroup in evsData.IcsGroups) CreateXElementIntoXElement("ics", natMeta, icsGroup);
            
            //adding custom EVS metadata
            var customMetaGroup = new XElement("custom-meta-group");
            if (!IsNullOrEmpty(evsData.EvsIsoData.Scope)) {
                var customMetaScope = new XElement("custom-meta");
                CreateXElementIntoXElement("meta-name", customMetaScope, "scope");
                CreateXElementIntoXElement("meta-value", customMetaScope, evsData.EvsIsoData.Scope);
                customMetaGroup.Add(customMetaScope);
            }
            if (!IsNullOrEmpty(evsData.EvsIsoData.Amendments)) {
                var customMetaAmendments = new XElement("custom-meta");
                CreateXElementIntoXElement("meta-value", customMetaAmendments, evsData.EvsIsoData.Amendments);
                CreateXElementIntoXElement("meta-name", customMetaAmendments, "amendments");
                customMetaGroup.Add(customMetaAmendments);
            }
            if (evsData.EvsIsoData.BulletinIssue != null && evsData.EvsIsoData.BulletinIssue != DateTime.MinValue) {
                var customMetaBulletin = new XElement("custom-meta");
                CreateXElementIntoXElement("meta-name", customMetaBulletin, "bulletin-issue");
                CreateXElementIntoXElement("meta-value", customMetaBulletin, FormatDate(DateTimeToHyphenString(evsData.EvsIsoData.BulletinIssue)));
                customMetaGroup.Add(customMetaBulletin);
            }
            natMeta.Add(customMetaGroup);

            var front = new XElement("front"); front.Add(natMeta);
            var standard = new XElement("standard"); standard.Add(front);
            var body = new XElement("body"); front.AddAfterSelf(body);
            var xDoc = new XDocument(standard);
            return xDoc;
        }

        public static XElement CreateXElementIntoXElement(string elementName, XElement container, 
            string elementValue = "", string attributeName = "", string attributeValue = "") {
            if (elementValue == null) elementValue = Empty;
            var element = IsNullOrEmpty(attributeName) || IsNullOrEmpty(attributeValue)
                ? new XElement(elementName, elementValue)
                : new XElement(elementName, new XAttribute(attributeName, attributeValue), elementValue);
            container.Add(element);
            return container;
        }
    }
}
