﻿using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Objects;
using EVS_XMLConverter.Logic.Classificators;
using static EVS_XMLConverter.Aids.XmlHelper;
using static EVS_XMLConverter.Aids.MicroConverter;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Logic.Xml {
    public class XmlModifier {
        public static List<XmlStandard> CompileXmlStandardList(List<DirectoryInfo> xmlDirectories, EvsData evsData) {
            var xmlStandardList = PrepareXmlStandardList(xmlDirectories);
            foreach (var xmlStandard in xmlStandardList) xmlStandard.StaDISOrganisation = Originator.GenerateOriginator(evsData.StaDISOrganisation, true);
            xmlStandardList = ModifyXmlStandardList(xmlStandardList);
            xmlStandardList = SequenceXmlStandardList(xmlStandardList);
            xmlStandardList[0].EvsData = evsData;
            return xmlStandardList;
        }
        public static List<XmlStandard> PrepareXmlStandardList(List<DirectoryInfo> xmlDirectories) {
            var xmlStandardList = new List<XmlStandard>();
            foreach (var xmlDirectory in xmlDirectories) {
                var xmlStandard = new XmlStandard { XmlDirectory = xmlDirectory, XDoc = FindXDocFromDirectory(xmlDirectory) };
                if (xmlStandard.XDoc.DocumentType != null) xmlStandard.XDoc.DocumentType.SystemId =
                        new FileInfo(Path.Combine(AppSettings["StsTools"], AppSettings["IsoStsDtd"])).FullName;
                xmlStandard.ReleaseDate = StringToDateTime(GetReleaseDate(xmlStandard.XDoc));
                xmlStandard.MetaStructure = GetMetaStructure(xmlStandard.XDoc);
                xmlStandardList.Add(xmlStandard);
            }
            return xmlStandardList;
        }
        public static List<XmlStandard> SequenceXmlStandardList(List<XmlStandard> xmlStandardList) {
            xmlStandardList = xmlStandardList.OrderBy(x => x.ReleaseDate).ToList();
            if (xmlStandardList.Count < 1) return xmlStandardList;
            for (var i = 1; i < xmlStandardList.Count; i+=2) {
                var specificOriginator = GetOriginator(xmlStandardList[i], xmlStandardList[i - 1], true);
                if (specificOriginator == Originator.CenIso || specificOriginator == Originator.ClcIec) {
                    Swap(xmlStandardList, i - 1, i);
                }
            }
            return xmlStandardList;
        }
        public static List<XmlStandard> ModifyXmlStandardList(List<XmlStandard> xmlStandardList) {
            var isPureIso = CheckIfPureIso(xmlStandardList);
            for (var i = 0; i < xmlStandardList.Count; i++) {
                xmlStandardList[i].IsPureIso = isPureIso;
                if (i == 0) ModifyXmlStandard(xmlStandardList[i], null);
                else ModifyXmlStandard(xmlStandardList[i], xmlStandardList[i - 1]);
            }
            foreach (var xmlStandard in xmlStandardList) {
                //Võimalik stsenaarium: CEN/CLC + ISO või IEC.. Uurida ja täiendada DetermineOriginatorit
                xmlStandard.SpecificOriginator =
                    DetermineOriginator(xmlStandard.Originator._originatorName, xmlStandard.IsEnIso, true);
                MoveSectionsToBody(xmlStandard.XDoc, xmlStandard.SpecificOriginator);
            }
            return xmlStandardList;
        }
        public static XmlStandard ModifyXmlStandard(XmlStandard xmlStandard, XmlStandard previousXmlStandard) {
            var xmlDirectoryFile = xmlStandard.XmlDirectory.GetFiles("*.xml").FirstOrDefault();
            if (xmlDirectoryFile == null) throw new ConversionException("Vähemalt ühes kaustas puudub XML fail");
            if (xmlStandard.XmlDirectory.GetFiles("*.xml").Length > 1) LogWriter.LogWrite("WARNING: Zip fail sisaldas rohkem kui ühte XML faili");
            xmlStandard.Xml = new FileInfo(Path.Combine(xmlStandard.XmlDirectory.FullName, @"modified-" + xmlDirectoryFile.Name));
            xmlStandard.Originator = GetOriginator(xmlStandard, null, false);
            xmlStandard.IsEuropean = CheckIfEuropean(xmlStandard.Originator);
            xmlStandard.IsEnIso = CheckIfEnIso(xmlStandard, previousXmlStandard);
            if (previousXmlStandard != null) previousXmlStandard.IsEnIso = xmlStandard.IsEnIso;
            xmlStandard.IsAmendment = CheckIfAmendment(xmlStandard.XDoc);
            return xmlStandard;
        }
        public static XDocument MoveSectionsToBody(XDocument xDoc, Originator specificOriginator) {
            var body = xDoc.Descendants("body").FirstOrDefault();
            var sections = xDoc.Descendants("front").FirstOrDefault()?.Descendants("sec").ToList();
            if (sections == null) return xDoc;
            for (var i = sections.Count - 1; i >= 0; i--) {
                if (sections[i]?.Attributes("sec-type").FirstOrDefault()?.Value == "titlepage") continue;
                if (specificOriginator != null) {
                    //Kas on tõene, et nii tahtsime? Uurida
                    if (specificOriginator == Originator.IsoCen) {
                        if (sections[i]?.Attributes("sec-type").FirstOrDefault()?.Value == "foreword") continue;
                    }
                }
                body?.AddFirst(sections[i]);
                sections[i]?.Remove();
            }
            return xDoc;
        }
    }
}
