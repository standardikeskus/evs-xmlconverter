﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using EVS_XMLConverter.Aids;

namespace EVS_XMLConverter.Logic.Zip {
    public class EvsXmlPackageUnpacker {
        public static DirectoryInfo ExtractPackageToDestination(FileInfo evsXmlPackage, DirectoryInfo destination) {
            using (var packageStream = new FileStream(evsXmlPackage.FullName, FileMode.Open)) {
                new ZipArchive(packageStream, ZipArchiveMode.Update).ExtractToDirectory(destination.FullName, true);
                packageStream.Close();
            }
            return destination;
        }
        public static List<DirectoryInfo> FindXmlZipDirectories(DirectoryInfo temporaryFolder) {
            //TODO: Corrupt ZIP files
            var xmlDirectories = new List<DirectoryInfo>();
            foreach (var zipFile in temporaryFolder.GetFiles("*.zip")) {
                var zipDirectory = Directory.CreateDirectory(Path.Combine(temporaryFolder.FullName, Path.GetFileNameWithoutExtension(zipFile.Name)));
                using (var zipStream = new FileStream(zipFile.FullName, FileMode.Open)) {
                    new ZipArchive(zipStream, ZipArchiveMode.Update).ExtractToDirectory(zipDirectory.FullName, false);
                    zipStream.Close();
                }
                xmlDirectories.Add(zipDirectory);
            }
            return xmlDirectories;
        }
    }
}
