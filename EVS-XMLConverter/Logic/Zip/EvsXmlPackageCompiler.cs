﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using EVS_XMLConverter.Logic.Xml;
using EVS_XMLConverter.Objects;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Logic.Zip {
    public class EvsXmlPackageCompiler {
        public static FileInfo CompileEvsXmlPackage(EvsData evsData) {
            evsData.FileName = Path.GetFileNameWithoutExtension(evsData.PdfDestination.Name);
            evsData.FolderName = Path.GetFileNameWithoutExtension(evsData.PdfDestination.Name);
            var temporary = new DirectoryInfo(Path.Combine(AppSettings["Temporary"], evsData.FolderName));
            var evsXmlName = string.Concat(evsData.FolderName, "_EVS-XML");

            //empty temporary folder and create new ones (ignores if exists)
            DeleteEmptyAndRecreateFolders(temporary, evsXmlName);

            var destination = new FileInfo(Path.Combine(temporary.FullName, evsXmlName, evsXmlName + ".zip"));
            if (File.Exists(destination.FullName)) File.Delete(destination.FullName);
            using (var archive = ZipFile.Open(destination.FullName, ZipArchiveMode.Create)) {
                var evsXDoc = EvsMetaAdder.CreateEvsXDocument(evsData);
                var xmlFileName = string.Concat(evsData.FileName, ".xml");
                var metaData = Path.Combine(AppSettings["Temporary"], xmlFileName);
                evsXDoc.Save(metaData);
                archive.CreateEntryFromFile(metaData, xmlFileName);
                File.Delete(metaData);

                //renaming possbile duplicates
                RenameAndInsertFilesToArchive(evsData.XmlZipFiles, archive);
            }
            return destination;
        }

        public static void DeleteEmptyAndRecreateFolders(DirectoryInfo temporary, string evsXmlName) {
            try { Directory.Delete(temporary.FullName, true); }
            catch { /* ignored*/; }
            Directory.CreateDirectory(AppSettings["Temporary"]);
            Directory.CreateDirectory(temporary.FullName);
            Directory.CreateDirectory(Path.Combine(temporary.FullName, evsXmlName));
        }
        public static ZipArchive RenameAndInsertFilesToArchive(List<FileInfo> zipFiles, ZipArchive archive) {
            var i = 1;
            foreach (var zipFile in zipFiles) {
                var zipName = zipFile.Name;
                if (zipFiles.Count(z => z.Name == zipFile.Name) > 1) {
                    zipName = zipName.Insert(zipName.IndexOf(".zip", StringComparison.Ordinal), "_" + i);
                    i++;
                }
                archive.CreateEntryFromFile(zipFile.FullName, zipName);
            }
            return archive;
        }
    }
}
