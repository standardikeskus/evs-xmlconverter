﻿using System.Linq;
using HtmlAgilityPack;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;

namespace EVS_XMLConverter.Logic.Html {
    public class AmendmentModifier {
        public static HtmlDocument ChangeAmendmentSectionTitleClasses(HtmlDocument htmlDoc, bool isAmendment) {
            if (isAmendment == false) return htmlDoc;
            var sectionTitles = FindNodesByClassName(htmlDoc.DocumentNode, StsSecTitle);
            foreach (var sectionTitle in sectionTitles) {
                //amendment sections need different css settings
                sectionTitle.Attributes["class"].Value =
                    sectionTitle.Attributes["class"].Value.Replace(StsSecTitle, StsAmdSecTitle);
            }
            return htmlDoc;
        }
        public static HtmlDocument GroupAmendmentSections(HtmlDocument htmlDoc, bool isAmendment) {
            if (isAmendment == false) return htmlDoc;
            FindAndAlignQuotes(htmlDoc);
            var possibleTitles = FindNodesByClassName(htmlDoc.DocumentNode, StsP);
            foreach (var title in possibleTitles) {
                //trying to find small nodes (titles) to group with their sections
                if (title.InnerLength < 100) {
                    var nextSibling = FindNextSibling(title);
                    if (nextSibling != null) {
                        nextSibling.Remove();
                        title.AppendChild(nextSibling);
                    }
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument FindAndAlignQuotes(HtmlDocument htmlDoc) {
            var quoteEnds = FindNodesByClassName(htmlDoc.DocumentNode, StsP).Where(d => d.InnerText.EndsWith("”.") && d.InnerText.Length < 20).ToList();
            foreach (var quoteEnd in quoteEnds) AddAttributeToNode(quoteEnd, "style", "text-align: right; ");
            return htmlDoc;
        }
    }
}
