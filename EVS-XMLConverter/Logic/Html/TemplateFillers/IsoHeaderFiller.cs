﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Objects;
using HtmlAgilityPack;
using EVS_XMLConverter.Logic.Classificators;
using static EVS_XMLConverter.Aids.XmlHelper;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static System.Configuration.ConfigurationManager;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;

namespace EVS_XMLConverter.Logic.Html {
    public class IsoHeaderFiller {
        public static HtmlDocument AddAndFillIsoHeader(HtmlDocument htmlBaseBody, XmlStandard xmlStandard) {
            try {
                var htmlDoc = new HtmlDocument();
                if (xmlStandard.Originator == Originator.Iso) htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["IsoHeader"]), Encoding.UTF8);
                else if (xmlStandard.Originator == Originator.Iec && !xmlStandard.IsAmendment) htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["IecHeader"]), Encoding.UTF8);
                else return htmlBaseBody;

                var htmlDiv = FindNodesByClassName(htmlDoc.DocumentNode, StsIsoHeader).FirstOrDefault();
                if (htmlDiv == null) htmlDiv = FindNodesByClassName(htmlDoc.DocumentNode, StsIecHeader).FirstOrDefault();

                if (xmlStandard.Originator == Originator.Iso) {
                    if (xmlStandard.IsAmendment) FindNodesByClassName(htmlDoc.DocumentNode, StsIsoTop).FirstOrDefault().Remove();
                    else FindNodesByClassName(htmlDoc.DocumentNode, StsIsoHeaderRight)
                        .FirstOrDefault().InnerHtml = FindOriginalMeta(xmlStandard.XDoc).Elements("std-ref")
                        .FirstOrDefault(e => e.Attribute("type")?.Value == "dated")?.Value;
                }

                var stsIsoComplTitle = FindNodesByClassName(htmlDoc.DocumentNode, StsIsoComplTitle).FirstOrDefault();
                var stsIsoMainTitle = FindNodesByClassName(htmlDoc.DocumentNode, StsIsoMainTitle).FirstOrDefault();

                //full = main + compl
                var fullTitle = GetTitleString(FindOriginalMeta(xmlStandard.XDoc), ElementNames.English);
                var index = fullTitle.LastIndexOf('—');
                if (index <= 0) index = fullTitle.LastIndexOf(" - ");
                var mainTitle = fullTitle.Substring(0, index + 2);
                while (mainTitle.Contains("Part")) mainTitle = mainTitle.Substring(0, mainTitle.LastIndexOf(" - ") + 2);
                var complTitle = fullTitle.Substring(mainTitle.Length + 1);

                //only seperate if more than one '—', or contains Part or AMENDMENT
                if (!string.IsNullOrEmpty(mainTitle) && (mainTitle.Count(m => m == '—') > 1 
                    || complTitle?.StartsWith("Part ") == true || complTitle?.Contains("AMENDMENT") == true)) {
                    stsIsoMainTitle.InnerHtml = mainTitle;
                    
                    //Part or AMENDMENT separation
                    if (complTitle?.StartsWith("Part ") == true || complTitle?.Contains("AMENDMENT") == true) {
                        var partString = string.Empty;
                        if (complTitle.StartsWith("Part ")) {
                            partString = complTitle.Substring(0, complTitle.IndexOf(":") + 1);
                            complTitle = complTitle.Substring(complTitle.IndexOf(":") + 2);
                        }
                        var complTitleAmd = htmlDoc.CreateElement(string.Empty);
                        if (complTitle.Contains("AMENDMENT")) {
                            var complTitleAmdString = complTitle.Substring(complTitle.IndexOf("AMENDMENT"));
                            complTitle = complTitle.Substring(0, complTitle.IndexOf("AMENDMENT") - 1);
                            complTitleAmd = htmlDoc.CreateElement("div");
                            AddAttributeToNode(complTitleAmd, "class", StsIsoComplTitleAmd);
                            complTitleAmd.InnerHtml = complTitleAmdString;
                        }
                        stsIsoComplTitle.InnerHtml = complTitle;
                        if (partString != string.Empty) {
                            var complTitlePart = htmlDoc.CreateElement("div");
                            AddAttributeToNode(complTitlePart, "class", StsIsoComplTitlePart);
                            complTitlePart.InnerHtml = partString;
                            stsIsoComplTitle.PrependChild(complTitlePart);
                        }
                        if (!string.IsNullOrEmpty(complTitleAmd.InnerHtml)) stsIsoComplTitle.AppendChild(complTitleAmd);
                    }
                    else stsIsoComplTitle.InnerHtml = complTitle;
                }
                else {
                    stsIsoMainTitle.InnerHtml = fullTitle;
                    stsIsoComplTitle.Remove();
                }
                //sec-foreword is for IEC
                var sectionMarkers = new List<string> { "sec_0", "sec_1", "section_0", "section_1", "sec-foreword" };
                var firstSection = htmlBaseBody.DocumentNode.Descendants().FirstOrDefault(d => sectionMarkers.Any(s => d.Id.Contains(s)));
                AddAttributeToNode(firstSection, "class", "first-iso-section");
                if (firstSection == null) {
                    LogWriter.LogWrite("WARNING: Probleem 'Scope' leidmise ja ISO päise lisamisega.");
                    return htmlBaseBody;
                }
                firstSection.PrependChild(htmlDiv);
                return htmlBaseBody;
            }
            catch (Exception) {
                throw new ConversionException("Ei saanud koostada ISO päist. Ebatraditsiooniline pealkirja struktuur");
            }
        }
    }
}
