﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using static EVS_XMLConverter.Aids.MicroConverter;
using static EVS_XMLConverter.Aids.XmlHelper;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using EVS_XMLConverter.Aids;

namespace EVS_XMLConverter.Logic.Html.TemplateFillers {
    public class TemplateFiller {
        public static HtmlDocument ReplaceTitles(HtmlDocument htmlDoc, EvsData evsData, string language, bool quotes) {
            var titles = FindNodesByClassName(htmlDoc.DocumentNode, string.Concat(EtTitle, language));
            foreach (var title in titles) {
                switch (language) {
                    case ElementNames.Estonian:
                        if (evsData.EtTitle == null) {
                            title.Remove();
                            break;
                        }
                        title.InnerHtml = evsData.EtTitle;
                        break;
                    case ElementNames.English:
                        if (evsData.EtTitle == null)  title.Id = EnTitleAlone;
                        title.InnerHtml = evsData.EnTitle;
                        break;
                }
                if (title.InnerHtml != string.Empty && quotes) title.InnerHtml = string.Concat('„', title.InnerHtml, '“');
            }
            return htmlDoc;
        }
        public static HtmlDocument ReplaceReferences(HtmlDocument htmlDoc, XmlStandard xmlStandard, bool evs) {
            var referenceNode = evs ? EvsReferenceEt : EvsReferenceEn;
            var references = htmlDoc.DocumentNode.Descendants("span")
                .Where(d => d.Attributes["class"]?.Value.Contains(referenceNode) == true);
            foreach (var reference in references) {
                if (evs) reference.InnerHtml = xmlStandard.EvsData.EvsReference;
                else {
                    var metaElement = FindOriginalMeta(xmlStandard.XDoc);
                    reference.InnerHtml = metaElement.Elements("std-ref")
                        .FirstOrDefault(e => e.Attribute("type")?.Value == "dated")?.Value;
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument ReplaceDates(HtmlDocument htmlDoc, DateTime date) {
            var availabilityDates = htmlDoc.DocumentNode.Descendants("span")
                .Where(d => d.Attributes["class"]?.Value.Contains(EnDateOfAvailability) == true);
            foreach (var availabilityDate in availabilityDates) {
                availabilityDate.InnerHtml = FormatDate(DateTimeToHyphenString(date));
            }
            return htmlDoc;
        }
        public static HtmlDocument AddForewordAmendments(HtmlDocument htmlDoc, List<XmlStandard> xmlStandardList) {
            if (xmlStandardList.Count < 2) return htmlDoc;
            if (xmlStandardList.Count == 2 && xmlStandardList.Count(x => x.IsAmendment) == 2) return htmlDoc;
            var isMultiple = xmlStandardList.Count(x => x.IsAmendment && !x.IsEuropean) > 1;
            var evsAmendmentStatements = isMultiple ? " ja selle muudatuste " : " ja selle muudatuse ";
            var natAmendmentStatements = isMultiple ? " and its amendments " : " and its amendment ";
            var evsAmendmentDates = isMultiple ? ", muudatused " : ", muudatuse ";
            var natAmendmentDates = isMultiple ? ", for amendments " : ", for amendment ";

            bool atLeastOne = false;
            var count = xmlStandardList.Count;
            for (var i = 1; i < count; i++) {
                if (!xmlStandardList[i].IsAmendment || xmlStandardList[i].IsEuropean) continue;
                atLeastOne = true;
                var last = xmlStandardList[i].XDoc == xmlStandardList[count - 1].XDoc;
                var penultimate = count > 2 && xmlStandardList[i].XDoc == xmlStandardList[count - 2].XDoc;
                
                var amendmentLong = FindAmendmentVersion(xmlStandardList[i].XDoc, true);
                if (string.IsNullOrEmpty(amendmentLong)) continue;
                if (last) {
                    evsAmendmentStatements += amendmentLong;
                    natAmendmentStatements += amendmentLong;
                }
                else {
                    evsAmendmentStatements += penultimate ? string.Concat(amendmentLong, " ja ") : string.Concat(amendmentLong, ", ");
                    natAmendmentStatements += penultimate ? string.Concat(amendmentLong, " and ") : string.Concat(amendmentLong, ", ");
                }
                if (!xmlStandardList[0].IsEuropean) continue;
                var amendmentShort = FindAmendmentVersion(xmlStandardList[i].XDoc, false);
                if (string.IsNullOrEmpty(amendmentShort)) continue;

                var amendmentDoa = xmlStandardList[i - 1].ReleaseDate;
                evsAmendmentDates += string.Concat(amendmentShort, " ", amendmentDoa.ToShortDateString());
                natAmendmentDates += string.Concat(amendmentShort, " ", amendmentDoa.ToShortDateString());
            }
            if (atLeastOne == false) return htmlDoc;
            AddAmendmentStatements(htmlDoc, evsAmendmentStatements, natAmendmentStatements);
            if (xmlStandardList[0].IsEuropean) AddAmendmentDates(htmlDoc, evsAmendmentDates, natAmendmentDates);
            return htmlDoc;
        }
        public static HtmlDocument AddAmendmentStatements(HtmlDocument htmlDoc, string evsAmendmentStatements, string natAmendmentStatements) {
            try {
                var evsAmendmentSpan = htmlDoc.CreateElement("span");
                evsAmendmentSpan.InnerHtml = evsAmendmentStatements;
                var evsReferenceNode = htmlDoc.GetElementbyId(EvsReferenceEt);
                evsReferenceNode.ParentNode.InsertAfter(evsAmendmentSpan, evsReferenceNode);
                var natAmendmentSpan = htmlDoc.CreateElement("span");
                natAmendmentSpan.InnerHtml = natAmendmentStatements;
                var natReferenceNode = htmlDoc.GetElementbyId(EvsReferenceEn);
                natReferenceNode.ParentNode.InsertAfter(natAmendmentSpan, natReferenceNode);
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument AddAmendmentDates(HtmlDocument htmlDoc, string evsAmendmentDates, string natAmendmentDates) {
            try {
                var evsAmendmentDateSpan = htmlDoc.CreateElement("span");
                evsAmendmentDateSpan.InnerHtml = evsAmendmentDates;
                var evsDateNode = htmlDoc.GetElementbyId(EnDateOfAvailabilityEt);
                evsDateNode.ParentNode.InsertAfter(evsAmendmentDateSpan, evsDateNode);
                var natAmendmentDateSpan = htmlDoc.CreateElement("span");
                natAmendmentDateSpan.InnerHtml = natAmendmentDates;
                var natDateNode = htmlDoc.GetElementbyId(EnDateOfAvailabilityEn);
                natDateNode.ParentNode.InsertAfter(natAmendmentDateSpan, natDateNode);
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceIcsGroups(HtmlDocument htmlDoc, XDocument xDoc, EvsData evsData = null) {
            //if EvsData is null, read from xml
            try {
                List<string> icsGroups;
                if (evsData != null) {
                    icsGroups = evsData.IcsGroups;
                }
                else {
                    var metaElement = FindOriginalMeta(xDoc);
                    icsGroups = metaElement.Elements("ics").Select(e => e.Value).ToList();
                }

                for (var i = 0; i < icsGroups.Count; i++) {
                    var icsSpan = htmlDoc.CreateElement("span");
                    icsSpan.InnerHtml = icsGroups[i];
                    if (i != icsGroups.Count - 1) icsSpan.InnerHtml += ", ";
                    var icsElements = htmlDoc.DocumentNode.Descendants()
                        .Where(d => d.Attributes["class"]?.Value == EnIcsGroups);
                    foreach (var element in icsElements) element.AppendChild(icsSpan);
                }
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceForewordScope(HtmlDocument htmlDoc, EvsIsoData evsIsoData) {
            try {
                var evsForewordScope = htmlDoc.GetElementbyId(EvsForewordScope);
                if (string.IsNullOrEmpty(evsIsoData.Scope)) {
                    evsForewordScope.ParentNode.ParentNode.Remove();
                    return htmlDoc;
                }
                var splitString = evsIsoData.Scope.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                foreach (var split in splitString.ToList()) {
                    var splitDiv = htmlDoc.CreateElement("span");
                    var preSplitDiv = htmlDoc.CreateElement("span");
                    AddAttributeToNode(preSplitDiv, "class", "list-span");
                    var newStartIndex = 0;
                    for (int i = 0; i < split.Length; i++) {
                        if (split[i] == '-' || split[i] == '―') preSplitDiv.InnerHtml = split[i].ToString();
                        if (split[i] == '-' || split[i] == '―' || split[i] == ' ') continue;
                        newStartIndex = i;
                        break;
                    }
                    splitDiv.InnerHtml = split.Substring(newStartIndex);
                    evsForewordScope.AppendChild(splitDiv);
                    if (!string.IsNullOrEmpty(preSplitDiv.InnerHtml)) splitDiv.PrependChild(preSplitDiv);
                }
                return htmlDoc;
            }
            catch(Exception) {
                throw new ConversionException("Käsitlusala tekst on ebatavaline"); 
            }
        }
        public static HtmlDocument ReplaceCommittees(HtmlDocument htmlDoc, EvsIsoData evsIsoData) {
            var evsCommittees = htmlDoc.DocumentNode.Descendants("span")
                .Where(d => d.Attributes["class"]?.Value == "evs-committee");
            foreach (var committee in evsCommittees) {
                if (evsIsoData.Committee == null) continue;
                committee.InnerHtml = evsIsoData.Committee;
            }
            return htmlDoc;
        }
        public static HtmlDocument ReplaceBulletinDate(HtmlDocument htmlDoc, EvsIsoData evsIsoData) {
            try {
                var bulletinDate = evsIsoData.BulletinIssue;
                htmlDoc.GetElementbyId(EvsBulletinDateEt).InnerHtml =
                    BulletinDateToWords(bulletinDate, false);
                htmlDoc.GetElementbyId(EvsBulletinDateEn).InnerHtml =
                    BulletinDateToWords(bulletinDate, true);
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static string BulletinDateToWords(DateTime date, bool isEnglish) {
            var worded = isEnglish 
                ? string.Concat(date.ToString("MMMM", new CultureInfo("en-UK")), " ", date.Year) 
                : string.Concat(date.Year, ". aasta ", DateTimeToEvsMonth(date));
            return worded;
        }

        public static string FindAmendmentVersion(XDocument xDoc, bool yearNeeded) {
            var std = xDoc.Descendants(GetMetaStructure(xDoc)).FirstOrDefault()?.Element("std-ident");
            var amendmentCheck = std?.Elements("suppl-type").FirstOrDefault()?.Value == "amd";
            if (!amendmentCheck) return string.Empty;
            var amdNumber = std.Elements("suppl-number").FirstOrDefault()?.Value;
            if (yearNeeded == false) return string.Concat("A", amdNumber);
            var amdYear = GetYearFromString(GetReleaseDate(xDoc));
            return string.Concat("A", amdNumber, ":", amdYear);
        }
        public static HtmlDocument ReplaceReferenceWithoutYear(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                bool undated = string.IsNullOrEmpty(FindOriginalMeta(xDoc)
                .Elements("std-ref").FirstOrDefault(e => e.Attribute("type")?.Value == "dated")?.Value);
                var enTitlepageNode = htmlDoc.GetElementbyId(EnTitlepageHeaderReference);
                if (!undated) {
                    var datedValue = FindOriginalMeta(xDoc).Elements("std-ref")
                        .FirstOrDefault(e => e.Attribute("type")?.Value == "dated")?.Value;
                    enTitlepageNode.InnerHtml = datedValue?.Substring(0, datedValue.Length - 5);
                }
                else {
                    enTitlepageNode.InnerHtml = FindOriginalMeta(xDoc)
                        .Elements("std-ref").FirstOrDefault(e => e.Attribute("type")?.Value == "undated")?.Value;
                }
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceDateYearAndMonth(HtmlDocument htmlDoc, XDocument xDoc, DateTime releaseDate) {
            try {
                htmlDoc.GetElementbyId(EnTitlepageHeaderDate).InnerHtml =
                GetMonthAndYearFromDateTime(releaseDate);
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceSupersedes(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                var supersedes = FindOriginalMeta(xDoc).Elements("std-xref")
                    .Where(e => e.Attribute("type")?.Value == "Revises" || e.Attribute("type")?.Value == "consolidates")
                    .Elements("std-ref").Where(e => e.Attribute("type")?.Value == "dated")
                    .Where(e => e.Value.Contains("prA") == false).ToList();
                if (!supersedes.Any()) return htmlDoc;
                var preSpan = htmlDoc.CreateElement("span");
                preSpan.InnerHtml = "Supersedes ";
                htmlDoc.GetElementbyId(EnTitlepageSupersedes).AppendChild(preSpan);
                foreach (var supersede in supersedes) {
                    var supersedeSpan = htmlDoc.CreateElement("span");
                    if (supersede == supersedes.Last()) {
                        supersedeSpan.InnerHtml += supersede.Value;
                    }
                    else supersedeSpan.InnerHtml += string.Concat(supersede.Value, ", ");
                    var enTitlepageSupersedes = htmlDoc.GetElementbyId(EnTitlepageSupersedes);
                    if (enTitlepageSupersedes.PreviousSibling.Name == "#text") enTitlepageSupersedes.PreviousSibling.Remove();
                    htmlDoc.GetElementbyId(EnTitlepageSupersedes).AppendChild(supersedeSpan);
                }
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceEuropeanTitlepageTitles(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                var metaElement = FindOriginalMeta(xDoc);
                htmlDoc.GetElementbyId(EnTitlepageTitleEn).InnerHtml = GetTitleString(metaElement, ElementNames.English);
                htmlDoc.GetElementbyId(EnTitlepageTitleFr).InnerHtml = GetTitleString(metaElement, ElementNames.French);
                htmlDoc.GetElementbyId(EnTitlepageTitleDe).InnerHtml = GetTitleString(metaElement, ElementNames.German);
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceSections(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                var sections = FindOriginalMeta(xDoc).Parent?.Elements("sec")
                .Where(e => e.Attribute("sec-type")?.Value == "titlepage").Elements("p");
                if (sections == null) return htmlDoc;
                foreach (var section in sections) {
                    var sectionDiv = htmlDoc.CreateElement("div");
                    sectionDiv.InnerHtml = section.Value;
                    htmlDoc.GetElementbyId(EnTitlepageSections).AppendChild(sectionDiv);
                }
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceCopyright(HtmlDocument htmlDoc, XmlStandard xmlStandard) {
            try {
                htmlDoc.GetElementbyId(EnCopyright).InnerHtml 
                    = GetCopyrightHolder(xmlStandard);
                htmlDoc.GetElementbyId(EnCopyrightStatement).InnerHtml 
                    = GetCopyrightStatement(xmlStandard);
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceReferenceNumber(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                htmlDoc.GetElementbyId(EnReference).InnerHtml += string.Concat(FindOriginalMeta(xDoc)
                    .Elements("std-ref").FirstOrDefault(e => e.Attribute("type")?.Value == "dated")?.Value, " E");
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
    }
}
