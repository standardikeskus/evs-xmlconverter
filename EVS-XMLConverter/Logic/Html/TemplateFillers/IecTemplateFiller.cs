﻿using HtmlAgilityPack;
using System.Linq;
using System.Xml.Linq;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverter.Aids.XmlHelper;
using System;
using System.Collections.Generic;

namespace EVS_XMLConverter.Logic.Html.TemplateFillers {
    public class IecTemplateFiller {
        public static HtmlDocument ReplaceIecReference(HtmlDocument htmlDoc, XDocument xDoc, bool useDatedReference) {
            try {
                var reference = string.Empty;
                if (!useDatedReference) reference = FindOriginalMeta(xDoc).Elements("std-ref").FirstOrDefault(e => e.Attribute("type")?.Value == "undated")?.Value;
                else {
                    reference = FindOriginalMeta(xDoc).Elements("std-ref").FirstOrDefault(e => e.Attribute("type")?.Value == "dated")?.Value;
                    reference = reference.Substring(0, reference.Length - 5);
                }
                htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == IecTitlepageHeaderReference).InnerHtml = reference;
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceIecSideReference(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                var reference = string.Empty;
                reference = FindOriginalMeta(xDoc).Elements("std-ref").FirstOrDefault(e => e.Attribute("type")?.Value == "dated")?.Value;
                htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == IecTitlepageFullReference).InnerHtml = reference;
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceIecEdition(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                dynamic edition = double.Parse(FindOriginalMeta(xDoc).Descendants("edition").FirstOrDefault()?.Value);
                edition = string.Format("Edition {0:0.0}", edition).Replace(",", ".");
                htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == IecTitlepageHeaderEdition).InnerHtml = edition;
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceIecDate(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == IecTitlepageHeaderDate).InnerHtml = 
                    DateTime.Parse(GetReleaseDate(xDoc)).ToString("yyyy-MM");
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceIecTitles(HtmlDocument htmlDoc, XDocument xDoc, bool isAmendment) {
            try {
                var languages = new List<string> { "en", "fr" };
                XNamespace xmlNamespace = "http://www.w3.org/XML/1998/namespace";
                foreach (var language in languages) {
                    var french = language.Equals("fr");
                    var fullTitle = FindOriginalMeta(xDoc).Elements("title-wrap")
                    .FirstOrDefault(e => e.Attribute(xmlNamespace + "lang")?.Value == language)?
                    .Element("full")?.Value ?? string.Empty; ;
                    var titles = GetMainAndPartTitle(fullTitle, french);
                    var iecTitlepageTitle = htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == string.Concat(IecTitlepageTitle, language));
                    var mainTitle = titles.Item1;
                    if (isAmendment) {
                        var otherTitles = RemoveAmendmentFromTitle(titles.Item1, french);
                        mainTitle = otherTitles.Item2;
                        if (otherTitles.Item1 != null) {
                            var iecTitlepageAmdTitle = htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == string.Concat(IecTitlepagePreTitle, language));
                            iecTitlepageAmdTitle.InnerHtml = otherTitles.Item1;
                        }
                    }
                    iecTitlepageTitle.InnerHtml = mainTitle;
                    if (mainTitle != null) {
                        var partTitle = htmlDoc.CreateElement("div");
                        partTitle.InnerHtml = titles.Item2;
                        iecTitlepageTitle.AppendChild(partTitle);
                    }
                }
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static Tuple<string, string> GetMainAndPartTitle(string fullTitle, bool french = false) {
            try {
                var part = " Part ";
                if (french) part = " Partie ";
                var mainTitle = fullTitle.Substring(0, fullTitle.IndexOf(part) + 1);
                var partTitle = fullTitle.Substring(fullTitle.IndexOf(part) + 1);
                return new Tuple<string, string>(mainTitle, partTitle);
            }
            catch { return new Tuple<string, string>(fullTitle, null); }
        }
        public static Tuple<string, string> RemoveAmendmentFromTitle(string mainTitle, bool french = false) {
            try {
                var amd = "Amendment ";
                if (french) amd = "Amendement ";
                var cutoff = " - ";
                if (mainTitle.StartsWith(amd)) {
                    var cutoffIndex = mainTitle.IndexOf(cutoff);
                    var amdPart = mainTitle.Substring(0, cutoffIndex);
                    mainTitle = mainTitle.Substring(cutoffIndex + 3);
                    return new Tuple<string, string>(amdPart, mainTitle);
                }
                else return new Tuple<string, string>(null, mainTitle);
            }
            catch { return new Tuple<string, string>(null, mainTitle); }
        }
        public static HtmlDocument ReplaceIecIcsGroups(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                var icsGroups = FindOriginalMeta(xDoc).Elements("ics").ToList();
                var icsGroupsString = "ICS ";
                foreach (var ics in icsGroups) {
                    icsGroupsString += ics.Value;
                    if (ics != icsGroups.Last()) icsGroupsString += ", ";
                }
                htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == IecTitlepageIcs).InnerHtml = icsGroupsString;
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceIsbn(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                var isbn = string.Concat("ISBN ", FindOriginalMeta(xDoc).Elements("std-ident").FirstOrDefault().Elements("isbn").FirstOrDefault().Value);
                isbn = isbn.Insert(8, "-");
                isbn = isbn.Insert(10, "-");
                isbn = isbn.Insert(15, "-");
                isbn = isbn.Insert(20, "-");
                htmlDoc.DocumentNode.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == IecTitlepageIsbn).InnerHtml = isbn;
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
        public static HtmlDocument ReplaceIecCopyrightYear(HtmlDocument htmlDoc, XDocument xDoc) {
            try {
                htmlDoc.GetElementbyId(IecCopyrightYear).InnerHtml = DateTime.Parse(GetReleaseDate(xDoc)).Year.ToString();
                return htmlDoc;
            }
            catch { return htmlDoc; }
        }
    }
}
