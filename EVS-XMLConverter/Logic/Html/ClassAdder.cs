﻿using System.Linq;
using HtmlAgilityPack;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;

namespace EVS_XMLConverter.Logic.Html {
    public class ClassAdder {
        public static HtmlDocument ClassifyEndorsement(HtmlDocument htmlDoc) {
            var endorsementNotices = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.InnerHtml.StartsWith("Endorsement notice") && d.Name == "#text").ToList();
            if (endorsementNotices == null || !endorsementNotices.Any()) return htmlDoc;
            foreach (var notice in endorsementNotices) {
                if (notice.ParentNode != null) {
                    AddAttributeToNode(notice.ParentNode, "class", StsEndorsementNotice);
                    notice.Name = "div";
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument ClassifyIntros(HtmlDocument htmlDoc) {
            //IEC normative nodes have "not-introduction" in ID
            var intros = htmlDoc.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Id.Contains("intro") && !d.Id.Contains("not-intro")).ToList();
            if (intros == null) return htmlDoc;
            foreach (var intro in intros) {
                AddAttributeToNode(intro, "class", StsSectionIntro);
            }
            return htmlDoc;
        }
        public static HtmlDocument ClassifyBibliograpies(HtmlDocument htmlDoc) {
            var bibliographies = htmlDoc.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Id.Contains("bibl")).ToList();
            if (bibliographies == null || !bibliographies.Any()) return htmlDoc;
            foreach (var bibl in bibliographies) {
                if (bibl.ParentNode.Attributes["class"].Value.Contains("sts-app"))
                    AddAttributeToNode(bibl.ParentNode, "class", StsSectionBibl);
                else AddAttributeToNode(bibl, "class", StsSectionBibl);
                var headerNode = bibl.SelectNodes("//h1")
                    ?.Where(d => d.Attributes["class"]?.Value == StsRefListHeader).FirstOrDefault();
                if (headerNode == null) continue;
                headerNode.Remove();
                bibl.InsertBefore(headerNode, bibl.FirstChild);
                GroupRefLists(bibl);
            }
            return htmlDoc;
        }
        public static HtmlNode GroupRefLists(HtmlNode bibl) {
            var refLists = FindNodesByClassName(bibl, StsRefList).Where(b => b.Name != "h1").ToList();
            foreach (var refList in refLists) {
                if (refList == refLists.First()) continue;
                foreach (var refListElement in refList.Descendants("li").ToList()) {
                    refLists[0].Descendants("ol").FirstOrDefault().AppendChild(refListElement);
                    refList.Remove();
                }
            }
            return bibl;
        }
        public static HtmlDocument ClassifySupLinks(HtmlDocument htmlDoc) {
            //IEC does not signify table footnote links
            var tableFootNotes = htmlDoc.DocumentNode.SelectNodes("//a")?
                .Where(a => a.Attributes["href"]?.Value.Contains("tfn") == true).ToList();
            if (tableFootNotes != null) {
                foreach (var tfn in tableFootNotes) {
                    var sup = htmlDoc.CreateElement("sup");
                    foreach (var child in tfn.ChildNodes) {
                        sup.AppendChild(child);
                    }
                    tfn.RemoveAllChildren();
                    tfn.AppendChild(sup);
                    AddAttributeToNode(tfn, "class", SupLink);
                }
            }

            var links = htmlDoc.DocumentNode.SelectNodes("//a")?
                .Where(a => a.Descendants().Where(c => c.Name == "sup").FirstOrDefault() != null).Distinct().ToList();
            if (links == null) return htmlDoc;
            foreach (var link in links) {
                //remove space from previous node
                if (link.PreviousSibling != null) link.PreviousSibling.InnerHtml = link.PreviousSibling.InnerHtml.TrimEnd();
                if (link.Attributes["class"]?.Value.Contains(SupLink) == false) AddAttributeToNode(link, "class", SupLink);
                if (CheckIfInTable(link)) AddAttributeToNode(link, "class", SupLinkTable);
                //if (link.ParentNode.Name != "div" || link.ParentNode.Attributes["class"] == null) continue;
                var containsSupDiv = link.ParentNode.Attributes["class"]?.Value.Contains(SupDiv);
                if (containsSupDiv == false || containsSupDiv == null) AddAttributeToNode(link.ParentNode, "class", SupDiv);
            }
            return htmlDoc;
        }
        public static HtmlDocument ClassifyForewords(HtmlDocument htmlDoc) {
            var forewords = htmlDoc.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Id.Contains("sec_foreword")).ToList();
            if (forewords == null || !forewords.Any()) return htmlDoc;
            foreach (var foreword in forewords) AddAttributeToNode(foreword, "class", StsForeword);
            return htmlDoc;
        }
    }
}
