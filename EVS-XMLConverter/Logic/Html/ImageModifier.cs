﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Media.Imaging;
using HtmlAgilityPack;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Logic.Html {
    public class ImageModifier {
        public static HtmlDocument AddImageLocationsToHtml(HtmlDocument htmlDoc, XmlStandard xmlStandard) {
            var images = htmlDoc.DocumentNode.SelectNodes("//img");
            if (images == null || !images.Any()) return htmlDoc;
            foreach (var image in images) {
                var imageName = image.Attributes["alt"]?.Value;
                imageName = imageName.Substring(imageName.IndexOf('/') + 1);
                if (imageName == null) continue;
                var imageFileLocation = FindImage(xmlStandard.ImageDirectory, imageName);
                if (imageFileLocation == null)
                    //unfound images need to be handled
                    if (imageFileLocation == null) {
                        LogWriter.LogWrite(string.Concat("WARNING: Pilt '", imageName, "' on puudu."));
                        continue;
                    }
                AddAttributeToNode(image, "src", imageFileLocation);
                var imageParent = image.ParentNode.Attributes["class"]?.Value;
                var isEquation = false;
                if (imageParent != null) isEquation = imageParent?.Contains(StsFig) == false && imageParent?.Contains(StsSection) == false && imageParent?.Contains(StsImgContainer) == false;
                ResizeImage(image, xmlStandard, imageFileLocation, isEquation);
                //MoveArrayOutOfImage(image);
            }
            return htmlDoc;
        }
        public static void ConvertTifImageToPng(string imageFileLocation) {
            if (imageFileLocation == null) return;
            imageFileLocation = imageFileLocation.Replace(".png", ".tif");
            System.Drawing.Image.FromFile(imageFileLocation).Save(imageFileLocation.Replace(".tif", ".png"),
                System.Drawing.Imaging.ImageFormat.Png);
            return;
        }
        public static HtmlNode ResizeImage(HtmlNode imageNode, XmlStandard xmlStandard, string imageFileLocation, bool isEquation) {
            if (string.IsNullOrEmpty(imageFileLocation)) return imageNode;
            var originator = xmlStandard.Originator;
            if (originator == Originator.CenClc) originator = xmlStandard.StaDISOrganisation;

            using (var imageStream = File.OpenRead(imageFileLocation)) {
                var decoder = BitmapDecoder.Create(imageStream, BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.Default);
                var width = Convert.ToDouble(decoder.Frames[0].PixelWidth);
                if (!isEquation) {
                    if (originator == Originator.Cen) width *= double.Parse(AppSettings["CenImageModifier"]);
                    if (originator == Originator.Clc) width *= double.Parse(AppSettings["CenelecImageModifier"]);
                    if (originator == Originator.Iec) width *= double.Parse(AppSettings["IecImageModifier"]);
                }
                if (isEquation || originator == Originator.Iso) {
                    width *= double.Parse(AppSettings["IsoImageModifier"]);
                    if (isEquation && (originator == Originator.Cen || xmlStandard.Originator == Originator.Clc)) {
                        width = decoder.Frames[0].PixelWidth * double.Parse(AppSettings["CenClcEquationModifier"]);
                    }
                }
                if (width > 750 && LogWriter.ScaledError == false) {
                    LogWriter.LogWrite("WARNING: Pildid võivad olla valesti skaleeritud. Kontrollida.");
                    LogWriter.ScaledError = true;
                }
                //sideturned image
                if (width > 700) {
                    if (imageNode.ParentNode.Attributes["class"]?.Value.Contains(StsImgContainer) == true) TableModifier.AddPortraitOrLandscapeClassToParents(imageNode.ParentNode);
                    else TableModifier.AddPortraitOrLandscapeClassToParents(imageNode);
                }
                //stretch wide image
                else if (width > 668) { width = 668; }
                var styleValue = string.Concat("width: ", width.ToString(CultureInfo.InvariantCulture).Replace(",", "."), "px;");
                AddAttributeToNode(imageNode, "style", styleValue);
                imageStream.Close();
            }
            return imageNode;
        }
        public static HtmlDocument ReplaceMathElements(HtmlDocument htmlDoc, XmlStandard xmlStandard) {
            //method is unused for now
            var mathElements = htmlDoc.DocumentNode.SelectNodes("//math");
            if (mathElements == null) return htmlDoc;
            foreach (var mathElement in mathElements) {
                var imageName = string.Concat(mathElement.Id, ".png");
                var imageFileLocation = FindImage(xmlStandard.ImageDirectory, imageName);
                //no handling for unfound images
                if (imageFileLocation == null) continue;
                ReplaceImage(htmlDoc, mathElement, xmlStandard, imageFileLocation, true);
            }
            return htmlDoc;
        }
        public static HtmlDocument ReplaceImage(HtmlDocument htmlDoc, HtmlNode imageNode, XmlStandard xmlStandard,
            string imageFileLocation,  bool isEquation) {
            if (!File.Exists(imageFileLocation) || imageNode == null) return htmlDoc;
            AddAttributeToNode(imageNode, "src", imageFileLocation);
            imageNode.Name = "img";
            imageNode.InnerHtml = string.Empty;
            ResizeImage(imageNode, xmlStandard, imageFileLocation, isEquation);
            File.OpenRead(imageFileLocation).Close();
            return htmlDoc;
        }
        public static HtmlNode MoveArrayOutOfImage(HtmlNode imageNode) {
            if (imageNode.ParentNode?.ParentNode?.Attributes["class"]?.Value.Contains(StsFig) == false)
                return imageNode;
            var arrayNode = imageNode.ParentNode?.ParentNode?.ChildNodes
                .FirstOrDefault(c => c.Attributes["class"]?.Value.Contains(StsArray) == true);
            if (arrayNode == null) return imageNode;
            var parent = arrayNode.ParentNode;
            var duplicate = arrayNode;
            arrayNode.Remove();
            parent.ParentNode.InsertAfter(duplicate, parent);
            return imageNode;
        }
        public static HtmlDocument MoveCaptionToImageBottom(HtmlDocument htmlDoc) {
            var containers = FindNodesByClassName(htmlDoc.DocumentNode, StsFig);
            foreach (var container in containers) {
                var childNodes = container.ChildNodes.ToList();

                //iec sts-fig-group class has sts-caption deeper
                if (container.Attributes["class"]?.Value == StsFigGroup) childNodes = childNodes.FirstOrDefault(d => d.Name != "#text")?.ChildNodes.ToList();

                var caption = childNodes.FirstOrDefault(c => c.Attributes["class"]?.Value.Contains(StsCaption) == true);
                if (caption != null) {
                    var lastNode = childNodes.LastOrDefault();
                    if (lastNode != null) {
                        if (lastNode == caption) continue;
                        caption.Remove();
                        lastNode.ParentNode?.InsertAfter(caption, lastNode);
                        continue;
                    }
                }
            }
            return htmlDoc;
        }
    }
}
