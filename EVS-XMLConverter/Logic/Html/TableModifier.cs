﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using EVS_XMLConverter.Logic.Classificators;
using HtmlAgilityPack;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverter.Logic.Html.ImageModifier;
using static EVS_XMLConverter.Aids.HtmlHelper;
using EVS_XMLConverter.Objects;

namespace EVS_XMLConverter.Logic.Html {
    public class TableModifier {
        public static HtmlDocument ReplaceTableElements(HtmlDocument htmlDoc, XmlStandard xmlStandard) {
            var tableContainers = htmlDoc.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Id?.Contains("tab_") == true
                || d.Attributes["class"]?.Value == StsArray)?.ToList();
            if (tableContainers == null || !tableContainers.Any()) return htmlDoc;
            foreach (var tableContainer in tableContainers) {
                var tableElement = tableContainer.Descendants().FirstOrDefault(c => c.Name == "table");
                if (tableContainer.Id?.Contains("tab_") == true) {
                    var imageName = tableContainer.Id?.Substring(tableContainer.Id.IndexOf("tab_", StringComparison.Ordinal));
                    var tableName = string.Concat(imageName, ".png");
                    var imageFileLocation = FindImage(xmlStandard.ImageDirectory, tableName);
                    if (imageFileLocation != null) ReplaceImage(htmlDoc, tableElement, xmlStandard, imageFileLocation, false);
                    if (tableElement?.Name == "img") continue;
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument ResizeTables(HtmlDocument htmlDoc, bool isEuropean, DirectoryInfo xmlDirectory, Originator originator) {
            var tables = htmlDoc.DocumentNode.SelectNodes("//table");
            if (tables == null) return htmlDoc;
            foreach (var table in tables) {
                ResizeTableBorders(table);
                AddAlignmentToRotatedTableContent(htmlDoc, table);
                var landscape = table.ParentNode.Attributes["class"]?.Value.Contains("sts-landscape-orientation");
                var tableWidth = table.Attributes["width"]?.Value;
                if (tableWidth == null) continue;
                //default resizing
                var width = double.Parse(tableWidth) * 0.73;
                if (originator == Originator.ClcIec) width *= 1.102;
                //streth wide tables to fit page
                if (width < 661 && width > 659 && landscape == false) {
                    width = 661;
                }
                //strech over-the-edge tables to fit
                if (width > 661 && width < 685 && landscape == false) {
                    width = 661;
                    //removing colgroup for over-the-edge tables for precaution (only arrays)
                    if (table?.ParentNode?.Attributes["class"]?.Value?.Contains(StsArray) == true) {
                        var colgroup = table.Descendants().Where(d => d.Name == "col").ToList();
                        if (colgroup != null) foreach (var col in colgroup) col.Remove();
                    }
                }
                //sts-array has a margin
                if (table.ParentNode?.ParentNode?.Attributes["class"]?.Value.Contains(StsArray) == true) width = width - 12;
                table.Attributes["width"].Value = width.ToString(CultureInfo.InvariantCulture).Replace(",", ".");
            }
            return htmlDoc;
        }
        public static HtmlNode ResizeTableBorders(HtmlNode tableElement) {
            var borderNameList = new List<string> {"top", "left", "right", "bottom"};
            double actualBottom = 0;
            var trNodes = tableElement.Descendants("tr").ToList();
            for (var i = 0; i < trNodes.Count(); i++) {
                var borderNodes = trNodes[i].Descendants().Where(c => c.Attributes["style"]?.Value.Contains("border-") == true).ToList();

                var check = false;
                if (i == trNodes.Count() && check == false) {
                    //sts-array with borders should be a table wrap
                    if (borderNodes != null && borderNodes.Any() && tableElement?.ParentNode?.ParentNode?.Attributes["class"]?.Value == StsArray)
                        tableElement.ParentNode.ParentNode.Attributes["class"].Value = StsTableWrap;
                    //table-wrap without borders is aligned to the left
                    if (borderNodes == null || !borderNodes.Any()) AddAttributeToNode(tableElement, "class", "borderless-table");
                    check = true;
                }

                foreach (var borderNode in borderNodes) {
                    foreach (var borderName in borderNameList) {
                        var styleValue = borderNode.Attributes["style"]?.Value;
                        if (string.IsNullOrEmpty(styleValue)) return tableElement;
                        var last = tableElement.Descendants("tr").LastOrDefault().ChildNodes.FirstOrDefault(c => c.Name == "td") == borderNode
                            && borderNode.Attributes["rowspan"]?.Value == (trNodes.Count() - i).ToString()
                            && borderNode == borderNodes.FirstOrDefault();
                        var borderValue = ResizeTableBorder(borderNode, borderName, styleValue, actualBottom);
                        if (last == true && borderName == "left" && actualBottom == 0) {
                            actualBottom = borderValue;
                            AddAttributeToNode(tableElement, "style", string.Concat("border-bottom: solid ", actualBottom, "px;"));
                        }
                    }
                }
            }
            return tableElement;
        }
        public static double ResizeTableBorder(HtmlNode borderNode, string borderName, string styleValue, double actualBottom) {
            if (styleValue.Contains("none")) return 0;
            var expectedBorderString = string.Concat("border-", borderName, ": solid ");
            var borderValuePosition = styleValue.IndexOf(expectedBorderString, StringComparison.Ordinal) + expectedBorderString.Length;
            if (!double.TryParse(styleValue[borderValuePosition].ToString(), out _)) return 0;
            var borderValue = double.Parse(styleValue[borderValuePosition].ToString());
            if (borderValue > 2) borderValue *= 0.55;
            else return borderValue;
            borderValue = Math.Round(borderValue, 0);
            var stringBuilder = new StringBuilder(styleValue);
            //always expects borders to be defined by one number
            stringBuilder.Remove(borderValuePosition, 1);
            stringBuilder.Insert(borderValuePosition, borderValue);
            borderNode.Attributes["style"].Value = stringBuilder.ToString();
            return borderValue;
        }
        public static HtmlDocument AddAlignmentToRotatedTableContent(HtmlDocument htmlDoc, HtmlNode tableElement) {
            var rotatedNodes = tableElement.Descendants().Where(d => d.Attributes["style"]?.Value.Contains("rotate(-90deg)") == true).ToList();
            foreach (var rotatedNode in rotatedNodes) {
                AddAttributeToNode(rotatedNode, "class", StsRotatedTd);
                //creates container for better rotation in html (and css)
                var rotateContainer = htmlDoc.CreateElement("div");
                AddAttributeToNode(rotateContainer, "class", TableRotatedContainer);
                foreach (var childNode in rotatedNode.ChildNodes.ToList()) {
                    var child = childNode;
                    child.InnerHtml = child.InnerHtml.Replace("-", "&#x2011;");
                    childNode.Remove();
                    rotateContainer.AppendChild(child);
                }
                rotatedNode.AppendChild(rotateContainer);
            }
            return htmlDoc;
        }
        public static HtmlDocument ClassifyTables(HtmlDocument htmlDoc, bool isEuropean, int year) {
            var tableWraps = htmlDoc.DocumentNode.Descendants()
                .Where(d => (d.Attributes["class"]?.Value.Contains(StsTableWrap) == true 
                && d.Attributes["class"]?.Value.Contains("foot") == false)
                || (d.Attributes["class"]?.Value.Contains(StsArray) == true 
                && d.Attributes["class"]?.Value.Contains(StsArrayContent) == false));
            foreach (var tableWrap in tableWraps) {
                var landscape = tableWrap.Attributes["class"]?.Value.Contains("sts-landscape-orientation");
                //not predicting based on width if ISO and if orientation was not landscape in XML
                var noLandscape = tableWrap.Attributes["class"]?.Value.Contains("sts-portrait-orientation") == true;
                var table = tableWrap.Descendants("table").FirstOrDefault();
                foreach (var td in table.Descendants().Where(d => d.Name == "td")) {
                    if (td.ChildNodes.FirstOrDefault(c => c.Attributes["class"]?.Value == StsNonNormativeNote) != null)
                        AddAttributeToNode(td, "class", StsNonNormativeNoteTd);
                }
                if (table?.Attributes["frame"]?.Value == "box") AddAttributeToNode(table, "class", StsBoxedTable);
                var tableWidth = table?.Attributes["width"]?.Value.Replace(".", ",");
                if (tableWidth == null) continue;
                var width = double.Parse(tableWidth);
                if (isEuropean && year < 2011) AddAttributeToNode(table, "class", StsTableEnBorderless);
                if (width < 661 && landscape == false) AddAttributeToNode(table, "class", StsTableNormal);
                if (width < 661 && landscape == true) ClassifySideTurnedTable(width, table, tableWrap);
                if (width > 668 && !noLandscape) ClassifySideTurnedTable(width, table, tableWrap);
                MoveIdFromWrapToTable(tableWrap);
            }
            return htmlDoc;
        }
        public static HtmlNode MoveIdFromWrapToTable(HtmlNode tableWrap) {
            var table = tableWrap.Descendants().FirstOrDefault(d => d.Name == "table");
            if (!string.IsNullOrEmpty(tableWrap.Id) && table != null) {
                table.Id = tableWrap.Id;
                tableWrap.Attributes["Id"].Remove();
            }
            return tableWrap;
        }
        public static HtmlNode ClassifySideTurnedTable(double width, HtmlNode table, HtmlNode tableWrap) {
            width *= 1.1;
            if (table.ParentNode.Attributes["class"]?.Value.Contains(StsArray) == true) width = 661;
            var isInForeword = table.Ancestors().FirstOrDefault(a => a.Attributes["class"]?.Value.Contains(StsForeword) == true) != null;
            //no sideturned tables in foreword
            if (isInForeword) width = 660;
            //stretch to max
            if (width > 800) width = 920;
            var widthString = width.ToString(CultureInfo.InvariantCulture).Replace(",", ".");
            if (table.Attributes["width"] == null) AddAttributeToNode(table, "width", widthString);
            else table.Attributes["width"].Value = widthString;
            if (!isInForeword) AddAttributeToNode(table, "class", SideturnedTable);
            return tableWrap;
        }
        public static HtmlDocument ClassifyLandscape(HtmlDocument htmlDoc) {
            var rotatedTables = FindNodesByClassName(htmlDoc.DocumentNode, SideturnedTable);
            if (rotatedTables == null || !rotatedTables.Any()) {
                var mains = FindNodesByClassName(htmlDoc.DocumentNode, StsMain);
                foreach (var main in mains) {
                    foreach (var childNode in main.ChildNodes) AddAttributeToNode(childNode, "class", PortraitPage);
                }
            }
            foreach (var rotatedTable in rotatedTables) AddPortraitOrLandscapeClassToParents(rotatedTable);
            return htmlDoc;
        }
        public static HtmlNode AddPortraitOrLandscapeClassToParents(HtmlNode rotatedTable) {
            if (rotatedTable.ParentNode.Attributes["class"]?.Value.Contains(PortraitPage) == true)
                RemoveClassFromNode(rotatedTable.ParentNode, PortraitPage);
            AddAttributeToNode(rotatedTable.ParentNode, "class", LandscapePage);
            var tableParent = rotatedTable.ParentNode.ParentNode;
            while (tableParent.ParentNode.Name != "html") {
                foreach (var childNode in tableParent.ChildNodes) {
                    if (childNode.DescendantsAndSelf().Any(d =>
                        d.Attributes["class"]?.Value.Contains(SideturnedTable) == true)) continue;
                    if (childNode.Attributes["class"]?.Value.Contains(LandscapePage) == false
                        && childNode.Attributes["class"]?.Value.Contains(PortraitPage) == false) {
                        AddAttributeToNode(childNode, "class", PortraitPage);
                    }
                }
                tableParent = tableParent.ParentNode;
            }
            return rotatedTable;
        }
        public static HtmlDocument MoveHeadingsToTable(HtmlDocument htmlDoc) {
            //this is a workaround for prince to not break large tables from headings (annexes)
            var tableContainers = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains(StsTableWrap) == true 
                            || d.Attributes["class"]?.Value.Contains(StsArray) == true
                            || (d.Attributes["class"]?.Value.Contains(StsFig) == true)
                            || d.Attributes["class"]?.Value.Contains(SideturnedTable) == true
                            && d.Attributes["class"]?.Value.Contains(StsTableWrapFoot) == false).ToList();
            foreach (var tableContainer in tableContainers) {
                var mainNode = tableContainer;
                while (mainNode.ParentNode.Attributes["class"]?.Value.Contains(StsMain) == false) {
                    var previousNode = mainNode.PreviousSibling;
                    //if previous element contains tables (is large) then ignore
                    if (previousNode != null && (previousNode.Descendants()
                        .Where(d => d.Attributes["class"]?.Value?.Contains(StsTableWrap) == true).Any() 
                                                 || previousNode.InnerLength > 500)) break;
                    var counter = CountPreviousNodes(previousNode);
                    if (counter < 2) MoveNodesIntoTableContainer(tableContainer, mainNode);
                    if (counter > 1) break;
                    MoveCoParentNodesIntoTableContainer(tableContainer, mainNode);
                    mainNode = mainNode.ParentNode;
                    if (mainNode.ChildNodes.Contains(tableContainer)) break;
                }
            }
            return htmlDoc;
        }
        public static int CountPreviousNodes(HtmlNode previousNode) {
            var counter = 0;
            var containsHeadings = false;
            while (previousNode != null) {
                if (previousNode.Name != "#text") counter++;
                if (previousNode.Attributes["class"]?.Value.Contains(StsAppHeader) == true ||
                    previousNode.Attributes["class"]?.Value.Contains(StsSecTitle) == true) {
                    counter--;
                    containsHeadings = true;
                }
                if (previousNode.Attributes["class"]?.Value.Contains(StsTableWrap) == true
                    || previousNode.Attributes["class"]?.Value.Contains(StsArray) == true
                    || previousNode.Attributes["class"]?.Value.Contains(StsFig) == true) {
                    counter = 3;
                    break;
                }
                previousNode = previousNode.PreviousSibling;
            }
            if (!containsHeadings) counter = 3;
            return counter;
        }
        public static HtmlNode MoveNodesIntoTableContainer(HtmlNode tableContainer, HtmlNode mainNode) {
            var previousNode = mainNode.PreviousSibling;
            var moveList = new List<HtmlNode>();
            while (previousNode != null) {
                moveList.Add(previousNode);
                previousNode = previousNode.PreviousSibling;
            }
            //don't move regular headings into table, only annex headings
            if (moveList.Where(d => d.Name.Contains("h") && d.Name.Length < 3).Any() 
                && !moveList.Where(d => d.Ancestors().Where(a => a.Attributes["class"]?.Name.Contains(StsApp) == false).Any()).Any())
                return tableContainer;
            foreach (var moveNode in moveList) {
                moveNode.Remove();
                tableContainer.InsertBefore(moveNode, tableContainer.ChildNodes[0]);
            }
            return tableContainer;
        }
        public static HtmlNode MoveCoParentNodesIntoTableContainer(HtmlNode tableContainer, HtmlNode mainNode) {
            var coParents = mainNode.ParentNode?.ParentNode?.ChildNodes.Where(c => c.Name != "#text").ToList();
            for (var i = 1; i < coParents.Count; i++) {
                var parent = coParents[i];
                if (parent != mainNode.ParentNode) continue;
                var childCounter = coParents[i - 1].Descendants().Where(c => c.Name != "#text").ToList().Count;
                if (childCounter > 1) break;
                coParents[i - 1].Remove();
                tableContainer.InsertBefore(coParents[i - 1], tableContainer.ChildNodes[0]);
                break;
            }
            return tableContainer;
        }
    }
}
