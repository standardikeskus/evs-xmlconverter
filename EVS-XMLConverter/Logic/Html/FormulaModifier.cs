﻿using System.Linq;
using HtmlAgilityPack;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverter.Aids.HtmlHelper;

namespace EVS_XMLConverter.Logic.Html {
    public class FormulaModifier {
        public static HtmlDocument FixFormulas(HtmlDocument htmlDoc) {
            var formulas = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains(InlineFormula) == true
                            || d.Attributes["class"]?.Value.Contains(StsDispFormulaPanel) == true).ToList();
            foreach (var formula in formulas) {
                if (formula.Attributes["class"]?.Value.Contains(StsDispFormulaPanel) == true)
                    CenterFormulaPanelsContents(formula);
                FixFormulaSpacings(formula);
                FixFormulaMarkers(formula);
            }
            return htmlDoc;
        }
        public static HtmlNode CenterFormulaPanelsContents(HtmlNode formulaPanel) {
            if (!formulaPanel.ChildNodes.Select(d => d.Attributes["class"]?.Value).Contains(StsDispFormulaLabel))
                AddAttributeToNode(formulaPanel, "class", "labelless-formula");
            //converting text nodes to span elements in order to vertically center them
            foreach (var formulaNode in formulaPanel.ChildNodes.ToList()) {
                if (formulaNode.Name != "#text") continue;
                var formulaSpan = HtmlNode.CreateNode("<span/>");
                formulaSpan.InnerHtml = formulaNode.InnerHtml;
                AddAttributeToNode(formulaSpan, "class", StsFormulaText);
                formulaPanel.InsertAfter(formulaSpan, formulaNode);
                formulaNode.Remove();
            }
            return formulaPanel;
        }
        public static HtmlNode FixFormulaSpacings(HtmlNode formula) {
            //removing spaces from outside math formula
            var mainSpaces = formula.ChildNodes.Where(d => d.InnerHtml == " " && d.Name != "#text");
            foreach (var space in mainSpaces.ToList()) { space.Remove(); }

            //modifiying spaces inside math formulas
            var spaces = formula.Descendants().Where(d => d.InnerHtml == " " && d.Name != "#text");
            foreach (var space in spaces.ToList()) {
                var previousElement = space.PreviousSibling;
                if (previousElement?.Name == "#text") previousElement = previousElement?.PreviousSibling;
                //mo elements have spacing already
                if (previousElement?.Name == "mo") continue;
                //double spacing is not needed
                if (previousElement != null && previousElement.InnerHtml == "&#8201;") {
                    space.Remove();
                    continue;
                }
                space.InnerHtml = "&#8201;";
            }

            //using empty span for justify-content alignment
            if (formula.ChildNodes.Select(c => c.Attributes["class"]?.Value).Contains(StsDispFormulaLabel)) {
                var startingNode = HtmlNode.CreateNode("<span/>");
                startingNode.InnerHtml = "&#8201;";
                formula.ChildNodes.Prepend(startingNode);
            }
            return formula;
        }
        public static HtmlNode FixFormulaMarkers(HtmlNode formula) {
            //.sts-formula-text:last-child sometimes contains parts of previous block - they need to be separated
            var lastNode = formula.ChildNodes
                .LastOrDefault(c => c.Attributes["class"]?.Value.Contains(StsFormulaText) == true && c.InnerHtml.Contains("&emsp;"));
            if (lastNode != null) {
                var preLastNode = lastNode.CloneNode(false);
                var test = lastNode.InnerHtml.IndexOf("&emsp;");
                preLastNode.InnerHtml = lastNode.InnerHtml.Substring(0, lastNode.InnerHtml.IndexOf("&emsp;") + 6);
                if (preLastNode.InnerHtml == string.Empty) return formula;
                else lastNode.ParentNode?.InsertBefore(preLastNode, lastNode);
                lastNode.InnerHtml = lastNode.InnerHtml.Substring(lastNode.InnerHtml.IndexOf("&emsp;") + 6);
                if (lastNode.InnerHtml.Contains("(") && lastNode.InnerHtml.Contains(")")) AddAttributeToNode(lastNode, "class", "math-text-last");
            }
            return formula;
        }
        public static HtmlDocument FixMathElements(HtmlDocument htmlDoc) {
            var mathElements = htmlDoc.DocumentNode.SelectNodes("//math");
            if (mathElements == null) return htmlDoc;
            foreach (var math in mathElements.ToList()) {
                var mstyles = math?.Descendants("mstyle");
                foreach (var mstyle in mstyles) {
                    if (mstyle?.Attributes["stretchy"] == null) AddAttributeToNode(mstyle, "stretchy", "false");
                    if (mstyle?.Attributes["mathsize"] != null) mstyle.Attributes["mathsize"].Value = "100%";
                }
                var mis = math?.Descendants("mi");
            }
            return htmlDoc;
        }
    }
}
