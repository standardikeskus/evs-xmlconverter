﻿using System.Linq;
using System.Xml.Linq;
using HtmlAgilityPack;
using EVS_XMLConverter.Objects;
using System.Collections.Generic;
using static EVS_XMLConverter.Aids.XmlHelper;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Aids.LogWriter;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;

namespace EVS_XMLConverter.Logic.Html {
    public class TocGenerator {
        public static HtmlDocument AddEvsTableOfContents(HtmlDocument htmlDoc, List<XmlStandard> xmlStandardList) {
            var allHeadingNodes = FindAllHeadingNodes(htmlDoc);
            var headings = FilterHeadings(allHeadingNodes);
            var evsTocNode = CreateTocNode(htmlDoc, headings, xmlStandardList);
            if (htmlDoc == null || evsTocNode.ChildNodes?.Count() < 3) return htmlDoc;
            var startOfStandard = FindNodesByClassName(htmlDoc.DocumentNode, StsMain).FirstOrDefault();
            startOfStandard?.ParentNode.InsertBefore(evsTocNode, startOfStandard);
            return htmlDoc;
        }
        public static List <HtmlNode> FindAllHeadingNodes(HtmlDocument htmlDoc) {
            //find the main containers of titles
            var mainContainers = htmlDoc.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Attributes["class"]?.Value.Contains(StsMain) == true || d.Id == StsTitlepageEn);
            var allHeadingNodes = new List<HtmlNode>();
            if (mainContainers != null && mainContainers.Any()) {
                foreach (var mainContainer in mainContainers) {
                    if (mainContainer.Id == StsTitlepageEn) { allHeadingNodes.Add(mainContainer); }
                    else allHeadingNodes.AddRange(FindNodesById(htmlDoc.DocumentNode, TocIdentifier));
                }
            }
            ChangeDuplicateIds(allHeadingNodes);
            return allHeadingNodes;
        }
        public static List<HtmlNode> ChangeDuplicateIds(List<HtmlNode> nodes) {
            var idAddition = 0;
            foreach (var node in nodes) {
                var s = nodes.Where(n => !string.IsNullOrEmpty(n.Id) && n != node && n.Id == node.Id);
                if (nodes.Count(n => !string.IsNullOrEmpty(n.Id) && n.Id == node.Id && n != node) == 0 ) continue;
                node.Id += string.Concat("_", idAddition);
                idAddition++;
            }
            return nodes;
        }
        public static List<HtmlNode> FilterHeadings(List<HtmlNode> allHeadingNodes) {
            var headings = new List<HtmlNode>();
            foreach (var headingDiv in allHeadingNodes) {
                //find the EN titlepage heading
                if (headingDiv.Id.Contains(StsTitlepageEn)) headings.Add(headingDiv);
                else {
                    //filter only three layers of headings
                    var heading = headingDiv.Descendants().FirstOrDefault(c => c.Name == "h1" || c.Name == "h2" || c.Name == "h3");
                    if (heading == null) continue;
                    //only allow first level heading for annexes
                    //var mainStsApp = heading.Ancestors()?.LastOrDefault(a => a.Attributes["class"]?.Value.Contains(StsApp) == true);
                    //if (mainStsApp != null && mainStsApp.Descendants().FirstOrDefault(d => d.Name == "h1") != heading) continue;
                    headings.Add(heading);
                }
            }
            return headings.Distinct().ToList();
        }
        public static HtmlNode CreateTocNode(HtmlDocument htmlDoc, List<HtmlNode> headings, List<XmlStandard> xmlStandardList) {
            var tocNode = CreateTocNodeBase(htmlDoc);
            var queueNumber = 0;
            var tocDatas = new List<TocData>();
            for (var i = 0; i < headings.Count; i++) {
                TocData tocData;
                if (xmlStandardList[0].IsPureIso) { AddAttributeToNode(tocNode, "counter-reset", "1"); }
                var isoForewordCheck = xmlStandardList[0].IsPureIso && headings[i] != headings[0] && headings[i].InnerHtml.Contains("Foreword");
                if (isoForewordCheck) queueNumber++;
                if (headings[i] != null && headings[i].Id.Contains(StsTitlepageEn)) {
                    if (headings[i] == headings[0]) { tocDatas.Add(null); continue; }
                    tocData = TitlepageDivToTocData(headings[i]);
                }
                //tocdatas list also contains null values to keep in sync with headers list
                else tocData = HeadingToTocData(headings[i], xmlStandardList[queueNumber].XDoc, 
                    isoForewordCheck, tocDatas.Where(d => d != null).LastOrDefault());
                //tocDatas contains only first level headings (with child TocDatas)
                if (tocData.TocPriority == 1) tocDatas.Add(tocData);
                else tocDatas.Add(null);
            }
            tocDatas.RemoveAll(t => t == null);
            InsertTocDatasIntoTocNode(tocNode, tocDatas);
            return tocNode;
        }
        public static HtmlNode CreateTocNodeBase(HtmlDocument htmlDoc) {
            var tocNode = htmlDoc.CreateElement("div");
            tocNode.Id = EvsTableOfContents;
            AddAttributeToNode(tocNode, "class", StsStandard);
            var titleDiv = htmlDoc.CreateElement("div");
            titleDiv.InnerHtml = "Contents";
            titleDiv.Id = TocTitle;
            tocNode.AppendChild(titleDiv);
            return tocNode;
        }
        public static TocData TitlepageDivToTocData(HtmlNode titlepageDiv) {
            var tocData = new TocData { TocTitle = FindNodesById(titlepageDiv, EnReference).FirstOrDefault().InnerHtml };
            if (tocData.TocTitle != null) tocData.TocTitle = tocData.TocTitle.Replace("Ref. No. ", string.Empty);
            else LogWrite("WARNING: Sisukorras on pealkiri puudu.");
            tocData.TocReference = titlepageDiv.Id;
            tocData.TocClass = TocTitlepage;
            return tocData;
        }
        public static TocData HeadingToTocData(HtmlNode heading, XDocument xDoc, bool isoForewordCheck, TocData previousTocData = null) {
            var tocReference = heading?.Ancestors().FirstOrDefault(a => a.Id.Contains(TocIdentifier) == true)?.Id;
            var tocTitle = ReplaceLinkElements(heading).InnerHtml;
            if (isoForewordCheck) tocTitle = FindOriginalMeta(xDoc).Elements("std-ref").FirstOrDefault(e => e.Attribute("type")?.Value == "undated")?.Value;
            if (string.IsNullOrEmpty(tocTitle)) return null;
            if (tocReference == null) LogWrite("WARNING: A reference in table of contents may is missing a link");
            var isAnnexHeading = heading.Ancestors()?.LastOrDefault(a => a.Attributes["class"]?.Value.Contains(StsApp) == true) != null;
            var isMainAnnexHeading = heading.ParentNode.Attributes["class"]?.Value.Contains(StsApp) == true;
            var tocData = new TocData();
            switch (heading.Name) {
                case "h3" when isAnnexHeading:
                    tocData.TocPriority = 4; break;
                case "h3":
                    tocData.TocPriority = 3; break;
                case "h2" when isAnnexHeading:
                    tocData.TocPriority = 3; break;
                case "h2":
                    tocData.TocPriority = 2; break;
                case "h1" when isMainAnnexHeading:
                    tocData.TocPriority = 1; break;
                case "h1" when isAnnexHeading:
                    tocData.TocPriority = 2; break;
                default:
                    tocData.TocPriority = 1; break;
            }
            //annex heading
            if (heading.Ancestors()?.LastOrDefault(a => a.Attributes["class"]?.Value.Contains(StsApp) == true) != null) {
                tocData.TocTitle = GetAnnexHeadingTitle(heading);
                if (tocData.TocTitle == null || tocData.TocTitle == string.Empty) tocData.TocTitle = tocTitle;
                tocData.TocClass = "toc-annex";
            }
            //regular heading
            else {
                tocData.TocTitle = tocTitle;
                tocData.TocClass = "toc-section";
            }
            tocData.TocReference = tocReference;
            AddAsChildToPreviousTocData(tocData, previousTocData);
            return tocData;
        }
        public static HtmlNode ReplaceLinkElements(HtmlNode node) {
            if (node == null) return null;
            var htmlDoc = new HtmlDocument();
            var newNode = htmlDoc.CreateElement(node.Name);
            foreach (var childNodes in node.ChildNodes) {
                var newNodeChild = htmlDoc.CreateTextNode(childNodes.OuterHtml);
                newNode.AppendChild(newNodeChild);
                var link = childNodes
                    .DescendantsAndSelf().FirstOrDefault(d => d
                    .Attributes["class"]?.Value.Contains(StsXref) == true);
                if (link == null) continue;
                var newSpan = htmlDoc.CreateElement("span");
                AddAttributeToNode(newSpan, "class", "link-span");
                newSpan.InnerHtml = link.InnerHtml;
                newNodeChild.ParentNode.ReplaceChild(newSpan, newNodeChild);
            }
            return newNode;
        }
        public static string GetAnnexHeadingTitle(HtmlNode heading) {
            var annexFullHeading = string.Empty;
            var annexHeadings = heading.ParentNode.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains(StsAppHeader) == true).ToList();
            foreach (var annexHeading in annexHeadings) {
                if (annexHeading == annexHeadings.Last()) {
                    annexHeading.Name = "div";
                    AddAttributeToNode(annexHeading, "class", StsAppHeaderMain);
                }
                var annexHeadingClean = ReplaceLinkElements(annexHeading).InnerHtml;
                if (annexHeading.Name == "div") annexHeadingClean = string.Concat("<span>", annexHeadingClean, "</span>");
                annexFullHeading += string.Concat(" ", annexHeadingClean);
            }
            return annexFullHeading;
        }
        public static TocData AddAsChildToPreviousTocData(TocData tocData, TocData previousTocData) {
            var priority = tocData.TocPriority;
            if (priority > 3) return tocData;
            var lastTocData = previousTocData;
            while (previousTocData?.TocPriority < priority || priority > 1) {
                if (priority - previousTocData.TocPriority == 1) {
                    if (lastTocData != null) lastTocData.ChildTocDatas.Add(tocData);
                    break;
                }
                if (priority - previousTocData.TocPriority > 1) lastTocData = lastTocData.ChildTocDatas.LastOrDefault();
                priority--;
            }
            return tocData;
        }
        public static HtmlNode InsertTocDatasIntoTocNode(HtmlNode tocNode, List<TocData> tocDatas) {
            foreach (var tocData in tocDatas) {
                if (tocData == null) continue;
                var tocElement = TocDataToHtmlElement(tocData);
                tocData.TocElement = tocElement;
                if (tocElement == null) continue;
                tocNode.AppendChild(tocElement);
                var tocChildren = tocData.ChildTocDatas;
                while (tocChildren != null && tocChildren.Any()) {
                    var tocChild = tocChildren.FirstOrDefault();
                    while (tocChild != null) {
                        var tocChildElement = TocDataToHtmlElement(tocChild);
                        tocElement.AppendChild(tocChildElement);
                        if (tocChild.ChildTocDatas.FirstOrDefault() == null) {
                            tocChildren.Remove(tocChild);
                            tocChild = tocChildren.FirstOrDefault();
                            if (tocChild == null) tocElement = tocChildElement;
                        }
                        else {
                            tocChildren = tocChild.ChildTocDatas;
                            tocChild = tocChild.ChildTocDatas.FirstOrDefault();
                            tocElement = tocElement.ChildNodes.LastOrDefault();
                        }
                    }
                    tocData.ChildTocDatas.Remove(tocData.ChildTocDatas.FirstOrDefault());
                    tocElement = tocData.TocElement;
                    tocChildren = tocData.ChildTocDatas;
                }
            }
            return tocNode;
        }
        public static HtmlNode TocDataToHtmlElement(TocData tocData) {
            if (tocData == null) return null;
            var htmlDoc = new HtmlDocument();
            var tocElement = htmlDoc.CreateElement("div");
            var tocLinkElement = htmlDoc.CreateElement("a");
            AddAttributeToNode(tocLinkElement, "href", string.Concat("#", tocData.TocReference ?? string.Empty));
            var idElement = htmlDoc.CreateElement("span");
            AddAttributeToNode(idElement, "class", "toc-left");
            var titleElement = htmlDoc.CreateElement("span");
            AddAttributeToNode(titleElement, "class", "toc-right");
            AddAttributeToNode(titleElement, "href", string.Concat("#", tocData.TocReference ?? string.Empty));

            var titleAsNode = htmlDoc.CreateElement(string.Empty);
            titleAsNode.InnerHtml = tocData.TocTitle;
            var spacingSpan = titleAsNode.ChildNodes
                .FirstOrDefault(c => c.Attributes["class"]?.Value.Contains("toc-spacing") == true);
            if (spacingSpan != null) {
                var identifier = spacingSpan.PreviousSibling.InnerHtml;
                idElement.InnerHtml = identifier;
                tocLinkElement.AppendChild(idElement);

                var titleNode = ComposeTitleNode(spacingSpan, titleElement);
                tocLinkElement.AppendChild(titleNode);
            }
            else {
                titleElement.InnerHtml = tocData.TocTitle;
                tocLinkElement.AppendChild(titleElement);
            }

            AddAttributeToNode(tocElement, "class", tocData.TocClass);
            tocElement.AppendChild(tocLinkElement);
            return tocElement;
        }
        public static HtmlNode ComposeTitleNode(HtmlNode spacingSpan, HtmlNode titleElement) {
            var title = string.Empty;
            var sibling = spacingSpan.NextSibling;
            while (sibling != null) {
                title += sibling.InnerHtml;
                sibling = sibling.NextSibling;
                if (sibling?.Attributes["class"]?.Value.Contains("link-span") == true &&
                    sibling.Descendants().Any(d => d.Attributes["class"]?.Value.Contains(StsFootnote) == true))
                    break;
            }
            titleElement.InnerHtml = title;
            return titleElement;
        }
    }
}
