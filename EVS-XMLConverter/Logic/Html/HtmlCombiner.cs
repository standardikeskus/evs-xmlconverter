﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using EVS_XMLConverter.Logic.Classificators;
using EVS_XMLConverter.Objects;
using EVS_XMLConverter.Aids;
using HtmlAgilityPack;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverter.Logic.Html.TemplateFillers.TemplateFiller;
using static EVS_XMLConverter.Logic.Html.TemplateFillers.IecTemplateFiller;
using static EVS_XMLConverter.Logic.Html.TocGenerator;
using static EVS_XMLConverter.Logic.Pdf.PreviewGenerator;
using static System.Configuration.ConfigurationManager;

namespace EVS_XMLConverter.Logic.Html {
    public class HtmlCombiner {
        public static Tuple<FileInfo, FileInfo> Combine(List<XmlStandard> xmlStandardList, EvsData evsData) {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["EvsTitlepage"]), Encoding.UTF8);
            FillEvsTitlepage(htmlDoc, xmlStandardList[0]);
            
            var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body");
            AddEvsForeword(htmlBody, xmlStandardList);
            foreach (var xmlStandard in xmlStandardList) {
                if (xmlStandard.IsEuropean) AddEnTitlepage(htmlBody, xmlStandard);
                if (xmlStandard.Originator == Originator.Iec) {
                    AddIecTitlepage(htmlBody, xmlStandard);
                    AddIecCopyright(htmlBody, xmlStandard);
                    AddIecTitlepage(htmlBody, xmlStandard, true);
                }
                AppendContentToBody(xmlStandard.HtmlDoc, htmlBody);
            }
            AddEvsBackCover(htmlBody);
            AddEvsTableOfContents(htmlDoc, xmlStandardList);

            var htmlPackage = SaveFullHtmlAndPreview(htmlDoc, xmlStandardList[0].XmlDirectory, xmlStandardList[0]?.EvsData.FileName);
            if (htmlPackage.Item1 == null) throw new ConversionException("Html faili genereerimine ebaõnnestus");
            if (htmlPackage.Item2 == null) throw new ConversionException("Html eelvaate genereerimine ebaõnnestus");
            return htmlPackage;
        }
        public static HtmlNode FillEvsTitlepage(HtmlDocument htmlDoc, XmlStandard xmlStandard) {
            if (htmlDoc.DocumentNode.SelectSingleNode("//body/div") == null) throw new ConversionException("Template on vigane");
            AddCssReference(htmlDoc);
            ReplaceTitles(htmlDoc, xmlStandard.EvsData, ElementNames.Estonian, false);
            ReplaceTitles(htmlDoc, xmlStandard.EvsData, ElementNames.English, false);
            ReplaceReferences(htmlDoc, xmlStandard, true);
            return htmlDoc.DocumentNode.SelectSingleNode("//body//div");
        }
        public static HtmlDocument AddCssReference(HtmlDocument htmlDoc) {
            var css = htmlDoc.CreateElement("link");
            css.SetAttributeValue("rel", "stylesheet");
            css.SetAttributeValue("type", "text/css");
            var path = Path.GetFullPath(string.Concat(AppSettings["Templates"], AppSettings["Paper"]));
            css.SetAttributeValue("href", path);
            htmlDoc.DocumentNode.SelectSingleNode("//head").AppendChild(css);
            return htmlDoc;
        }
        public static HtmlNode AddEvsForeword(HtmlNode htmlBaseBody, List<XmlStandard> xmlStandardList) {
            var htmlDoc = new HtmlDocument();
            var templateName = xmlStandardList[0].IsEuropean
                ? AppSettings["EvsForeword"]
                : AppSettings["EvsIsoForeword"];
            htmlDoc.Load(string.Concat(AppSettings["Templates"] + templateName), Encoding.UTF8);
            FillEvsForeword(htmlDoc, xmlStandardList);
            var htmlDiv = htmlDoc.GetElementbyId(StsForewordEvs);
            htmlBaseBody.AppendChild(htmlDiv);
            return htmlBaseBody;
        }
        public static HtmlNode FillEvsForeword(HtmlDocument htmlDoc, List<XmlStandard> xmlStandardList) {
            if (htmlDoc.DocumentNode.SelectSingleNode("//body/div") == null) throw new ConversionException("Template on vigane");
            var xmlStandard = xmlStandardList[0];
            ReplaceReferences(htmlDoc, xmlStandard, true);
            ReplaceReferences(htmlDoc, xmlStandard, false);
            ReplaceDates(htmlDoc, xmlStandard.ReleaseDate);
            ReplaceIcsGroups(htmlDoc, xmlStandard.XDoc, xmlStandard.EvsData);
            if (xmlStandard.EvsData.EtTitle == null) xmlStandard.EvsData.EtTitle = string.Empty;
            ReplaceTitles(htmlDoc, xmlStandard.EvsData, ElementNames.English, true);
            ReplaceTitles(htmlDoc, xmlStandard.EvsData, ElementNames.Estonian, true);
            AddForewordAmendments(htmlDoc, xmlStandardList);
            if (!xmlStandardList[0].IsEuropean) {
                ReplaceForewordScope(htmlDoc, xmlStandard.EvsData.EvsIsoData);
                ReplaceCommittees(htmlDoc, xmlStandard.EvsData.EvsIsoData);
                ReplaceBulletinDate(htmlDoc, xmlStandard.EvsData.EvsIsoData);
            }
            var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body//div");
            return htmlBody;
        }
        public static HtmlNode AddEnTitlepage(HtmlNode htmlBaseBody, XmlStandard xmlStandard) {
            try {
                var htmlDoc = LoadEnTitlepageTemplate(xmlStandard);
                FillEnTitlepage(htmlDoc, xmlStandard);
                var htmlDiv = htmlDoc.GetElementbyId(StsTitlepageEn);
                htmlBaseBody.AppendChild(htmlDiv);
                return htmlBaseBody;
            }
            catch(Exception ex) { 
                throw new ConversionException(string.Concat("Probleem Euroopa tiitellehe lisamisega: ", ex.Message));
            }
        }
        public static HtmlDocument LoadEnTitlepageTemplate(XmlStandard xmlStandard) {
            var htmlDoc = new HtmlDocument();
            if (xmlStandard.SpecificOriginator == Originator.Cen ||
                    xmlStandard.SpecificOriginator == Originator.CenIso)
                htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["CenTitlepage"]), Encoding.UTF8);
            else if (xmlStandard.SpecificOriginator == Originator.Clc ||
                     xmlStandard.SpecificOriginator == Originator.ClcIec)
                htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["CenelecTitlepage"]), Encoding.UTF8);
            else if (xmlStandard.SpecificOriginator == Originator.CenClc &&
                     xmlStandard.StaDISOrganisation == Originator.Cen || xmlStandard.StaDISOrganisation == Originator.CenIso)
                htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["CenCenelecTitlepage"]), Encoding.UTF8);
            else if (xmlStandard.SpecificOriginator == Originator.CenClc &&
                     xmlStandard.StaDISOrganisation == Originator.Clc || xmlStandard.StaDISOrganisation == Originator.ClcIec)
                htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["CenelecCenTitlepage"]), Encoding.UTF8);
            else throw new ConversionException("Probleem Euroopa tiitellehe template valimisega");
            return htmlDoc;
        }
        public static HtmlNode AddIecTitlepage(HtmlNode htmlBaseBody, XmlStandard xmlStandard, bool secondTitlepage = false) {
            try {
                var htmlDoc = new HtmlDocument();
                var iecTitlepage = secondTitlepage ? AppSettings["IecSecondTitlepage"] : AppSettings["IecTitlepage"];
                htmlDoc.Load(string.Concat(AppSettings["Templates"], iecTitlepage), Encoding.UTF8);
                FillIecTitlepage(htmlDoc, xmlStandard);
                var htmlDiv = htmlDoc.GetElementbyId(StsTitlepageIec);
                htmlBaseBody.AppendChild(htmlDiv);
                return htmlBaseBody;
            }
            catch (Exception ex) {
                throw new ConversionException(string.Concat("Probleem IEC tiitellehe lisamisega: ", ex.Message));
            }
        }
        public static HtmlNode AddIecCopyright(HtmlNode htmlBaseBody, XmlStandard xmlStandard) {
            try {
                var htmlDoc = new HtmlDocument();
                htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["IecCopyright"]), Encoding.UTF8);
                ReplaceIecCopyrightYear(htmlDoc, xmlStandard.XDoc);
                var htmlDiv = htmlDoc.GetElementbyId(StsCopyrightIec);
                htmlBaseBody.AppendChild(htmlDiv);
                return htmlBaseBody;
            }
            catch (Exception ex) {
                throw new ConversionException(string.Concat("Probleem IEC tiitellehe lisamisega: ", ex.Message));
            }
        }
        public static HtmlNode AppendContentToBody(HtmlDocument htmlDocMain, HtmlNode htmlBody) {
            var addition = htmlDocMain.DocumentNode.SelectSingleNode("//body").ChildNodes;
            foreach (var add in addition) {
                var emptyNodes = add.ChildNodes.Where(d => d.InnerLength < 2).ToList();
                //ignore if body has only empty nodes
                foreach (var emptyNode in emptyNodes)
                    if (emptyNode.Attributes["class"]?.Value.Contains(StsP) == true) emptyNode.Remove();
                if (add.InnerText.Equals("\r\n      ​\r\n    ")) continue;
                htmlBody.AppendChild(add);
            }
            return htmlBody;
        }
        public static HtmlNode FillEnTitlepage(HtmlDocument htmlDoc, XmlStandard xmlStandard) {
            if (htmlDoc.DocumentNode.SelectSingleNode("//body/div") == null)
                throw new ConversionException("Template on vigane");
            ReplaceReferenceWithoutYear(htmlDoc, xmlStandard.XDoc);
            ReplaceDateYearAndMonth(htmlDoc, xmlStandard.XDoc, xmlStandard.ReleaseDate);
            ReplaceIcsGroups(htmlDoc, xmlStandard.XDoc);
            ReplaceSupersedes(htmlDoc, xmlStandard.XDoc);
            ReplaceEuropeanTitlepageTitles(htmlDoc, xmlStandard.XDoc);
            ReplaceSections(htmlDoc, xmlStandard.XDoc);
            ReplaceCopyright(htmlDoc, xmlStandard);
            ReplaceReferenceNumber(htmlDoc, xmlStandard.XDoc);
            var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body//div");
            return htmlBody;
        }
        public static HtmlNode FillIecTitlepage(HtmlDocument htmlDoc, XmlStandard xmlStandard) {
            if (htmlDoc.DocumentNode.SelectSingleNode("//body/div") == null)
                throw new ConversionException("Template on vigane");
            ReplaceIecReference(htmlDoc, xmlStandard.XDoc, !xmlStandard.IsAmendment);
            ReplaceIecSideReference(htmlDoc, xmlStandard.XDoc);
            ReplaceIecEdition(htmlDoc, xmlStandard.XDoc);
            ReplaceIecDate(htmlDoc, xmlStandard.XDoc);
            ReplaceIecTitles(htmlDoc, xmlStandard.XDoc, xmlStandard.IsAmendment);
            ReplaceIecIcsGroups(htmlDoc, xmlStandard.XDoc);
            ReplaceIsbn(htmlDoc, xmlStandard.XDoc);
            var htmlBody = htmlDoc.DocumentNode.SelectSingleNode("//body//div");
            return htmlBody;
        }
        public static HtmlNode AddEvsBackCover(HtmlNode htmlBaseBody) {
            var htmlDoc = new HtmlDocument();
            htmlDoc.Load(string.Concat(AppSettings["Templates"], AppSettings["EvsBackCover"]), Encoding.UTF8);
            var htmlDiv = htmlDoc.GetElementbyId(StsBackCoverEvs);
            htmlBaseBody.AppendChild(htmlDiv);
            return htmlBaseBody;
        }
        public static Tuple<FileInfo, FileInfo> SaveFullHtmlAndPreview(HtmlDocument htmlDoc, DirectoryInfo firstFileDirectory, string fileName) {
            if (firstFileDirectory == null) throw new ConversionException("HTML faili kausta ei leitud");
            var directory = firstFileDirectory.Parent;
            if (directory == null) throw new ConversionException("HTML fail ei ole õiges kaustas");
            var fullName = MicroConverter.ConstructName(directory.FullName, fileName, ".html");
            var finalName = HtmlHelper.SaveHtml(htmlDoc, fullName);
            var finalHtml = new FileInfo(finalName);

            GeneratePreview(htmlDoc, directory, fileName);
            var previewHtml = new FileInfo(MicroConverter.ConstructName(directory.FullName, fileName, ".html", AppSettings["HtmlPreview"]));

            var htmlPackage = Tuple.Create(finalHtml, previewHtml);
            return htmlPackage;
        }
    }
}
