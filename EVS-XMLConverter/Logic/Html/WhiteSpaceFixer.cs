﻿using System.Linq;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverter.Aids.HtmlHelper;
using System.IO;

namespace EVS_XMLConverter.Logic.Html {
    public class WhiteSpaceFixer {
        public static HtmlDocument MinimizeHtml(HtmlDocument htmlDoc) {
            //text nodes containing > 1 spacings
            var emptyLines = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.Name == "#text" && d.ParentNode?.Name != "pre" && !d.InnerHtml.Contains("\n")
                && (d.InnerHtml.Contains("  ") || d.InnerHtml.Contains("  ") || d.InnerHtml.Contains("\u00A0\u00A0"))).ToList();
            foreach (var empty in emptyLines) {
                var emptyHtml = RemoveSpacingsFromNode(empty);
                if (emptyHtml == string.Empty) empty.Remove();
            }
            return htmlDoc;
        }
        public static string RemoveSpacingsFromNode(HtmlNode node) {
            var innerHtml = node.InnerHtml.Replace(" ", string.Empty);
            innerHtml = innerHtml.Replace(" ", string.Empty);
            innerHtml = innerHtml.Replace("\u00A0", string.Empty);
            return innerHtml;
        }
        public static HtmlDocument RemoveMarginFromEmptyFirstTableColumn(HtmlDocument htmlDoc) {
            var arrayNodes = FindNodesByClassName(htmlDoc.DocumentNode, StsArray);
            foreach (var array in arrayNodes) {
                var rows = array.Descendants().FirstOrDefault(d => d.Name == "tbody" == true).ChildNodes.Where(c => c.Name == "tr");
                var shouldRemoveMargin = false;
                foreach (var row in rows)
                {
                    var firstCell = row.ChildNodes.FirstOrDefault(c => c.Name == "td");
                    var isEmpty = false;
                    if (firstCell.ChildNodes.Count == 1) isEmpty = RemoveSpacingsFromNode(firstCell.FirstChild) == string.Empty;
                    else isEmpty = RemoveSpacingsFromNode(firstCell) == string.Empty;
                    if (isEmpty) {
                        firstCell.InnerHtml = string.Empty;
                        shouldRemoveMargin = true;
                        continue;
                    }
                    //only if entire first column is empty, we remove our margin
                    shouldRemoveMargin = false;
                    break;
                }
                if (shouldRemoveMargin) AddAttributeToNode(array, "class", "marginless");
            }
            return htmlDoc;
        }
        
        public static HtmlDocument FixWhitespaces(HtmlDocument htmlDoc) {
            var tabSpaces = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.InnerHtml.Contains("\t") == true && d.Name == "#text" && d.ParentNode?.Name != "pre").ToList();
            foreach (var tabSpace in tabSpaces) {
                if (tabSpace.ParentNode != null) tabSpace.ParentNode.InnerHtml = tabSpace.ParentNode.InnerHtml.Replace("\t", "&emsp;");
            }
            return htmlDoc;
        }
        public static HtmlDocument RemoveEmptyDivs(HtmlDocument htmlDoc, bool isAmendment) {
            var allNodes = htmlDoc.DocumentNode.Descendants().Where(d => d.Name == "#text" && d.ParentNode?.Name != "pre");
            foreach (var node in allNodes) node.InnerHtml = Regex.Replace(node.InnerHtml, @"\s+", " ");

            if (!isAmendment) {
                var emptyDivs = htmlDoc.DocumentNode.Descendants("div")
                    .Where(d => d.Attributes["class"]?.Value == StsP && (d.InnerLength < 1 || d.InnerHtml.Contains("â€‹"))
                                || (d.NextSibling?.InnerHtml == " " && d.PreviousSibling?.InnerHtml == " " && d.InnerHtml.Length < 2)).ToList();
                foreach (var div in emptyDivs) div.Remove();
            }
            return htmlDoc;
        }
        public static HtmlDocument RemoveMarginFromBsFont(HtmlDocument htmlDoc) {
            var bsFonts = htmlDoc.DocumentNode.Descendants("span")
                .Where(d => d.Attributes["class"]?.Value.Contains(BsFontChar) == true).ToList();
            var startNodes = bsFonts.Where(d => d.Attributes["class"]?.Value.Contains("start") == true);
            foreach (var start in startNodes) {
                var iteration = start;
                if (start.ParentNode.ParentNode.Attributes["class"]?.Value.Contains(StsP) == true)
                    AddAttributeToNode(start.ParentNode.ParentNode, "class", BsFontChar);
                while (iteration != null) {
                    var parent = iteration.ParentNode;
                    if (parent?.Attributes["class"]?.Value.Contains(StsStandard) == true) break;
                    if (parent?.PreviousSibling?.Name == "#text" &&
                        parent.PreviousSibling.InnerHtml.Contains(@"\n  ") == false) break;
                    var nameCheck = parent?.Name == "div";
                    var styleCheck = parent?.Attributes["style"] == null
                        || parent.Attributes["style"]?.Value.Contains("margin-bottom: 0") == false;
                    if (nameCheck && styleCheck) AddAttributeToNode(parent, "style", "margin-bottom: 0;");
                    iteration = parent;
                }
            }
            var endNodes = bsFonts.Where(d => d.Attributes["class"]?.Value.Contains("end") == true);
            foreach (var end in endNodes) {
                if (end.ParentNode.ParentNode.Attributes["class"]?.Value.Contains(StsP) == true)
                    AddAttributeToNode(end.ParentNode.ParentNode, "class", BsFontChar);
                var parent = end.ParentNode;
                if (parent?.ParentNode.Attributes["class"]?.Value.Contains(StsP) == true
                    && parent.ParentNode.InnerLength < 45) parent = end.ParentNode.ParentNode;
                if (parent?.PreviousSibling != null
                    && parent.PreviousSibling.InnerHtml.Contains(@"\n  ")
                    && parent.PreviousSibling?.PreviousSibling?.Name != "i")
                    AddAttributeToNode(end, "class", BsFontRight);
            }
            return htmlDoc;
        }
        public static HtmlDocument FixCaptionSpacing(HtmlDocument htmlDoc) {
            var captionLabels = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains(StsCaptionLabel) == true
                            && d.NextSibling?.InnerHtml.StartsWith(" ") == false);
            foreach (var captionLabel in captionLabels.ToList()) captionLabel.InnerHtml += " ";
            return htmlDoc;
        }
        public static HtmlDocument FixTableWhiteSpaces(HtmlDocument htmlDoc) {
            var tableElements = htmlDoc.DocumentNode.Descendants().Where(d => d.Name == "td" || d.Name == "th");
            foreach (var tableElement in tableElements.ToList()) {
                if (tableElement.Descendants().Where(d => d.Name == "img").Any()) AddAttributeToNode(tableElement, "class", ImgTd);
                if (tableElement.InnerHtml.Contains("p-330"))
                    ;
                foreach (var desc in tableElement.Descendants().ToList()) RemoveWhiteSpaces(desc);
            }
            return htmlDoc;
        }
        public static HtmlNode RemoveWhiteSpaces(HtmlNode htmlNode) {
            if (htmlNode.InnerHtml == "\r" || htmlNode.InnerHtml == "\r " || htmlNode.InnerHtml == "\r\n") htmlNode.Remove();
            if (htmlNode.PreviousSibling == null || !htmlNode.PreviousSibling.InnerHtml.Contains(@"\n"))
                return htmlNode;
            var newValue = htmlNode.PreviousSibling.InnerHtml.Replace(@"\n                        ", string.Empty);
            newValue = newValue.Replace(@"\n", string.Empty);
            newValue = newValue.Replace("  ", string.Empty);
            htmlNode.PreviousSibling.InnerHtml = newValue;
            return htmlNode;
        }
        public static FileInfo FixIecTitleDiv(FileInfo html) {
            string text = File.ReadAllText(html.FullName);
            text = text.Replace("<title />", string.Empty);
            File.WriteAllText(html.FullName, text);
            return html;
        }
        public static HtmlDocument FixCodeBlocks(HtmlDocument htmlDoc) {
            var preElements = htmlDoc.DocumentNode.Descendants().Where(d => d.Name == "pre");
            foreach (var pre in preElements.ToList()) {
                foreach (var preChild in pre.ChildNodes.ToList()) {
                    var text = preChild.InnerText;
                    if ((text.EndsWith("}") || text.EndsWith(")") || text.EndsWith("*/")) && !text.Contains("{")) {
                        var spacing = htmlDoc.CreateElement("br");
                        preChild.ParentNode.InsertAfter(spacing, preChild);
                    }
                    if (text.StartsWith("//") || text.StartsWith("/*") || text.StartsWith("--")) {
                        var spacing = htmlDoc.CreateElement("br");
                        preChild.ParentNode.InsertAfter(spacing, FindPreviousSibling(preChild));
                    }
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument FixEllipsisFont(HtmlDocument htmlDoc) {
            var cells = htmlDoc.DocumentNode.Descendants().Where(d => d.Name == "td" || d.Name == "th");
            foreach (var cell in cells.ToList()) {
                if (cell.InnerHtml.Contains("… …")) cell.InnerHtml = cell.InnerHtml.Replace("… …", "…&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;…");
            }
            return htmlDoc;
        }
    }
}
