﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using EVS_XMLConverter.Aids;
using EVS_XMLConverter.Objects;
using EVS_XMLConverter.Logic.Classificators;
using static EVS_XMLConverter.Aids.XmlHelper;
using static EVS_XMLConverter.Aids.MicroConverter;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using static EVS_XMLConverter.Logic.Html.ImageModifier;
using static EVS_XMLConverter.Logic.Html.TableModifier;
using static EVS_XMLConverter.Logic.Html.FootnoteModifier;
using static EVS_XMLConverter.Logic.Html.FormulaModifier;
using static EVS_XMLConverter.Logic.Html.IsoHeaderFiller;
using static EVS_XMLConverter.Logic.Html.ClassAdder;
using static EVS_XMLConverter.Logic.Html.WhiteSpaceFixer;
using static EVS_XMLConverter.Logic.Html.AmendmentModifier;
using static System.Configuration.ConfigurationManager;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace EVS_XMLConverter.Logic.Html {
    public class XslConverter {
        public static List<XmlStandard> XmlsToHtmls(List<XmlStandard> xmlStandardList) {
            foreach (var xmlStandard in xmlStandardList) {
                var isEuropean = xmlStandard.IsEuropean;
                var year = xmlStandard.ReleaseDate.Year;
                if (xmlStandard.XmlDirectory.GetDirectories().Any())
                    xmlStandard.ImageDirectory = new DirectoryInfo(Path.Combine(xmlStandard.XmlDirectory.FullName, 
                        xmlStandard.XmlDirectory.GetDirectories()[0].Name));
                var html = new FileInfo(Path.Combine(xmlStandard.XmlDirectory.FullName, "generated.html"));
                TransformXmlToHtml(xmlStandard, html);
                FixIecTitleDiv(html);
                var htmlDoc = new HtmlDocument();
                htmlDoc.Load(html.FullName, Encoding.UTF8);
                File.Delete(html.FullName);

                if (!xmlStandard.IsEuropean) AddAndFillIsoHeader(htmlDoc, xmlStandard);
                AddCopyrightNotice(htmlDoc, xmlStandard);
                if (xmlStandard.ImageDirectory != null) {
                    AddImageLocationsToHtml(htmlDoc, xmlStandard);
                    //ReplaceMathElements(htmlDoc, xmlStandard);
                    ReplaceTableElements(htmlDoc, xmlStandard);
                }
                MinimizeHtml(htmlDoc);
                AddOrganisationToHtml(htmlDoc, xmlStandard.Originator);
                RemoveMarginFromEmptyFirstTableColumn(htmlDoc);
                ClassifyForewords(htmlDoc);
                ResizeTables(htmlDoc, isEuropean, xmlStandard.XmlDirectory, xmlStandard.SpecificOriginator);
                FixMathElements(htmlDoc); /*fix math before mocking with mathjax*/
                htmlDoc = MockHtml(htmlDoc, xmlStandard.XmlDirectory);
                ClassifyTables(htmlDoc, isEuropean, year);
                ClassifyEndorsement(htmlDoc);
                ClassifyIntros(htmlDoc);
                ClassifyBibliograpies(htmlDoc);
                ClassifySupLinks(htmlDoc);
                RemoveLinkFromExternalLinks(htmlDoc);
                FixTableWhiteSpaces(htmlDoc);
                FixWhitespaces(htmlDoc);
                RemoveEmptyDivs(htmlDoc, xmlStandard.IsAmendment);
                RemoveMarginFromBsFont(htmlDoc);
                FixFootnotes(htmlDoc);
                RemoveRepeatedLabels(htmlDoc);
                MarkStartOfStandard(htmlDoc);
                MoveHeadingsToTable(htmlDoc);
                ClassifyLandscape(htmlDoc);
                ChangeAmendmentSectionTitleClasses(htmlDoc, xmlStandard.IsAmendment);
                MoveCaptionToImageBottom(htmlDoc);
                FixCaptionSpacing(htmlDoc);
                MarkBoldStatements(htmlDoc);
                FixFormulas(htmlDoc);
                GroupAmendmentSections(htmlDoc, xmlStandard.IsAmendment);
                MoveQuotesToNextElement(htmlDoc);
                RemoveSpacingBeforeSubAndSup(htmlDoc);
                BringLabelToFrontOfListElement(htmlDoc);
                RemoveEmptyNodeFromEnd(htmlDoc);
                FixEntailedTermsLinking(htmlDoc);
                FixIecTbxSection(htmlDoc);
                BringInstructionsInFrontOfList(htmlDoc);
                FixEllipsisFont(htmlDoc);

                xmlStandard.HtmlDoc = htmlDoc;
            }
            return xmlStandardList;
        }
        public static HtmlDocument AddOrganisationToHtml(HtmlDocument htmlDoc, Originator originator) {
            var mainNode = htmlDoc.DocumentNode.Descendants("div")
            .FirstOrDefault(d => d.Attributes["class"]?.Value.Contains(StsMain) == true);
            AddAttributeToNode(mainNode, "class", string.Concat("sts-", originator._originatorName.ToLower()));
            return htmlDoc;
        }
        public static HtmlDocument MockHtml(HtmlDocument htmlDoc, DirectoryInfo xmlDirectory) {
            var injectJs = htmlDoc.CreateElement("script");
            injectJs.SetAttributeValue("type", "text/javascript");
            injectJs.SetAttributeValue("src", Path.GetFullPath(string.Concat(AppSettings["Templates"], AppSettings["InjectJs"])));
            htmlDoc.DocumentNode.SelectSingleNode("//head").AppendChild(injectJs);
            var conf = htmlDoc.CreateElement("script");
            htmlDoc.DocumentNode.SelectSingleNode("//head").AppendChild(conf);
            var mmlToSvg = htmlDoc.CreateElement("script");
            mmlToSvg.SetAttributeValue("type", "text/javascript");
            mmlToSvg.SetAttributeValue("src", AppSettings["Mml2Svg"]);
            htmlDoc.DocumentNode.SelectSingleNode("//head").AppendChild(mmlToSvg);
            var temporaryHtmlLocation = Path.Combine(xmlDirectory.FullName, "tables-resized.html");
            SaveHtml(htmlDoc, temporaryHtmlLocation);
            var options = new ChromeOptions();
            options.AddArgument("headless");
            var driverService = ChromeDriverService.CreateDefaultService(Path.GetFullPath(AppSettings["DriverService"]), "chromedriver.exe");
            var driver = new ChromeDriver(driverService);
            var js = driver as IJavaScriptExecutor;
            driver.Navigate().GoToUrl(temporaryHtmlLocation);
            Thread.Sleep(2000);
            var pageSource = driver.PageSource;
            driver.Dispose();

            var htmlDocNew = new HtmlDocument();
            htmlDocNew.LoadHtml(pageSource);
            var assistiveMath = htmlDocNew.DocumentNode.Descendants().Where(d => d.Name == "mjx-assistive-mml").ToList();
            foreach (var math in assistiveMath) math.Remove();
            return htmlDocNew;
        }

        public static HtmlDocument AddCopyrightNotice(HtmlDocument htmlDoc, XmlStandard xmlStandard = null) {
            var copyright = htmlDoc.CreateElement("span");
            copyright.Id = CopyrightNotice;
            copyright.InnerHtml = GetCopyrightNoticeForIso(xmlStandard);
            var firstDiv = htmlDoc.DocumentNode.SelectNodes("//div")?
                .Where(d => d.Attributes["class"]?.Value.Contains(StsStandard) == true).FirstOrDefault();
            if (firstDiv == null) return htmlDoc;
            var firstNode = ((firstDiv?.ChildNodes 
                                ?? throw new ConversionException("Probleem 'Copyright' lisamisega HTML struktuuri"))
                    .FirstOrDefault(c => c.Name != "#text")?.ChildNodes 
                                ?? throw new ConversionException("Probleem 'Copyright' lisamisega HTML struktuuri"))
                .FirstOrDefault(c => c.Name != "#text" && c.Name != "span");
            firstNode?.ParentNode.InsertAfter(copyright, firstNode);
            return htmlDoc;
        }
        public static HtmlDocument RemoveLinkFromExternalLinks(HtmlDocument htmlDoc) {
            var ids = htmlDoc.DocumentNode.Descendants()
                .Where(d => !string.IsNullOrEmpty(d.Id)).Select(d => d.Id).Distinct();
            var links = htmlDoc.DocumentNode.SelectNodes("//a")?
                .Where(a => ids.Contains(a.Attributes["href"]?.Value.Replace("#", string.Empty)) == false
                && a.Attributes["href"]?.Value?.StartsWith("#") == true
                && a.Attributes["class"]?.Value?.Contains(StsXrefSupLink) == false);
            if (ids == null || links == null) return htmlDoc;
            foreach (var link in links.ToList()) {
                link.Attributes["href"]?.Remove();
                link.Name = "span";
            }
            return htmlDoc;
        }
        public static HtmlDocument RemoveRepeatedLabels(HtmlDocument htmlDoc) {
            var figures = htmlDoc.DocumentNode.Descendants("div")?
                .Where(d => d.Attributes["class"]?.Value.Contains(StsFig) == true)
                .Where(f => f.Descendants("div")?
                .Where(d => d.Attributes["class"]?.Value == StsCaption) != null);
            if (figures == null) return htmlDoc;
            foreach (var fig in figures) {
                var captions = fig.Descendants("div")
                    .Where(d => d.Attributes["class"]?.Value == StsCaption);
                var capGrouped = captions.GroupBy(c => c.ChildNodes
                    .FirstOrDefault(d => d.Attributes["class"]?.Value == StsCaptionLabel)?.InnerHtml);
                foreach (var group in capGrouped) {
                    if (group.Key == null) continue;
                    if (group.Count() > 1)
                        group.First().Remove();
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument MarkStartOfStandard(HtmlDocument htmlDoc) {
            var start = htmlDoc.DocumentNode.Descendants("div")
            .FirstOrDefault(d => d.Attributes["class"]?.Value.Contains(StsMain) == true)?
            .ChildNodes.FirstOrDefault(d => d.Attributes["class"]?.Value.Contains(StsForeword) == false &&
                                            d.Attributes["class"]?.Value.Contains(StsEndorsement) == false &&
                                            d.Attributes["class"]?.Value.Contains(FirstIsoSection) == false &&
                                            !d.ChildNodes.Where(c => c.Attributes["class"]?.Value.Contains(StsEndorsement) == true).Any());
            if (start != null) AddAttributeToNode(start, "class", StsFirstSection);
            return htmlDoc;
        }
        public static HtmlDocument MarkBoldStatements(HtmlDocument htmlDoc) {
            var boldStatementStrings = new List<string> {"SAFETY STATEMENT", "WARNING"};
            var boldStatements = htmlDoc.DocumentNode.Descendants()
                .Where(d => d.Attributes["class"]?.Value.Contains("sts-non-normative-note") == true &&
                            d.ChildNodes.Where(c => c.Attributes["class"]?.Value.Contains("sts-label") == true
                                                    && boldStatementStrings.Any(b => c.InnerHtml.Contains(b)) == true).Any());
            foreach (var boldStatement in boldStatements) {
                //safety statements are bold
                if (boldStatement.InnerHtml.Contains("SAFETY STATEMENT"))
                    AddAttributeToNode(boldStatement, "class", "sts-bold-note");
                //for other statements - only label is bold
                else {
                    var label = boldStatement.ChildNodes.FirstOrDefault(c =>
                        c.Attributes["class"]?.Value.Contains("sts-label") == true);
                    AddAttributeToNode(label, "class", "sts-bold-note");
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument MoveQuotesToNextElement(HtmlDocument htmlDoc) {
            var quotes = htmlDoc.DocumentNode.Descendants().Where(d => d.InnerHtml == "“ " && d.Name == "#text");
            foreach (var quote in quotes.ToList()) {
                var quoteNode = quote;
                var nextSibling = FindNextSibling(quote);
                while (nextSibling == null) {
                    quoteNode = quoteNode.ParentNode;
                    nextSibling = FindNextSibling(quoteNode);
                }
                quote.Remove();
                nextSibling.PrependChild(quote);
            }
            return htmlDoc;
        }
        public static HtmlDocument RemoveSpacingBeforeSubAndSup(HtmlDocument htmlDoc) {
            var subsAndSups = htmlDoc.DocumentNode.Descendants().Where(d => d.Name == "sup" || d.Name == "sub").ToList();
            foreach (var element in subsAndSups) {
                var previous = element.PreviousSibling;
                while (previous != null) {
                    //check previous and remove space from end
                    if (previous?.InnerHtml.EndsWith(" ") == true) previous.InnerHtml = previous.InnerHtml.Remove(previous.InnerHtml.Length - 1);
                    //if node is now empty - remove
                    if (previous?.InnerHtml == string.Empty) {
                        previous = previous.PreviousSibling;
                        element.PreviousSibling.Remove();
                        continue;
                    }
                    break;
                }
                //if normal node, then move on
                if (previous != null && previous?.InnerHtml != string.Empty) continue;

                var parentName = element.ParentNode?.Name;
                var parentPrevious = element.ParentNode?.PreviousSibling?.InnerHtml;
                //check previous parent and remove space from end
                if ((parentName == "b" || parentName == "a") && (parentPrevious?.EndsWith(" ") == true || parentPrevious?.EndsWith("&nbsp;") == true)) {
                    if (parentPrevious.EndsWith("&nbsp;")) element.ParentNode.PreviousSibling.InnerHtml = parentPrevious.Remove(parentPrevious.Length - 6);
                    else element.ParentNode.PreviousSibling.InnerHtml = parentPrevious.Remove(parentPrevious.Length - 1);
                }
                //if previous parent is empty - remove
                if ((parentName == "b" || parentName == "a") && (parentPrevious == " " || parentPrevious == "&nbsp;"))
                    element.ParentNode?.PreviousSibling.Remove();
                
                //link case
                var previousLinkSibling = FindPreviousSibling(element.Ancestors().FirstOrDefault(a => a.Name == "a"));
                if (previousLinkSibling?.InnerHtml.EndsWith(" ") == true)
                    previousLinkSibling.InnerHtml = previousLinkSibling.InnerHtml.Remove(previousLinkSibling.InnerHtml.Length - 1);
            }
            return htmlDoc;
        }
        public static HtmlDocument BringLabelToFrontOfListElement(HtmlDocument htmlDoc) {
            var listItems = htmlDoc.DocumentNode.Descendants().Where(d => d.Name == "li").ToList();
            foreach (var item in listItems) {
                var stsPNodes = FindNodesByClassName(item, StsP);
                foreach (var stsP in stsPNodes) {
                    var stsLabel = stsP.ChildNodes.FirstOrDefault(d => d.Attributes["class"]?.Value.Contains(StsLabel) == true);
                    if (stsLabel != null) {
                        stsLabel.Remove();
                        stsP.ParentNode.InsertBefore(stsLabel, stsP);
                    }
                }
                var label = item.ChildNodes.FirstOrDefault(c => c.Attributes["class"]?.Value == StsLabel);
                if (label != null) {
                    var value = string.Concat(label.InnerText.TakeWhile(c => c != ')' && c != '.'));
                    if (value != label.InnerText && !string.IsNullOrEmpty(value)) {
                        var extra = label.InnerText.FirstOrDefault(s => s == ')' || s == '.');
                        value = string.Concat(value, extra);
                        AddAttributeToNode(item, "value", value);
                        //TODO: if value > 999)
                        AddAttributeToNode(item, "class", "custom-numbered-item");
                    }
                    else if (label.InnerText.Contains("—") || label.InnerText.Contains("-") || label.InnerText.Contains("–")) {
                        value = "—";
                        AddAttributeToNode(item, "value", value);
                        AddAttributeToNode(item, "class", "custom-hyphen-item");
                    }
                    else {
                        value = "•";
                        AddAttributeToNode(item, "value", value);
                        AddAttributeToNode(item, "class", "custom-disc-item");
                    }
                    AddAttributeToNode(item, "class", "custom-type-item");
                    label.Remove();
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument RemoveEmptyNodeFromEnd(HtmlDocument htmlDoc) {
            //find last section before bibliography
            var lastSection = FindNodesByClassName(htmlDoc.DocumentNode, "sts-section")
                .LastOrDefault(d => d.Attributes["class"]?.Value.Contains(StsSectionBibl) == false);
            var lastElement = lastSection?.ChildNodes.LastOrDefault(c => c.Name != "#text");
            if (lastElement?.InnerHtml == string.Empty || lastElement?.InnerHtml == "\u200B") lastElement.Remove();
            return htmlDoc;
        }
        public static HtmlDocument FixEntailedTermsLinking(HtmlDocument htmlDoc) {
            //find last section before bibliography
            var entailedTerms = FindNodesByClassName(htmlDoc.DocumentNode, StsTbxEntailedTerm);
            foreach (var term in entailedTerms) {
                var link = term.ChildNodes?.FirstOrDefault(c => c.Name == "a");
                if (link != null) {
                    foreach (var child in link.ChildNodes.ToList()) {
                        if (child.Attributes["class"]?.Value.Contains(StsTbxEntailedTerm) == true) continue;
                        if (child.Name == "#text") child.Name = "span";
                        child.Remove();
                        term.PrependChild(child);
                    }
                }
            }
            return htmlDoc;
        }
        public static HtmlDocument FixIecTbxSection(HtmlDocument htmlDoc) {
            //moving source to the end
            var tbxSections = FindNodesByClassName(htmlDoc.DocumentNode, StsTbxSec);
            foreach (var tbx in tbxSections) {
                var source = tbx.Descendants().FirstOrDefault(d => d.Attributes["class"]?.Value == StsTbxSource);
                if (source == null) continue;
                var parent = source?.ParentNode;
                source?.Remove();
                parent.AppendChild(source);
            }
            return htmlDoc;
        }
        public static HtmlDocument BringInstructionsInFrontOfList(HtmlDocument htmlDoc) {
            var refLists = FindNodesByClassName(htmlDoc.DocumentNode, StsRefList).FindAll(sts => sts.Name == "ol");
            foreach (var refList in refLists) {
                var instructions = FindNodesByClassName(refList, StsEditingInstruction).FirstOrDefault();
                    if (instructions != null) {
                        instructions.Remove();
                        refList.ParentNode.InsertBefore(instructions, refList);
                    }
            }
            return htmlDoc;
        }
    }
}
