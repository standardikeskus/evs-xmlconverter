﻿using System.Linq;
using HtmlAgilityPack;
using static EVS_XMLConverter.Aids.HtmlHelper;
using static EVS_XMLConverter.Logic.Classificators.TemplateElementNames;
using System;

namespace EVS_XMLConverter.Logic.Html {
    public class FootnoteModifier {
        public static HtmlDocument FixFootnotes(HtmlDocument htmlDoc) {
            //adjusting footnote format for Prince XML
            var footnoteLinks = htmlDoc.DocumentNode.Descendants("a")?
                .Where(d => ((d.Attributes["class"]?.Value.Contains(StsXrefSupLink) == true) 
                || d.Attributes["href"]?.Value.Contains("foo") == true)
                && d.Attributes["href"]?.Value.Contains("table-fn") == false
                && d.Attributes["href"]?.Value.Contains("tfn") == false).ToList();
            if (footnoteLinks != null && footnoteLinks.Any()) {
                foreach (var footnoteLink in footnoteLinks) {
                    if (footnoteLink.Attributes["class"]?.Value.Contains(SupLink) == false) AddAttributeToNode(footnoteLink, "class", SupLink);
                    CleanFootnoteLink(footnoteLink, htmlDoc);
                    if (footnoteLink.ChildNodes.FirstOrDefault(c => c.Name == "sup") == null) footnoteLink.Name = "sup";
                    var valueLink = CreateValueLink(footnoteLink, htmlDoc);
                    if (valueLink == null) continue;
                    var footnote = CreateFootnote(valueLink, htmlDoc);
                    footnoteLink.AppendChild(footnote);
                    CleanFootnote(footnote);
                    var nextSibling = FindNextSibling(footnoteLink);
                    if (nextSibling != null && nextSibling.Attributes["class"]?.Value.Contains(StsFn) == true) FindNextSibling(footnoteLink).Remove();
                }
            }
            var oldFootnotes = htmlDoc.DocumentNode.Descendants("div")?.Where(d => d.Attributes["class"]?.Value == StsFootnotes).ToList();
            if (oldFootnotes == null || !oldFootnotes.Any()) return htmlDoc;
            foreach (var old in oldFootnotes) old.Remove();
            return htmlDoc;
        }
        public static HtmlNode CleanFootnoteLink(HtmlNode footnoteLink, HtmlDocument htmlDoc) {
            var footnoteLinkTitle = footnoteLink.Attributes["title"];
            if (footnoteLinkTitle != null) {
                footnoteLinkTitle.Value = TrimStart(footnoteLinkTitle.Value, "&#xA;");
                footnoteLinkTitle.Value = footnoteLinkTitle.Value.TrimStart(' ', ')', ' ');
            }
            if (footnoteLink.NextSibling?.InnerText == ")") footnoteLink.NextSibling.Remove();
            footnoteLink.Name = "span";
            return footnoteLink;
        }
        public static HtmlNode CreateValueLink(HtmlNode footnoteLink, HtmlDocument htmlDoc) {
            var reference = footnoteLink.Attributes["href"].Value.Replace("#", string.Empty);
            footnoteLink.Attributes["href"].Remove();
            var valueLink = htmlDoc.GetElementbyId(reference)?.ChildNodes.FirstOrDefault(c => c.Name == "div");
            if (valueLink == null) return null;
            valueLink.Name = "span";
            return valueLink;
        }
        public static HtmlNode CreateFootnote(HtmlNode valueLink, HtmlDocument htmlDoc) {
            var footnote = htmlDoc.CreateElement("div");
            AddAttributeToNode(footnote, "class", StsFootnote);
            footnote.InnerHtml = valueLink.InnerHtml;
            return footnote;
        }
        public static HtmlNode CleanFootnote(HtmlNode footnote) {
            foreach (var childNode in footnote.ChildNodes.ToList()) {
                try { childNode.InnerHtml = childNode.InnerHtml.TrimStart(); }
                catch (Exception) { continue; }
                if (childNode.InnerHtml == ")") childNode.Remove();
                if (childNode.Name == "#text") childNode.InnerHtml = childNode.InnerHtml.TrimStart();
            }
            return footnote;
        }
    }
}
