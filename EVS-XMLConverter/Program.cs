﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using EVS_XMLConverter.Objects;
using static EVS_XMLConverter.Logic.Pdf.PdfGenerator;
using static EVS_XMLConverter.Logic.Zip.EvsXmlPackageCompiler;

namespace EVS_XMLConverter {
    internal class Program {
        private static void Main()
        {
            //Sissetulevad XML-id
            var zipFiles = new DirectoryInfo(@"..\..\Assets\InputXMLs")
                    .GetFiles("*.*").Where(file => string.Equals(file.Extension, ".zip")).ToList();

                //EVS XML paketi sihtkoht
                var zipPackageDestination = new FileInfo(Path.Combine(@"..\..\Assets\EVS_XML_Packages\InputXMLs", "InputXMLs.zip"));

                //Pdf sihtkoht
                var pdfDestination = new FileInfo(Path.Combine(@"..\..\Assets\EVS_XML_Packages\InputXMLs", "InputXMLs.pdf"));

                //Pdf Preview sihtkoht
                var pdfPreviewDestination = new FileInfo(Path.Combine(@"..\..\Assets\EVS_XML_Packages\InputXMLs", "InputXMLs_preview.pdf"));

            //Näidisandmed
            var example = new EvsData {
                XmlZipFiles = zipFiles,
                EvsXmlPackageDestination = zipPackageDestination,
                PdfDestination = pdfDestination,
                PdfPreviewDestination = pdfPreviewDestination,
                StaDISOrganisation = "CLC/IEC",
                EnTitle = "Ingliskeelne pealkiri",
                EvsReference = "EVS TÄHIS",
                IcsGroups = new List<string> { "ICS GRUPP" },
                EvsIsoData = new EvsIsoData {
                    Amendments = "",
                    Committee = "",
                    Scope = @"",
                    BulletinIssue = DateTime.Now
                    }
                };
                var xmlPackage = CompileEvsXmlPackage(example);
                var finalFiles = XmlToPdf(example, xmlPackage);
            
        }
    }
}
